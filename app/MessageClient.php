<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageClient extends Model
{
   protected $fillable = [
       'client_id',
       'contenu',
       'date',
       'is_admin'
   ];
   
   public $timestamps = false;
   
   public function client(){
       return $this->belongsTo('App\User');
   } 
}
