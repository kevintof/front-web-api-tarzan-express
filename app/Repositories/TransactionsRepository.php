<?php
  namespace App\Repositories;
  use Illuminate\Support\Facades\DB;
  use App\Transactions;
  

  class TransactionsRepository
  {
     public static function getWalletLastTransaction($id)
     {
         return Transactions::where('portefeuille_id','=',$id)->orderBy('id','desc')->first();
     }

     public static function getStateOnPaygate()
     {
        return Transactions::where('etat_paiement_id',array(8,30))
                           ->get();

     }
     
    

    
  }