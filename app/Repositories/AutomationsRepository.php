<?php
  namespace App\Repositories;
  use Illuminate\Support\Facades\DB;
  use App\Automation;

  class AutomationsRepository{

    public static function getPaymentTypes(){
       return DB::table('automations')
                    ->where('group','=','TYPE')
                    ->where('desc','=','PAIEMENT')
                    ->get();
                
    } 
    public static function getPaymentTypesObj(){
      return   Automation::where('group','=','TYPE')
                   ->where('desc','=','PAIEMENT')
                   ->where('param1', '!=', 'A LA LIVRAISON')
                    ->where('param1', '!=', 'ESPECE')
                   ->get();
               
   } 

    public static function getEtatCommandes(){
     return DB::table('automations')
            ->where('group', '=', 'STATUT')
            ->where('desc', '=', 'COMMANDE')
            ->get();
    }

    public static function getPaymentState($etat)
    {
      return DB::table('automations')
                   ->where('group','=','STATUT')
                    ->where('desc','=','PAIEMENT')
                    ->where('param1','=',$etat)
                    ->first();
    }

    public static function getModeLivraisons(){
      return DB::table('automations')
                   ->select('desc','param1','id')
                   ->where('group','=','TYPE')
                   ->where('desc','=','LIVRAISON')
                   ->get()
                   ;
      }
      public static function getPaygatesKey(){
        return DB::table('automations')
                     ->select('param3')
                     ->where('group','=','TYPE')
                     ->where('desc','=','PAIEMENT')
                     ->where('param1','=','T-MONEY')
                     ->first()
                     ;
        }

    
  }