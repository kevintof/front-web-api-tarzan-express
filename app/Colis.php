<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Colis extends Model
{
    
	protected $fillable = [
        'reference',
        'poids',
        'transport',
        'frais_livraison',
        'transaction_id',
        'type_livraison_id',
        'images',
        'commentaires',
        'details_commande_id',
        'adresse_livraison_id',
        'lots_id',
        'agence_id'
        
        
    ];
    public $timestamps = false;

    protected $with = ['type_livraison','agence','adresse_livraison'];
    
    protected $appends = ['images_customized'];
    public function produits(){
        return $this->hasMany('App\Produits');
    }

    public function transaction(){
        return $this->belongsTo('App\Transactions');
    }

    public function type_livraison(){
        return $this->belongsTo('App\Automation');
    }
    public function details_commande(){
        return $this->belongsTo('App\DetailsCommande');
    } 

    public function adresse_livraison(){
        return $this->belongsTo('App\Adresses');
    }

    public function livraison_details(){
        return $this->hasMany('App\LivraisonDetails');
    }
    public function lots()
    {
        return $this->belongsTo('App\Lots');
    }

    public function agence()
    {
        return $this->belongsTo('App\Agence');
    }

    public function getImagesCustomizedAttribute()
    {
        $images = json_decode($this->images);
       
        
        if($this->images )
        { 
            if(!isset($images->name)){
                return null;
            } 
           
            $imgArray =[];
            
            if(is_array($images->name))
            {
              foreach($images->name as $name)
              {
                  $link = $images->host.'/'.$images->user.'/'.$name;
                  array_push($imgArray,$link);
              }
            }
            else{
                $link = $images->host.'/'.$images->user.'/'.$images->name;
                array_push($imgArray,$images->name);

            }
              
            return $imgArray;
        }
        else{
            return null;
        }
        
    }

    
    
}
