<?php
  namespace App\Notifications;
  use App\Commandes;
  
  class CommandePaid 
  {
     private $beam;

     private $commande;

     

     public function __construct($beam,$commande)
     {
         $this->beam = $beam;
         $this->commande = $commande;
     }

     public function notifyAdmin()
     {
        $interest = "user".$this->commande->user->id;
        return $this->beam->publishToInterests(
            ['jean'],
            [
                "apns" => [
                  "aps" => [
                    "alert" => "Hello!",
                  ],
                ],
                "web"=> [
                  "notification" => [
                    "title" => "Tarzan express",
                    "body" => "La commande ".$this->commande->reference." est passé en cours d'execution",
                    "deep_link"=> "http://127.0.0.1:8001/admin/commande/show/".$this->commande->id
                  ],
                ],
                "fcm" => [
                  "notification" => [
                    "title" => "Tarzan express",
                    "body" => "La commande ".$this->commande->reference." est passé en cours d'execution. Veuillez consulter",
                    "deep_link" => "http://127.0.0.1:8001/admin/commande/show/".$this->commande->id
                  ],
                ],
              ]);
     }







  }

?>
