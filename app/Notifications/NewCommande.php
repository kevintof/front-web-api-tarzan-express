<?php
  namespace App\Notifications;
  use App\Commandes;

  class NewCommande 
  {
     private $beam;

     private $commande;

     

     public function __construct($beam,$commande)
     {
         $this->beam = $beam;
         $this->commande = $commande;
     }

     public function notifyAdmin()
     {
         $interest = "user".$this->commande->user->id;
        return $this->beam->publishToInterests(
            ["jean"],
            [
                "apns" => [
                  "aps" => [
                    "alert" => "Hello!",
                  ],
                ],
                "web"=> [
                  "notification" => [
                    "title" => "Tarzan express",
                    "body" => "Une nouvelle commande a été créée (".$this->commande->reference.")",
                    "deep_link"=> "http://127.0.0.1:8001/admin/commande/show/".$this->commande->id
                  ],
                ],
                "fcm" => [
                  "notification" => [
                    "title" => "Tarzan express",
                    "body" => "La commande".$this->commande->reference."est passé ".$this->commande->statut->param1,
                    "deep_link" => "https://www.facebook.com"
                  ],
                ],
              ]);
     }







  }

?>
