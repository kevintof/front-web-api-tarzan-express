<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Commandes extends Model
{
    protected $fillable = [
        'reference',
        'user_id',
        'details_commande_id',
        'statut_id',
        'montant_livraison',
        'montant_commande',
        'montant_service',
        'information'
    ];

    public $timestamps = false;
    
    public function user(){
        return $this->belongsTo('App\User');
    }

    public function details_commande(){
        return $this->belongsTo('App\DetailsCommande');
    }

    public function statut(){
        return $this->belongsTo('App\Automation');
    }
}
