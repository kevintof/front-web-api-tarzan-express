<?php
 
namespace App\Lib;
 
 
use Pusher\Pusher;
use \Pusher\PushNotifications\PushNotifications;

class PusherFactory
{
    public static function make()
    {
        return new Pusher(
            env("PUSHER_APP_KEY"), // public key
            env("PUSHER_APP_SECRET"), // Secret
            env("PUSHER_APP_ID"), // App_id
            array(
                'cluster' => env("PUSHER_APP_CLUSTER"), // Cluster
                'encrypted' => true,
            )
        );
    }

    public static function makeBeam()
    {
        return new PushNotifications(array(
            "instanceId" => "25ee16ac-55dd-4d65-b870-8b4495092d71",
            "secretKey" => "31C88C548B5037397E5B2147B5D73B23270DDD7F6CAFBC657344A277E0CF7884",
          ));
    
    }

}