<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\DetailsCommande;
class Transactions extends Model
{
    protected $fillable = [
        'reference',
        'mobile_payement_reference',
        'montant',
        'mode_paiement_id',
        'etat_paiement_id',
        'client_id',
        'phone_number',
        'attempt',
        'commentaire',
        'date',
        'montantCreditPorteFeuille',
        'portefeuille_id'
    ];
    public $timestamps = true;

    protected $appends = ['type'];
    
    protected $with = ['mode_paiement','etat_paiement','detailsCommande','portefeuille'];
    public function mode_paiement(){
        return $this->belongsTo('App\Automation');
    }

    public function detailsCommande()
    {
        return $this->hasOne('App\DetailsCommande','transaction_id');
    }

    public function etat_paiement(){
        return $this->belongsTo('App\Automation');
    }

    public function client(){
        return $this->belongsTo('App\User');
    }

    public function portefeuille()
    {
        return $this->belongsTo('App\Portefeuille');
    }

    public function getTypeAttribute()
    {
        if($this->montantCreditPorteFeuille >0)
        {
          return 'Recharge de portefeuille';
        } else {
           return 'Paiement de commande';

        }

    }

}
