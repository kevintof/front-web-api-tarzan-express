<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ville extends Model
{
    protected $fillable = ['nom',
                           'pays_id'];
    
    protected $with = ['agences'];
    
    public function pays()
    {
       return $this->belongsTo('App\Pays');
    }

    public function agences()
    {
        return $this->hasMany('App\Agence');
    }
}
