<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LivraisonDetails extends Model
{
    protected $fillable = [
        'position',
        'commentaire',
        'date',
        'colis_id'
    ];
    public $timestamps = true;

    public function colis(){
        return $this->belongsTo('App\Colis');
    }
}
