<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailsCommande extends Model
{
    protected $fillable = [
        'date_commande',
        'date_prevu_livraison_min',
        'date_prevu_livraison_max',
        'date_livraison',
        'transaction_id',
        'commentaire'
    ];
    public $timestamps = false;

    protected $with = ['commande'];

    public function colis(){
        return $this->hasMany('App\Colis');
    }

    public function transaction(){
        return $this->belongsTo('App\Transactions');
    }

    public function commande(){
        return $this->hasOne('App\Commandes');
    }
}
