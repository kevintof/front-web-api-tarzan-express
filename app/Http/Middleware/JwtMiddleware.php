<?php

namespace App\Http\Middleware;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;
use Closure;
use Exception;
use JWTAuth;
use Illuminate\Http\Response;


class JwtMiddleware extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();
        } catch (Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                 return Response([
                    'status' => 'Token is Invalid'
                ], 401); // Status code here
                /* response(401)->json(['status' => 'Token is Invalid'],401); */
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                /* return response()->json(['status' => 'Token is Expired']); */
                return Response([
                    'status' => 'Token is Expired'
                ], 401);
            
            }else{
                /* return response()->json(['status' => 'Authorization Token not found']); */
                return Response([
                    'status' => 'Authorization Token not found'
                ], 401);
            }
        }
        return $next($request);
    }
}
