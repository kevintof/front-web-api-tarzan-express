<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nom' => 'required|string|between:2,100',
            'prenoms' => 'required|string|max:255',
            'pays_id' => 'required|int',
            'agree' => 'required',
            'phone_number' => 'required|string|unique:users',
            'email' => 'nullable|string|email|max:100|unique:users',
            'password' => 'required|string|confirmed|min:6',
            'code_parrainage' => 'nullable|string|exists:code_affiliations,code' 

        ];
    }


    public function messages()
    {
        return [
            'code_parrainage.exists' => 'Le code de parrainage est invalide',
        ];
    }
}
