<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Commandes;

class FilterCommandeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $id = $this->route('id');
        
        $commandes = Commandes::select('id')->where('user_id','=',Auth::user()->id)->get();
        //dd($commandes->toArray());
        $verdict = false;
        foreach($commandes as $commande)
        {
            if ($commande->id == $id) {
                $verdict = true;
            } 
        }
        

        return $verdict;
        
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
