<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Commande extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'reference' => $this->nom,
            'statut' => $this->statut(),
            'montant_commande' => $this->montant_commande,
            'montant_service' =>$this->montant_service,
            'information' => $this->information
        ];
    }
}
