<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MessageClient;
use Illuminate\Support\Facades\Auth;
use App\Lib\PusherFactory;

class MessageClientController extends Controller
{
    
    public function sendMessage(Request $request,$id)
    {
        $message = new MessageClient();
        if($request->input("contenu")== null)
        {
          return Response(["contenu" =>"this field is required"],422);
        }
        $contenu = $request->input("contenu");
        //dump($contenu);
        $message->contenu = $contenu;
        $message->client_id = $id;
        $message->date = date("Y-m-d H:i:s");
        
        $data["contenu"] = $contenu;
        $data["date"] = date("d/m/Y H:i:s");
        
        if(Auth::user() == null)
        {
          $message->is_admin = true;
          $data["is_admin"] = true;
          $data["sender"] = null;
          $data["sender_name"]= "admin";
          $data["user_id"] = $id;
        }
        else{
            $message->is_admin = false;
            $data["is_admin"] = false;
            $data["sender"] = Auth::id();
            $data["sender_name"]= Auth::user()->nom;
            $data["user_id"] = $id;
          
          }
           
        $pusher = PusherFactory::make();
        $data['event-source'] = 'chat_client';

        
        $pusher->trigger('tarzan-express', 'chat_client', $data);    
        $message->save();
        return response()->json(['state' => 1, 'data' => $message]);



           
        
    }

    public function fetch_client_messages($id)
    {
      $messages = MessageClient::where('client_id','=',$id)->get();
      $contenu ='';
     //dump($messages);
      foreach($messages as $message)
      {
        if($message->is_admin == false)
        {
          $contenu .='<div class="d-flex justify-content-start mb-4">';
        }
        else{
          $contenu .='<div class="d-flex justify-content-end mb-4">';
        }
          $contenu .='<div class="img_cont_msg">
              <img src="https://static.turbosquid.com/Preview/001292/481/WV/_D.jpg" class="rounded-circle user_img_msg">
          </div>
          <div class="msg_cotainer">'
              .$message->contenu.
              /* <span class="msg_time">8:40 AM, Today</span> */
              '
          </div>
      </div>';
        
       

      }
      return response($contenu);

    }
}
