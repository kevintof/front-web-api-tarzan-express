<?php

namespace App\Http\Controllers\User\App;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Gloudemans\Shoppingcart\Facades\Cart;
use App\Notifications\NewCommande;
use Illuminate\Http\JsonResponse;
use App\Commandes;
use App\DetailsCommande;
use App\Colis;
use App\Produits;
use App\Pays;
use App\Automation;
use App\Adresses;
use App\Lib\PusherFactory;
use App\NotificationAdmin;
use App\Http\Resources\Commande as CommandeResource;
use App\Repositories\AutomationsRepository;
use App\Http\Requests\FilterCommandeRequest;

class CommandesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        $commandes = Commandes::where('user_id', '=', Auth::id())->orderBy('id', 'desc')->get();

        $cart_count = Cart::count();
        return view('frontend.commandes.index', compact(['commandes', 'cart_count']));
    }
      
     /**
     * @OA\Get(
     * path="/api/commande",
     * summary="récupération des commandes",
     * description="récupérer les commandes de l'utilisateur connecte. Autorisation jwt requise",
     * operationId="commandes",
     * tags={"Commande"},
     * @OA\Response(
     *    response=422,
     *    description="Wrong credentials response",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
     *        )
     *     ),
	 * @OA\Response(
     *    response=401,
     *    description="Token expriré",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="token expiré")
     *        )
     *     )
     * )
     */
     public function apiIndex(){

        $commandes = Commandes::where('user_id', '=', Auth::id())->orderBy('id', 'desc')->get();

        $cmd = $commandes;
        $index = 0;
        foreach($cmd as $commande)
        {
            $cmd[$index]->statut;
            $cmd[$index]->details_commande;
            $index++;

        }
        return  Response($cmd);

     }



    /**     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request )
    {
        $cart_count = Cart::count();
        $typeLivraisons = AutomationsRepository::getModeLivraisons();
        $pays = Pays::all();
        return view('frontend.commandes.create', compact('cart_count','typeLivraisons','pays'));
    }


    /**     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createByCart(Request $request, $id){
        
        $cart_count = Cart::count();
        $input = $request->all();
        $j = 0;

        for ($i=1; $i <= $request->param ; $i++) {
            $arg = "item".$i;
            $items[$j] = Cart::get($request->$arg);
            $j++;
        }

        return view('frontend.commandes.create', compact(['items', 'cart_count']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         
        $commandRef = Automation::where('group', 'REFERENCE')->where('desc', 'COMMANDE')
                                ->first();

        $colisRef = Automation::where('group', 'REFERENCE')->where('desc', 'COLIS')
                                ->first();


        $detailsCommande = new DetailsCommande();
        $detailsCommande->date_commande = date("d/m/Y H:i:s");
        $detailsCommande->save();

        $colis = new Colis();
        $colis->reference = $colisRef->param1.Auth::user()->id.$this->make_random_custom_string($colisRef->param2);
        $colis->details_commande_id = $detailsCommande->id;
        $colis->type_livraison_id = $request->input('type_livraison_id');
        
        if ($request->input('a_livraison') == '1') {
            //$colis->adresse_livraison_id = 1;
            $colis->agence_id = $request->input('agence');
        }else{
            $adresse = new Adresses();
            $adresse->pays =  $request->input('pays');
            $adresse->ville = $request->input('ville');
            $adresse->quartier = $request->input('quartier');
            $adresse->adresse1 = $request->input('adresse1');
            $adresse->adresse2 = $request->input('adresse2');
            $adresse->save();
            $colis->adresse_livraison_id = $adresse->id;
        }

        $colis->save();
        // dd($request->input('nbreProduit'));
        for ($i=1; $i <= $request->input('nbreProduit') ; $i++) { 
            $produit = new Produits();
            $produit->nom =  $request->input('nom'.$i);
            $produit->description = $request->input('description'.$i);
            $produit->lien = $request->input('lien'.$i);
            $produit->quantite = $request->input('quantite'.$i);

            $produit->colis_id = $colis->id;

            if ($request->input('isItem'.$i) != '0') {
               if($request->input('isItem'.$i)){
                $produit->images = Cart::get($request->input('isItem'.$i))->options->image;
                Cart::remove($request->input('isItem'.$i));
            }
            }
             
            if ($request->file('images'.$i) != null) {
                $imageName = $request->file('images'.$i)->getClientOriginalName();
                $request->file('images'.$i)->move(public_path().'/commandes-colis/'.Auth::user()->id, $imageName);

                $produit->images = $imageName;
                
            }

            $produit->save();
        }
        

        $commande = new Commandes();
        $commande->reference = $commandRef->param1.Auth::user()->id.$this->make_random_custom_string($commandRef->param2);
        $commande->details_commande_id = $detailsCommande->id;
        
        $commande->user_id = Auth::user()->id;

        $commandeStatut = Automation::where('desc', 'COMMANDE')
                                ->where('param2', '1')
                                ->first();


        $commande->statut_id = $commandeStatut->id;
        $commande->save();
        $notification = new NotificationAdmin();
        $notification->type = 'NOUVELLE_COMMANDE';
        $notification->commande_id = $commande->id;
        $notification->etatLecture = false;
        $notification->date = date("Y-m-d H:i:s");
          
        $notification->save();
        $pushNotifications = PusherFactory::makeBeam();
        $publishResponse = new NewCommande($pushNotifications,$commande);
        $publishResponse->notifyAdmin();

        if($request->is('api/*'))
        {
            return  Response($commande->toArray(),201);
        }
        
        else{
            return redirect()->route('user.commande.index', Auth::user()->id)
            ->with('success','Votre commande a été ajouté avec succès');
        }
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Operateurs  $operateurs
     * @return \Illuminate\Http\Response
     */

     /**
     *@OA\SecurityScheme(
     *   securityScheme="bearerAuth",
     *  type="http",
     * scheme="bearer"
  *)*/
 
    /**
     * @OA\Get(
     * path="/api/commande/show/{id}",
     * summary="commande info",
     * description="informations d'une commande spécifique. Autorisation jwt requise",
     * operationId="commandeInfo",
     * tags={"Commande"},
     *   @OA\Parameter(
     *         description="ID commande à retourner",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *           format="int64"
     *         )
     *     ),
     * @OA\Response(
     *    response=422,
     *    description="Wrong credentials response",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
     *        )
     *     )
     * )
     */

    public function show(Request $request,$id)
    {
        //dump($_SERVER['SERVER_NAME']);
        //dump(request()->getHttpHost());
        $cart_count = Cart::count();
         $commande = Commandes::with('statut')->get()->find($id); 
          /* dump($commande->details_commande); */
          if($commande == null)
          {
              return Response(["message"=>"La commande est inexistante"],404);
          }
         $commande->details_commande->colis[0]->produits; 
         $commande->details_commande->colis[0]->livraison_details;
         $commande->details_commande->transaction;
         $commande->details_commande->colis[0]->transaction; 
         

        if($request->is('api/*'))
        {
            $index =0;
            foreach($commande->details_commande->colis[0]->produits as $produit)
            {
                //dd($produit->images);
                if($produit->images )
                {
                    $commande->details_commande->colis[0]->produits[$index]->images = request()->getHttpHost().'/commandes-colis/'.Auth::user()->id.'/'.$produit->images;
                }
                
                $index++;
            }
            return new JsonResponse($commande);
        }
        else{
            return view('frontend.commandes.show', compact('commande', 'cart_count'));
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Operateurs  $operateurs
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Operateurs  $operateurs
     * @return \Illuminate\Http\Response
     */
    public function recap(FilterCommandeRequest $request,$id)
    {
        $commande = Commandes::find($id);
        //$typePaiements = Automation::where('group', 'TYPE')->where('desc', 'PAIEMENT');

        $typePaiements = DB::table('automations')
           ->where('group', '=', 'TYPE')
           ->where('desc', '=', 'PAIEMENT')
           ->where(function($query) {
                $query->where('param1', '!=', 'A LA LIVRAISON')
                      ->where('param1', '!=', 'ESPECE');
            })

           ->get();


        $nbre_produit = 0;
        foreach ($commande->details_commande->colis[0]->produits as $produit) {
            $nbre_produit += $produit->quantite;
        }
        return view('frontend.commandes.recap', compact('commande', 'nbre_produit', 'typePaiements'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Operateurs  $operateurs
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Operateurs  $operateurs
     * @return \Illuminate\Http\Response
     */
    public function getAmount($arg, $id)
    {
        $commande = Commandes::find($id);
        $coli = $commande->details_commande->colis[0];

        switch ($arg)  {
            case 'montant_total':

                $data["montant_total"] = $commande->montant_service + $commande->montant_commande + $commande->montant_livraison;
                return response()->json($data, 200);

                break;

            case 'montant_commande':
                $data["montant_commande"] = $commande->montant_service + $commande->montant_commande;

                $data["html"] = '
                    <div class="wide-block pt-2 pb-2">
                        <font color="black">
                        Transport : <strong><span style="float: right;">
                            '.$coli->transport.' 
                        </span></strong>  </br>

                        Livraison à domicile :  <strong><span style="float: right;">
                            '.$coli->frais_livraison.' 
                        </span></strong> </br>
                        </font>

                        <div class="alert alert-outline-danger mb-1" role="alert">
                            A payer à la livraison :
                            <strong><span style="float: right;"> '.$commande->montant_livraison.' F CFA</span></strong>
                        </div>
                    </div>
                ';
                return response()->json($data, 200);
                
                break;
            
            default:
                # code...
                break;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Operateurs  $operateurs
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }

    
    public function make_random_custom_string($n)
    {

        $alphabet = "123456789abcdefghijklmnopqrstuvwxyz";
        $s = "";
        for ($i = 0; $i != $n; ++$i)
            $s .= $alphabet[mt_rand(0, strlen($alphabet) - 1)];
        return $s;
    }
}
