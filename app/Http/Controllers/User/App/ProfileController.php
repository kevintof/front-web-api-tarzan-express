<?php

namespace App\Http\Controllers\User\App;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Gloudemans\Shoppingcart\Facades\Cart;


use App\User;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $cart_count = Cart::count();
    	$user = User::find(Auth::id());
        return view('frontend.profile.index', compact('user', 'cart_count'));
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Operateurs  $operateurs
     * @return \Illuminate\Http\Response
     */
    public function edit($arg)
    {
    	switch ($arg) {
    		case 'nom':
				$data["html"] = '
                    <div class="modal-header">
                    	<h5 class="modal-title">Modifier votre nom et prénoms</h5>
                    </div>
                    <form>
                        <div class="modal-body text-left mb-2">
                            <div class="form-group basic">
                                <div class="input-wrapper">
                                    <label class="label" for="nom">Nom</label>
                                    <input type="text" class="form-control" id="nom" value="'.Auth::user()->nom.'">
                                    <i class="clear-input">
                                        <ion-icon name="close-circle"></ion-icon>
                                    </i>
                                </div>
                            </div>

                            <div class="form-group basic">
                                <div class="input-wrapper">
                                    <label class="label" for="prenoms">Prénoms</label>
                                    <input type="text" class="form-control" id="prenoms" value="'.Auth::user()->prenoms.'">
                                    <i class="clear-input">
                                        <ion-icon name="close-circle"></ion-icon>
                                    </i>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="btn-inline">
                                <button type="button" class="btn btn-text-danger"
                                    data-dismiss="modal">Annuler</button>
                                <a class="btn btn-text-success" onclick="submit(\''.$arg.'\')">Valider</a>
                            </div>
                        </div>
                    </form>
                ';
                return response()->json($data, 200);
    			break;
    		
    		case 'phone_number':
    			$data["html"] = '
                        <div class="modal-header">
                            <h5 class="modal-title">Modifier votre Téléphone</h5>
                        </div>
                        <form>
                            <div class="modal-body text-left mb-2">

                                <div class="form-group basic">
                                    <div class="input-wrapper">
                                        <label class="label" for="phone_number">Téléphone</label>
                                        <input type="text" class="form-control" id="phonenumber" value="'.Auth::user()->phone_number.'">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </div>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <div class="btn-inline">
                                    <button type="button" class="btn btn-text-danger"
                                        data-dismiss="modal">Annuler</button>
                                    <a class="btn btn-text-success" onclick="submit(\''.$arg.'\')">Valider</a>
                                </div>
                            </div>
                        </form>
                    ';
                return response()->json($data, 200);
    			break;

    		case 'email':
    			$data["html"] = '
                        <div class="modal-header">
                            <h5 class="modal-title">Modifier votre Mail</h5>
                        </div>
                        <form>
                            <div class="modal-body text-left mb-2">

                                <div class="form-group basic">
                                    <div class="input-wrapper">
                                        <label class="label" for="email">E Mail</label>
                                        <input type="text" class="form-control" id="email" value="'.Auth::user()->email.'">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </div>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <div class="btn-inline">
                                    <button type="button" class="btn btn-text-danger"
                                        data-dismiss="modal">Annuler</button>
                                    <a class="btn btn-text-success" onclick="submit(\''.$arg.'\')">Valider</a>
                                </div>
                            </div>
                        </form>
                    ';
                    return response()->json($data, 200);
    			break;

    		case 'password':
                $data["html"] = '
                        <div class="modal-header">
                            <h5 class="modal-title">Modifier votre Mot de passe</h5>
                        </div>
                        <form>
                            <div class="modal-body text-left mb-2">

                                <div class="form-group basic">
                                    <div class="input-wrapper">
                                        <label class="label" for="oldpassword">Ancien mot de passe</label>
                                        <input type="password" class="form-control" id="oldpassword">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </div>
                                </div>

                                <div class="form-group basic">
                                    <div class="input-wrapper">
                                        <label class="label" for="password">Nouveau mot de passe</label>
                                        <input type="password" class="form-control" id="password">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </div>
                                </div>

                                <div class="form-group basic">
                                    <div class="input-wrapper">
                                        <label class="label" for="confirm">Confirmer mot de passe</label>
                                        <input type="password" class="form-control" id="confirm">
                                        <i class="clear-input">
                                            <ion-icon name="close-circle"></ion-icon>
                                        </i>
                                    </div>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <div class="btn-inline">
                                    <button type="button" class="btn btn-text-danger"
                                        data-dismiss="modal">Annuler</button>
                                    <a class="btn btn-text-success" onclick="submit(\''.$arg.'\')">Valider</a>
                                </div>
                            </div>
                        </form>
                    ';
                return response()->json($data, 200);
    			# code...
    			break;

    		default:
    			# code...
    			break;     
    	}
    }


    public static function phone_number_exist($phone_number)
    {
        $u = User::where('phone_number', $phone_number)->first();
        if ($u != null)
            return true;
        
        return false;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Operateurs  $operateurs
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $arg)
    {
        $user = User::find(Auth::user()->id);

        if($user) {
            switch ($arg) {
                case 'nom':
                    
                    $user->nom = $request->input('nom');
                    $user->prenoms = $request->input('prenoms');
                    $user->save();
                    
                    break;

                case 'phone_number':
                    if (!ProfileController::phone_number_exist($request->input('phone_number'))) {
                        $user->phone_number = $request->input('phone_number');
                        $user->save();
                    }else
                        return response()->json(['msg_error' => 'Ce numéro a été déjà enregistré'], 500);
                    
                    break;

                case 'email':
                    $user->email = $request->input('email');
                    $user->save();
                    break;

                case 'password':
                    # code...
                    break;

                default:
                    # code...
                    break;

                
            }
            return response()->json(['message' => 'Données enregistrées avec succès'], 200);
        }

        return response()->json(['msg_error' => 'Utilisateur inexistant'], 500);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function verification()
    {
        return view('frontend.profile.verification');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Operateurs  $operateurs
     * @return \Illuminate\Http\Response
     */
    public function validation(Request $request, $u_id)
    {
    }

}
