<?php

namespace App\Http\Controllers\User\App;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use JWTAuth;
use App\CodeAffiliation;

use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    /*public function __construct()
    {
        $this->middleware('auth:web');
    }*/

    public function index()
    {
        return view('frontend.home.index');
    }

    /**
     * @OA\Put(
     * path="/api/users/update/{id}",
     * summary="Modification des utilisateurs",
     * description="Modification des utilisateurs",
     * operationId="usersUpdate",
     * tags={"utilisateurs"},
     * @OA\Parameter(
     *         description="ID utilisateur",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *           format="int64"
     *         )
     *     ),
     * @OA\RequestBody(
     *    required=true,
     *    description="Pass user credentials",
     *    @OA\JsonContent( 
     *       @OA\Property(property="nom", type="string",example="AFATCHAO"),
     *       @OA\Property(property="prenoms", type="string",example="AMI"),
     *       @OA\Property(property="phone_number", type="string",example="70999999"),
     *       @OA\Property(property="email", type="string",example="garrickervin@gmail.com"),
     *    ),
     * ),
     * @OA\Response(
     *    response=422,
     *    description="Wrong credentials response",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
     *        )
     *     )
     * )
     */
     
    public function edit(Request $request,$id)
    {
      $user = new User();
      $user = User::find($id);
      $user->nom = $request->input('nom');
      $user->prenoms = $request->input('prenoms');
      $user->phone_number = $request->input('phone_number');
      $user->email = $request->input('email');      
      $user->save();

      return Response($user);
    }


    public function changePassword(Request $request)
    {
        if(Hash::check($request->input('old_password'),Auth::user()->password))
        {
            $user = new User();
            $user = Auth::user();

            $user->password = Hash::make($request->input('new_password'));

            $user->save();
            $attemptUser = $user->toArray();
            $attemptUser['password'] = $request->input('new_password');

            return Response(["message" => "le mot de passe a été changé avec succès","token" => $this->createNewToken(JWTAuth::attempt($attemptUser))],200);

        } else {
            return Response(["message" => "Votre ancien mot de passe est incorrect"]);
        }
    }

    protected function createNewToken($token){
        auth()->user()->code_affiliation;
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            //'expires_in' => auth()->factory()->getTTL() * 60,
            'expires_in' => auth('api')->factory()->getTTL(),
            'user' => auth()->user(),            
        ]);
    }

    public function show_user_affilies(){
        
        $code_parrainage= CodeAffiliation::select('id')->where('user_id','=',Auth::user()->id);
        $nb_users = User::whereIn('users.code_parrainage_id',$code_parrainage)->count();
       
        return Response(["nombre_affilies"=>$nb_users],200);    
    }
}
