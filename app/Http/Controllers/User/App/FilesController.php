<?php

namespace App\Http\Controllers\User\App;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Response;

class FilesController extends Controller
{
    public function getGuidePdf(){
        $path = public_path('files/guide');
        $files = File::files($path);
        
        $filename = 'guide.pdf';

        return Response::make(file_get_contents($files[0]), 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="'.$filename.'"'
        ]);
    }
}