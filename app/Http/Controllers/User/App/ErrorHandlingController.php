<?php

namespace App\Http\Controllers\User\App;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ErrorHandlingController extends Controller
{
    //
    public function notFound(){
        return view('frontend.errors.404');
    }

    public function unauthorized(){
        return view('frontend.errors.401');
    }
}