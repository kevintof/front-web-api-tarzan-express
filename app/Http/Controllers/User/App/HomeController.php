<?php

namespace App\Http\Controllers\User\App;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Commandes;
use App\Articles;
use Gloudemans\Shoppingcart\Facades\Cart;

class HomeController extends Controller
{   

    public function __construct()
    {
        $this->middleware('auth:web');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $cart_count = Cart::count();
        $commandes = Commandes::where('user_id','=',Auth::id())->orderBy('id','desc')->limit(5)->get();
        $articles = Articles::orderBy('id','desc')->limit(5)->get();


        $commandes_encours = Commandes::where('user_id','=',Auth::id())
            ->where(function ($join) {
                $join->where('statut_id', 1)
                ->orWhere('statut_id', 2)
                ->orWhere('statut_id', 3);
            })
            ->get();

        $count_commande_cours = count($commandes_encours);


        return view('frontend.home.index', compact('commandes', 'articles', 'cart_count', 'count_commande_cours'));
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexAlibaba()
    {
        return view('frontend.home.indexAlibaba');
    }


     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexAliexpress()
    {
        return view('frontend.home.indexAliexpress');
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexTaobao()
    {
        return view('frontend.home.indexTaobao');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexShopping()
    {
        return view('frontend.home.indexShopping');
    }
}
