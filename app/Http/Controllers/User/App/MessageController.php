<?php

namespace App\Http\Controllers\User\App;
use App\Http\Controllers\Controller;
use App\Commandes;
use App\Message;
use Illuminate\Http\JsonResponse;
use App\Lib\PusherFactory;
use Illuminate\Support\Facades\Auth;
use Gloudemans\Shoppingcart\Facades\Cart;

        

use Illuminate\Http\Request;

class MessageController extends Controller
{

     /**
     * getLoadLatestMessages
     *
     *
     * @param Request $request
     */
    public function getLoadLatestMessages(Request $request)
    {
        if(!$request->user_id) {
            return;
        }
 
        $messages = Message::where(function($query) use ($request) {
            $query->where('from_user', Auth::user()->id)->where('to_user', $request->user_id);
        })->orWhere(function ($query) use ($request) {
            $query->where('from_user', $request->user_id)->where('to_user', Auth::user()->id);
        })->orderBy('created_at', 'ASC')->limit(10)->get();
 
        $return = [];
 
        foreach ($messages as $message) {
 
            $return[] = view('message-line')->with('message', $message)->render();
        }
 
 
        return response()->json(['state' => 1, 'messages' => $return]);
    }

    public function getCommandes()
    {
        $commandes = Commandes::where('user_id','=',Auth::id())->get();
        $cart_count = Cart::count();
        
         
        return view('frontend.commandes.messages',compact(['commandes', 'cart_count'])); 
    }

    /**
     * @OA\Get(
     * path="/api/messages/commandes/{id}",
     * summary="récupération des messages liés à une commande",
     * description="récupération des messages liés à une commande",
     * operationId="getMessagesFromCommande",
     * tags={"Commandes messages"},
     * * @OA\Parameter(
     *         description="ID commande concernée par le message",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *           format="int64"
     *         )
     *     ),
     * @OA\Response(
     *    response=422,
     *    description="Wrong credentials response",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
     *        )
     *     ),
	 * @OA\Response(
     *    response=401,
     *    description="Token expriré",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="token expiré")
     *        )
     *     )
     * )
     */

    public function getMessagesfromCommande(Request $request,$id)
    {

        $cart_count = Cart::count();
        $commande = Commandes::find($id);
        $messages = Message::where('commande_id','=',$id)->get();
        $index = 0;
        if($request->is('api/*'))
        {
            foreach($messages as $message)
            {
              if($message->image)
              {
                $messages[$index]->image = request()->getHttpHost().'/messages-commandes/'.$id.'/'.$message->image; 
                 //dump($messages[$index]->image);
              }
              $index++;
            }
            return new JsonResponse($messages);
        }
         else{
            return view('frontend.commandes.chat',compact('messages','commande', 'cart_count'));
        } 
        
    }
    
}
