<?php

namespace App\Http\Controllers\User\App;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Http\Resources\ModePaiement;
use App\Transactions;
use App\Automation;
use App\Lib\PusherFactory;
use App\Commandes;
use App\Portefeuille;
use App\Traits\AutomaticTask;
use Gloudemans\Shoppingcart\Facades\Cart;
use App\Repositories\AutomationsRepository;
use App\Repositories\TransactionsRepository;
use App\Notifications\CommandePaid;

use Illuminate\Support\Facades\DB;
class TransactionsController extends Controller
{
    use AutomaticTask;
    
    public function index($value='')
    {
    	# code...
    }

    public function historique()
    {
      $transactions = Transactions::where('client_id','=',Auth::user()->id)
                                    ->orderBy('id','DESC')->get();    
    
    
     return Response($transactions);
    }


    public function store_paygate(Request $request){

        $identifier = $request->input('identifier');
        $transaction = Transactions::firstWhere('reference', $identifier);

        $transaction->reference = $request->input('tx_reference');
        $transaction->mobile_payement_reference = $request->input('payment_reference');
        $transaction->montant = $request->input('amount');
        $transaction->date = $request->input('datetime');
        
        // if ($request->input('payment_method') == 'FLOOZ') {
        //     $transaction->mode_paiement_id = 12 ;
        // }else
        //     $transaction->mode_paiement_id = 11 ;

        $transaction->etat_paiement_id = 7;
        
        //$transaction->client_id = Auth::user()->id;
        $transaction->phone_number = $request->input('phone_number');

        return redirect()->route('user.commande.index', [Auth::user()->id, ])
             ->with('success','Votre paiement a été éffectué avec succès');      
    }


    public function store(Request $request) {
        
        
        $identifier = $request->input('identifier');
        $montant = $request->input('montant');
        $date = date("d/m/Y H:i:s");
        $phone_number = $request->input('phone_number');
        $etat_paiement_id = 8;
        $client_id = Auth::user()->id;
        $network = $request->input('network');


        $commande = Commandes::find($request->input('cmd_id'));
        $commande_ref = $commande->reference;
        
        if ($request->input('network') == 'FLOOZ') {
            $mode_paiement_id = 12 ;
            $network = 'MOOV';
        }    

        elseif($request->input('network') == 'T-MONEY')
        {
            $mode_paiement_id = 11 ;
            $network = 'TOGOCEL';
        }
        elseif($request->input('network') == 'PORTEFEUILLE')
        {
            $mode_paiement_id = 29 ;

        }
            

        if ($identifier != '0') {
            $transaction = new Transactions();
            //$transaction = Transactions::firstWhere('reference', $identifier);
            $identifier = Auth::user()->id.$this->make_random_custom_string(4);
            $transaction->client_id = Auth::user()->id;
            //$transaction->commentaire .= '|'.$transaction->reference;
            //dd($identifier);
            $transaction->reference = $identifier;
            $transaction->montant = $montant;
            $transaction->mode_paiement_id = $mode_paiement_id ;
            $transaction->etat_paiement_id = 8;

            $transaction->phone_number = $phone_number;
            $transaction->attempt += 1;
            $transaction->date = $date;

            $transaction->save();

            $commande->details_commande->transaction_id = $transaction->id;

            $commande->details_commande->save();

        }else{
            
            $transaction = new Transactions();
            $identifier = Auth::user()->id.$this->make_random_custom_string(9);

            $transaction->reference = $identifier;
            $transaction->montant = $montant;

            $transaction->mode_paiement_id = $mode_paiement_id ;
                     
            $transaction->etat_paiement_id = 8;
            $transaction->client_id = Auth::user()->id;
            $transaction->phone_number = $phone_number;
            $transaction->attempt = 1;
            $transaction->date = $date;

            $transaction->save();

            $commande->details_commande->transaction_id = $transaction->id;
            $commande->details_commande->save();

        }     

        $info = Automation::where('group', '=', 'TYPE')
                    ->where('desc', '=', 'PAIEMENT')
                    ->where('param1', '=', 'FLOOZ')
                    ->first();

        $key = $info->param3;

        $paygateLink = 'https://paygateglobal.com/v1/page?token='.$key.'&amount='.$montant.'&description=Paiement_de_la_commande_Reference_'.$commande_ref.'&identifier='.$identifier.'&phone='.$phone_number.'&network='.$network;
       
        if($request->is('api/*'))
        {
           return Response([
            'lien' => $paygateLink]);
        }
        else {
            return redirect()->away($paygateLink);
        }
    }
    
        

    public function check_state($auth_token, $tx_reference)
    {
    	
        $response = Http::post('https://paygateglobal.com/api/v1/status', [
            'auth_token' => $auth_token,
            'tx_reference' => $tx_reference,
        ]);

        return $response;
    }

    
    public function checkpaygate($id)
    {
       $commande = Commandes::find($id);
       $auth_token = AutomationsRepository::getPaygatesKey();
       $reference = $commande->details_commande->transaction->reference;
        //dd($reference);
        //dd($auth_token);
       $paygateConfirmation = json_decode($this->paycheck($auth_token->param3,$reference));
       //dd($paygateConfirmation);
       //return Response($paygateConfirmation);
       if(array_key_exists('error_code',$paygateConfirmation))
       {
           //dd($paygateConfirmation->error_message);
           $data['error_message'] = $paygateConfirmation->error_message;
           return Response($data); 
       }
       $data['status'] = $paygateConfirmation->status;
       $data['commande'] = $commande->toArray();
       
       if($data['status']==0){
         $data['description'] = "Paiement réussi avec succès";
         $commande->details_commande->transaction->etat_paiement_id = 7;
          
       } elseif($data['status']==2){
        $data['description'] = "En cours";
        $commande->details_commande->transaction->etat_paiement_id = 30;
       } elseif($data['status']==4){
        $data['description'] = "Expiré";
        $commande->details_commande->transaction->etat_paiement_id = 9;
       }else{
        $data['description'] = "Annulé";
        $commande->details_commande->transaction->etat_paiement_id = 9;
       }
       $commande->details_commande->transaction->save();



       return Response($data);
       




    }
    
   

    public function make_random_custom_string($n)
    {

        $alphabet = "1234567890";
        $s = "";
        for ($i = 0; $i != $n; ++$i)
            $s .= $alphabet[mt_rand(0, strlen($alphabet) - 1)];
        return $s;
    }

    public function getModePaiement()
    {
        $modePaiements = AutomationsRepository::getPaymentTypesObj(); 
        
        return Response(ModePaiement::collection($modePaiements));
    }

    public function payCommandWithWallet(Request $request)
    {
        if($request->input('montant') > Auth::user()->portefeuille->valeur_actuelle)
        {
            return Response(["message"=>"solde insuffisant"],422);
        }
        $transaction = new Transactions();
        $transaction->montant = $request->input('montant');
        $transaction->date = date("d/m/Y H:i:s"); 
        $transaction->etat_paiement_id = 7;
        $transaction->mode_paiement_id = 29;
        $transaction->reference = Auth::user()->id.$this->make_random_custom_string(4);
        $commande = Commandes::find($request->input('cmd_id'));
        $commande->statut_id = 2;
        $commande->save();
        $transaction->client_id = Auth::user()->id;    
        $transaction->attempt = 1;
        $transaction->portefeuille_id = Auth::user()->portefeuille->id;
        $transaction->save();
        Auth::user()->portefeuille->valeur_actuelle -=$transaction->montant;
        Auth::user()->portefeuille->save();
        $commande->details_commande->transaction_id = $transaction->id;
        $commande->details_commande->save();
        $pushNotifications = PusherFactory::makeBeam();
        $publishResponse = new CommandePaid($pushNotifications,$commande);
        
        $publishResponse->notifyAdmin();
        
        return Response($transaction);

    }

    public function checkAllTransactionStateOnPaygate()
    {
        $transactions = TransactionsRepository::getStateOnPaygate();
         //dd($transactions);
        $auth_token = AutomationsRepository::getPaygatesKey();
        
        foreach($transactions as $transaction)
        {
            $auth_token = AutomationsRepository::getPaygatesKey();
            $reference = $transaction->reference;
             
            $paygateConfirmation = json_decode($this->paycheck($auth_token->param3,$reference));
                //dd($paygateConfirmation);
            if(array_key_exists('error_code',$paygateConfirmation))
            {
                $transaction->etat_paiement_id = 8; 
            }
              
            if($paygateConfirmation->status==0){
              //$data['description'] = "Paiement réussi avec succès";
              $transaction->etat_paiement_id = 7;
              $pusher = PusherFactory::make();
                if($transaction->montantCreditPorteFeuille != 0){
                  $wallet = Portefeuille::find($transaction->portefeuille_id);
                  $wallet->valeur_actuelle += $transaction->montantCreditPorteFeuille;
                    $wallet->save();
                    
                    $event = 'portefeuille_recharge';
                    $data['event-source'] = $event;
                    $data['montant-recharge'] = $transaction->montantCreditPorteFeuille;
                    $data['solde-actuelle'] = $wallet->valeur_actuelle;
                    $data['user_id'] = $wallet->user_id;
                    $pusher->trigger('tarzan-express',$event,$data);
                }
                else{
                    $transaction->detailsCommande->commande->statut_id = 2;
                    $transaction->detailsCommande->commande->save();
                    $pushNotifications = PusherFactory::makeBeam();
                    $publishResponse = new CommandePaid($pushNotifications,$transaction->detailsCommande->commande);
                    
                    $publishResponse->notifyAdmin();

                    $event = 'confirmation_paiement';
                    $data['event-source'] = $event;
                    $data['montant'] = $transaction->montant;
                    $data['commande_id'] = $transaction->detailsCommande->commande->id;
                    $data['reference'] = $transaction->detailsCommande->commande->reference;
                    $data['user_id'] = $transaction->user_id;
                    $pusher->trigger('tarzan-express',$event,$data);
                }

               
            } elseif($paygateConfirmation->status==2){
             //$data['description'] = "En cours";
             $transaction->etat_paiement_id = 30;
            } elseif($paygateConfirmation->status==4){
             //$data['description'] = "Expiré";
             $transaction->etat_paiement_id = 9;
            }else{
             $data['description'] = "Annulé";
             $transaction->etat_paiement_id = 9;
            }
            $transaction->save();
     
     
            
            
            
        }
         \Log::info('right here well');
        return Response('leger');
    }


}
