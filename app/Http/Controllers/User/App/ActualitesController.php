<?php

namespace App\Http\Controllers\User\App;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Gloudemans\Shoppingcart\Facades\Cart;
use App\Automations;
use App\Articles;


class ActualitesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cart_count = Cart::count();

        $promos = Articles::where('type_id', '=', 28)
                    ->orderBy('id','desc')
                    ->limit(5)
                    ->get();

        $infos = Articles::where('type_id', '=', 29)
                    ->orderBy('id','desc')
                    ->limit(5)
                    ->get();

        $divers = Articles::where('type_id', '=', 30)
                    ->orderBy('id','desc')
                    ->limit(5)
                    ->get();

        
        $articles = Articles::orderBy('id','desc')->get();
        if($request->is('api/*')) {
            return Response($articles);
        } else {
            return view('frontend.actualites.index', compact(['promos','infos','divers', 'cart_count']));
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Operateurs  $operateurs
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        $cart_count = Cart::count();
        $article = Articles::find($id);

        if($request->is('api/')) {
            return Response($article);
        } else {
            return view('frontend.actualites.show', compact(['article', 'cart_count']));
        }
        
    }

    public function store(Request $request)
    {
        $article = new Articles();

        $article->titre = $request->input('titre');

        $article->contenu = $request->input('contenu');
        
        $article->type_id =  $request->input('type_id');
        /*$articleType = Auvetomations::find($request->input('type_id'));

        if($articleType )*/
        $article->save();

        return Response($article);



    }
}
