<?php

namespace App\Http\Controllers\User\App;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProduitsController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_link(Request $request)
    {

    	$value = $request->input('value');
        return view('frontend.produits.displayLink', $value);
    }
}
