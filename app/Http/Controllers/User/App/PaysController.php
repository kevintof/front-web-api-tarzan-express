<?php

namespace App\Http\Controllers\User\App;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Pays;
use App\Ville;
use App\Agence;
use App\Repositories\AutomationsRepository;
use App\Http\Resources\Pays as PaysResource;
use App\Http\Resources\PaysWithAgences;

class PaysController extends Controller
{

    /**
     * @OA\Get(
     * path="/api/pays",
     * summary="PAYS",
     * description="recuperation des pays",
     * operationId="paysListe",
     * tags={"Pays"},
     * @OA\Response(
     *    response=422,
     *    description="Wrong credentials response",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
     *        )
     *     )
     * )
     */

    public function getPays(Request $request)
    {
        if($request->input('query') ==null)
        {
            
            $pays = PaysResource::collection(Pays::all());
        } elseif($request->input('query') == 'agences'){
            $pays = PaysWithAgences::collection(Pays::all());
        }

        $modeLivraisons = AutomationsRepository::getModeLivraisons();
        
        
         return  Response(["mode_livraisons" =>$modeLivraisons, "pays"=>$pays]);
    }

    public function getPaysWithVillesAgences()
    {
        
    }    

    public function getVillesFromPays(Request $request,$id)
    {
       $villes = Ville::where('pays_id','=',$id)->orderBy('nom','asc')->get();
       
       return Response($villes);
    }

    public function getAgencesFromVilles(Request $request,$id)
    {
        $agences = Agence::where('ville_id','=',$id)->orderBy('nom','asc')->get();
        
        $index = 0;

        foreach($agences as $agence)
        {
            $agences[$index]->adresse;
            $index++;

        }
        return Response($agences);
    }
}
			
