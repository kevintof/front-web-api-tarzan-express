<?php

namespace App\Http\Controllers\User\App;
use App\Traits\AutomaticTask;
use Illuminate\Http\Request;
use App\Transactions;
use App\Repositories\AutomationsRepository;
use App\Repositories\TransactionsRepository;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Portefeuille;
use Illuminate\Support\Facades\Http;

class PortefeuilleController extends Controller
{
    use AutomaticTask;

    public function fetch()
    {
        $portefeuille = Auth::user()->portefeuille;

        return Response($portefeuille);

    }
    
    public function chargerParPaygates(Request $request)
    {
        $montant = $request->input('montant');
        $date = date("d/m/Y H:i:s");
        $phone_number = $request->input('phone_number');
        $network = $request->input('network');
        $id = Auth::user()->portefeuille->id;
        
        if ($request->input('network') == 'FLOOZ') {
            $mode_paiement_id = 12 ;
            $network = 'MOOV';
        } else {
            $mode_paiement_id = 11;
            $network = 'TOGOCEL';
        }
        $identifier = Auth::user()->id.$this->make_random_custom_string(9);
        
        $transaction =  new Transactions();

        $transaction->reference = $identifier;
        $transaction->mode_paiement_id = $mode_paiement_id;
        $transaction->phone_number = $phone_number;
        $transaction->attempt = 1;
        $transaction->date = $date;
        $transaction->client_id = Auth::user()->id;
        $transaction->portefeuille_id = $id;
        $transaction->montantCreditPorteFeuille = $montant;
        $transaction->etat_paiement_id = 9;    
        $transaction->save();
         
        
        $auth_token = AutomationsRepository::getPaygatesKey();

        $paygateLink = 'https://paygateglobal.com/v1/page?token='.$auth_token->param3.'&amount='.$montant.'&description=Chargement_de_portefeuille&identifier='.$identifier.'&phone='.$phone_number.'&network='.$network;

        return Response(["lien"=> $paygateLink]);
        
    }


    public function checkTransactionOnPaygates()
    {
        
        $portefeuille_id = Auth::user()->portefeuille->id;
         //dd($portefeuille_id);
        $portefeuille = Portefeuille::find($portefeuille_id);
        //dd($portefeuille->valeur_actuelle);
        $transaction = TransactionsRepository::getWalletLastTransaction($portefeuille_id);
        //dd($transaction->phone_number);
        $auth_token = AutomationsRepository::getPaygatesKey();
        $paygateConfirmation = json_decode($this->paycheck($auth_token->param3,$transaction->reference));
        
        if(array_key_exists('error_code',$paygateConfirmation))
       {
           //dd($paygateConfirmation->error_message);
           $data['error_message'] = $paygateConfirmation->error_message;
           return Response($data); 
       }
       $data['status'] = $paygateConfirmation->status;
       //$data['commande'] = $commande->toArray();
       
       if($data['status']==0){
         $data['description'] = "Paiement réussi avec succès";
         $transaction->etat_paiement_id = 7;
         if($transaction->isDirty('etat_paiement_id'))
         {
            $portefeuille->valeur_actuelle += $transaction->montantCreditPorteFeuille; 
            $portefeuille->save();
         }
         


          
       } elseif($data['status']==2){
        $data['description'] = "En cours";
        $transaction->etat_paiement_id = 30;
       } elseif($data['status']==4){
        $data['description'] = "Expiré";
        $transaction->etat_paiement_id = 9;
       }else{
        $data['description'] = "Annulé";
        $transaction->etat_paiement_id = 9;
       }
       $transaction->save();
       


       return Response($data);
       

        

    }

    
}
