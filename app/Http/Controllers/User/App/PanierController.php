<?php

namespace App\Http\Controllers\User\App;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Gloudemans\Shoppingcart\Facades\Cart;
use App\Produits;

class PanierController extends Controller
{


    public function show(){

        $data["items"] = Cart::content();
        return view('frontend.panier.show', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $urequest
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
     
        $rowId = Auth::user()->id.$this->make_random_custom_string(20);
            
        $produit = new Produits();       
        $produit->nom =  $request->input('nom');
        $produit->description = $request->input('description');
        $produit->lien = $request->input('lien');
        $produit->quantite = $request->input('quantite');


        $this->validate($request, [
            'filename.*' => 'mimes:jpg,jpeg,png,jpg,gif,svg'
        ]);

        
        if($request->hasfile('images')){
        
            $produit->images = $request->images->getClientOriginalName();
            $request->images->move(public_path().'/commandes-colis/'.Auth::user()->id, $produit->images);    
        }


        Cart::add(['id' => $rowId, 'name' => $request->input('nom'), 'qty' => intval($request->input('quantite')), 'price' => 0, 'weight' => 0, 'options' => ['lien' => $request->input('lien'), 'description' => $request->input('description'), 'image' => $produit->images]]);

        return redirect()->route('user.panier', Auth::user()->id)->with('success',var_dump($request->images));
    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cart::remove($id);
        return back()->with('success','suppression du produit réussie');
    }


    public function reset(){
        Cart::destroy();
    }

    public function save($id){
        
        $item = Cart::get($id);
        Cart::remove($id);

        Cart::instance('save')->add($item->id, $item->name, 1, $item->price)->associate('App\Produits');
        return redirect()->route('cart.index')->with('success','Produit enregistré pour plutard');
    }


    public function update( $id)
    {
    	# code...
    }



    public function make_random_custom_string($n)
    {

        $alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $s = "";
        for ($i = 0; $i != $n; ++$i)
            $s .= $alphabet[mt_rand(0, strlen($alphabet) - 1)];
        return $s;
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function get_all_items($u_id)
    {
        $items = Cart::content();
        $data["count"] = Cart::count();

        if (Cart::count() > 0 ) {
            $i = 1;
            foreach ($items as $item) {

                $img =  '';
                if ($item->options->image != '')
                    $img .= '<img id="itemImg'.$i.'" src="/commandes-colis/'.Auth::user()->id.'/'.$item->options->image.'" alt="img" class="image-block imaged w64">';
                else
                    $img .= '<img id="itemImg'.$i.'" src="/img/sample/brand/1.jpg" alt="img" class="image-block imaged w64">';

                
                $data[$i] = ' <a class="item">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="'.$i.'" value="'.$item->id.'" onchange="addNewProductfield(\''.$item->name.'\', \''.$item->options->description.'\', '.$item->qty.', \''.$item->options->lien.'\' , \''.$item->rowId.'\', '.$i.')">
                        <label class="custom-control-label" for="'.$i.'"></label>
                    </div>

                    <div class="detail">'
                    .$img.

                    '<div>
                        <strong> '.$item->name.' </strong> x '.$item->qty.'
                            
                        <p> '.$item->options->description.' &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</p>
                        </div>
                    </div>

                    <div class="right">
                        <button type="button" onclick="window.location.href=\''.$item->options->lien.'\';" class="btn btn-icon btn-secondary mr-1">
                            <img src="/img/app/link.png" alt="img" class="square imaged w24">
                        </button> 
                    </div>
                    
                </a>';

                $i++;
            }
        }
        
        return response()->json($data,200);
       
    }



    public function get_saveApp(Request $request){

        $link = $request->input("link");
        $name = $request->input("name");
        return view('frontend.produits.productSave', compact('link', 'name'));
    }

    public function post_saveApp(Request $request){

        $rowId = Auth::user()->id.$this->make_random_custom_string(20);
            
        $produit = new Produits();       
        $produit->nom =  $request->input('nom');
        $produit->description = $request->input('description');
        $produit->lien = $request->input('lien');
        $produit->quantite = $request->input('quantite');


        $this->validate($request, [
            'filename.*' => 'mimes:jpg,jpeg,png,jpg,gif,svg'
        ]);

        
        if($request->hasfile('images')){
        
            $produit->images = $request->images->getClientOriginalName();
            $request->images->move(public_path().'/commandes-colis/'.Auth::user()->id, $produit->images);    
        }


        Cart::add(['id' => $rowId, 'name' => $request->input('nom'), 'qty' => intval($request->input('quantite')), 'price' => 0, 'weight' => 0, 'options' => ['lien' => $request->input('lien'), 'description' => $request->input('description'), 'image' => $produit->images]]);

        return redirect()->route('user.produit.save')->with('success','Votre commande a été ajouté avec succès');
        
    }


}
