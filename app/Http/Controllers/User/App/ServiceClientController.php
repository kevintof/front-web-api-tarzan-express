<?php 


namespace App\Http\Controllers\User\App;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\MessageClient;
use Illuminate\Support\Facades\Auth;
use Gloudemans\Shoppingcart\Facades\Cart;



class ServiceClientController extends Controller
{
    public function show(Request $request){

        $cart_count = Cart::count();
        $messages = MessageClient::where('client_id','=',Auth::id())->get();
        if($request->is('api/*')) {
            return  Response($messages);
        } else {
            return view('frontend.informations.service_client',compact('messages', 'cart_count'));
        }

        

    }

    
}