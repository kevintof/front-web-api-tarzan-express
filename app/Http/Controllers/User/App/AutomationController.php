<?php

namespace App\Http\Controllers\User\App;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Automation;
use App\Repositories\AutomationsRepository;
use Illuminate\Support\Facades\DB;


class AutomationController extends Controller
{
    public function getType($arg)
    {
    	$types = Automation::where('group', '=', 'TYPE')
			    	->where('desc', '=', $arg)
			    	->get();

    	return response()->json($types, 200);
	}
	

    public function getKey()
    {
    	$info = Automation::where('group', '=', 'TYPE')
			    	->where('desc', '=', 'PAIEMENT')
			    	->where('param1', '=', 'FLOOZ')
			    	->first();

    	return response()->json($info, 200);
	}
	
	/**
     * @OA\Get(
     * path="/api/mode-livraisons/",
     * summary="récupération des modes de livraison",
     * description="récupérer les modes de livraison. Autorisation jwt requise",
     * operationId="modesLivraison",
     * tags={"Mode de livraison"},
     * @OA\Response(
     *    response=422,
     *    description="Wrong credentials response",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
     *        )
     *     ),
	 * @OA\Response(
     *    response=401,
     *    description="Token expriré",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="token expiré")
     *        )
     *     )
     * )
     */
	public function getModeLivraisons(){
		$modeLivraisons = AutomationsRepository::getModeLivraisons();
		
		return response()->json($modeLivraisons,200);
	}
}
