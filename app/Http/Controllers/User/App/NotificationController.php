<?php

namespace App\Http\Controllers\User\App;
use App\Notification;
use Illuminate\Support\Facades\Mail;
use App\Commandes;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use App\Lib\PusherFactory;


class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Get(
     * path="/api/notifications",
     * summary="Compter les nouvelles notifications",
     * description="Compter les nouvelles notifications",
     * operationId="countNewNotifications",
     * tags={"Notifications"},
     * 
     * @OA\Response(
     *    response=422,
     *    description="Wrong credentials response",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
     *        )
     *     ),
	 * @OA\Response(
     *    response=401,
     *    description="Token expriré",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="token expiré")
     *        )
     *     )
     * )
     */

    public function index()
    {
        $notifications= DB::table('notifications')
        ->where('etatLecture','=',false)
        ->join('commandes', function ($join) {
        $join->on('commandes.id', '=', 'notifications.commande_id')
                 ->where('commandes.user_id', '=', Auth::id());
        })
        ->get();

        $num = count($notifications);
         return response($num, 200)
        ->header('Content-Type', 'text/plain');
    }

    /**
     * @OA\Get(
     * path="/api/notifications_view",
     * summary="Afficher les notifications de l'utilisateur",
     * description="Afficher les notifications de l'utilisateur",
     * operationId="getUserNotifications",
     * tags={"Notifications"},
     * 
     * @OA\Response(
     *    response=422,
     *    description="Wrong credentials response",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
     *        )
     *     ),
	 * @OA\Response(
     *    response=401,
     *    description="Token expriré",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="token expiré")
     *        )
     *     )
     * )
     */

    public function show(Request $request)
    {
      $notifs = DB::table('notifications')->whereIn('commande_id',DB::table('commandes')->select('id')->where('user_id',Auth::id()))->orderByDesc('id')->get();
      
      $notifications = new Collection();
      foreach($notifs as $notif)
      {
        $news = Notification::find($notif->id);
        $news->commande->statut;
        $notifications->push($news);
         
      }

      if($request->is('api/*'))
        {
          
            return new JsonResponse($notifications);
        }
        else{

          return view('frontend.informations.notifications',compact('notifications')); 
        }
     
      
       
      
         
    }

    public function updateReadStateOfNotification()
    {
       $notifications = Notification::whereIn('commande_id',Commandes::select('id')->where('user_id','=',Auth::user()->id)->where('etatLecture','=',false))->get(); 

       foreach($notifications as $notification) {
          $notification->etatLecture = true;
          $notification->save();
          
       }
       return Response(["message" => "mises à jour effectuées"],200);
    }

    public function getNotReadNotifications()
    {
      $notifications = Notification::whereIn('commande_id',Commandes::select('id')->where('user_id','=',Auth::user()->id)->where('etatLecture','=',false))->get();

      return Response($notifications);
    }

    public function state($id) {
       
        $notification = Notification::find($id);
        $notification->etatLecture = true;

        $notification->save();
        return response()->json(["statut"=>"success"],200);

    }

    public function changeIsClickedState($id)
    {
      $notification = Notification::find($id);
      $notification->seen = true;
      $notification->save();
      return response()->json(["statut"=>"success"]);
    }

    public function triggerMissedPushNotification(Request $request,$userId)
    {
      $lastConnectionStr = $request->input('last_connection');
      $lastConnection = date("Y-m-d H:i:s", strtotime($lastConnectionStr. '+1 seconds '));
      $nowDate = date("Y-m-d H:i:s");
      
      $notifications = Notification::whereBetween('created_at',[$lastConnection,$nowDate])
                                    ->OrwhereBetween('updated_at',[$lastConnection,$nowDate])
                                     ->whereIn('commande_id',DB::table('commandes')->select('id')
                                     ->where('user_id',$userId))
                                   ->orderByDesc('id')->get();
      //$notifs = DB::table('notifications')->whereIn('commande_id',DB::table('commandes')->select('id')->where('user_id',Auth::id()))->orderByDesc('id')->get();
      $pusher = PusherFactory::make();
      foreach($notifications as $notification)
      {
        $commande = Commandes::find($notification->commande_id);
         if($notification->type == 'ETAT_COMMANDE')
         {
          $data["class"] = $commande->statut->param5;
          $data["user_id"] = $commande->user->id;
          $data["message"] = "La commande ".$commande->reference." est ".$commande->statut->param1;
          $data["event-source"] = 'push';
          $notification->save();
        
          $pusher->trigger('tarzan-express', 'push', $data);
         }
         if($notification->type == 'MESSAGERIE')
         {
          
          $data["is_admin"] = true;
          $data["sender"] = null;
          $data["sender_name"]= "admin";
          $data["commande_id"] = $notification->commande_id;
          
          $channel = 'tarzan-express';
          $event = 'chat';
          $data['event-source'] = $event;
          $data['reference'] = $commande->reference;
          
          $pusher->trigger($channel, $event, $data);


         }
      }
      return Response($notifications);


    }

    

    public function basic() {
        
        $data = array('name'=>"kevin toffa");
        Mail::send('frontend.informations.mail', $data, function($message) {
        $message->to('garrickervin@gmail.com', 'Tutorials Point')->subject
        ('Laravel HTML Testing Mail');
        $message->from('kevinoustoffa@gmail.com','kevin toffa');
      });
      
        return response()->json(["statut"=>"success"],200);
     }

     /**envoyer une notification par mail à l'utilisateur après changement d'état d'une commande */
     public function basicc(){
      
        $data = array('name'=>"Service clientèle chinashopping");
        Mail::send('frontend.informations.mail', $data, function($message) {
        $message->to('garrickervin@gmail.com', 'Tutorials Point')->subject
        ('Laravel HTML Testing Mail');
        $message->from('kevinoustoffa@gmail.com','Virat Gandhi');
      });
    
    }
    
}