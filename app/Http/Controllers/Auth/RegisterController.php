<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Pays;
use App\User;
use App\CodeAffiliation;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:web');
    }

    public function showRegisterForm()
    {
        $pays = Pays::orderBy('id', 'asc')->get();
        return view('auth.user-register', compact('pays'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nom' => ['required', 'string', 'max:255'],
            'prenoms' => ['required', 'string', 'max:255'],
            'pays_id' => ['required', 'int'],
            'phone_number' => ['required', 'string', 'unique:users'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  Request  $data
     * @return \App\User
     */
    protected function create(Request $data)
    {   
        $data->validate([
           'nom' => ['required', 'string', 'max:255'],
            'prenoms' => ['required', 'string', 'max:255'],
            'pays_id' => ['required', 'int'],
            'phone_number' => ['required', 'string', 'unique:users'],
            'agree' => ['required'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        $user = User::create([
            'nom' => $data['nom'],
            'prenoms' => $data['prenoms'],
            'pays_id' => $data['pays_id'],
            'phone_number' => $data['phone_number'],
            'email' => $data['email'],
            'agree' => $data['agree'],
            'password' => Hash::make($data['password']),
        ]);



        $codeAffiliation = new CodeAffiliation();

        $codeAffiliation->code = $this-> make_random_custom_string(4);
        $codeAffiliation->date_validite = '99999';
        $codeAffiliation->user_id = $user->id;
        $codeAffiliation->save();

         if ($data['code_parrainage'] != '') {
           $code_parrainage = CodeAffiliation::where('code', $data['code_parrainage'])
                                ->first();
              //review with david code
            if ($code_parrainage) {
                $user->code_parrainage_id =  $code_parrainage->id;
                $user->save();
               }                  
            //review
            
         }
         
       
        
       
            if(Auth::guard('web')->attempt(['phone_number' => $data['phone_number'], 'password' => $data['password']])){
              return redirect()->intended(route('user.home'))->with('success','Votre inscription a été effectuée avec succès.');
            }
         
       

/*         return redirect()->intended(route('user.login'))->with('success','Votre inscription a été effectuée avec succès.'); */

    }

    public function make_random_custom_string($n)
    {

        $alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        $s = "";
        for ($i = 0; $i != $n; ++$i)
            $s .= $alphabet[mt_rand(0, strlen($alphabet) - 1)];
        return $s;
    }

}
