<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\CodeAffiliation;
use Validator;
use JWTAuth;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\RegisterRequest;
use App\Portefeuille;

class ApiAuthController extends Controller
{
    

   
    /**
     * @OA\Post(
     * path="/api/login",
     * summary="AUTHENTIFICATION",
     * description="Authentification par numero de téléphone, mot de passe",
     * operationId="authLogin",
     * tags={"authentification"},
     
     * @OA\RequestBody(
     *    required=true,
     *    description="Pass user credentials",
     *    @OA\JsonContent(
     *       required={"phone_number","password"},
     *       @OA\Property(property="phone_number", type="string", example="90023454"),
     *       @OA\Property(property="password", type="string", format="password", example="12345678"),
     *    ),
     * ),
     * @OA\Response(
     *    response=422,
     *    description="Wrong credentials response",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
     *        )
     *     )
     * )
     */

    
    public function login(Request $request){
    	$validator = Validator::make($request->all(), [
            'phone_number' => 'required|min:8|numeric',
            'password' => 'required|string|min:6',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        if (! $token = JWTAuth::attempt($validator->validated())) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->createNewToken($token);
    }

    public function checkPhoneNumber(Request $request)
    {
        $state = User::where('phone_number','=',$request->input('phone_number'))
                        ->where('pays_id','=',$request->input('pays_id'))
                        ->exists();
        
        return Response(["state"=>$state]);
    }

    public function changePassWord(Request $request)
    {
        $user = User::where('phone_number','=',$request->input('phone_number'))
                      ->where('pays_id','=',$request->input('pays_id'))
                      ->first();
        
        if($user == null){
            return Response(["message" => "utilisateur non trouvé"],404);
        }
        $password = Hash::make($request->input('password'));

        $user->password = $password;

        $user->save();

        return Response(["user"=>$user]);
        
    }

    /**
     * @OA\Post(
     * path="/api/register",
     * summary="AUTHENTIFICATION",
     * description="Inscription des utilisateurs. Le mot de passe minimum est 6",
     * operationId="authRegister",
     * tags={"authentification"},
     * @OA\RequestBody(
     *    required=true,
     *    description="Pass user credentials",
     *    @OA\JsonContent(
     *       required={"phone_number","password","password_confirmation","agree","pays_id","nom","prenoms"},
     *       @OA\Property(property="phone_number", type="string", example="90023454"),
     *       @OA\Property(property="password", type="string", format="password", example="12345678"),
     *       @OA\Property(property="pays_id", type="integer", example="1"),
     *       @OA\Property(property="agree", type="string", example="on"),
     *       @OA\Property(property="email", type="string", example="johndoe@gmail.com"), 
     *       @OA\Property(property="password_confirmation", type="string", example="12345678"),
     *       @OA\Property(property="nom", type="string", example="john"),
     *       @OA\Property(property="prenoms", type="string", example="doe"),
     *      @OA\Property(property="code_parrainage", type="string", example="ZRT8"),
     * ),
     * ),
     * @OA\Response(
     *    response=422,
     *    description="Wrong credentials response",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
     *        )
     *     )
     * )
     */

    public function register(RegisterRequest $request) {
        
        $validated = $request->validated();

       

        $user = User::create(array_merge(
                     $validated ,
                    ['password' => Hash::make($request->password)]
                ));
        
        $codeAffiliation = new CodeAffiliation();
        $user->password = Hash::make($request->password);
        $codeAffiliation->code = $this-> make_random_custom_string(4);
        $codeAffiliation->date_validite = '99999';
        $codeAffiliation->user_id = $user->id;
        $codeAffiliation->save();



        if ($request->input('code_parrainage') != '') {
            $code_parrainage = CodeAffiliation::where('code', $request->input('code_parrainage'))
                                 ->first();
               //review with david code
             if ($code_parrainage) {
                 $user->code_parrainage_id =  $code_parrainage->id;
                 $user->save();
                }                  
             //review
             
          }
           $attemptUser = $user->toArray();
           $attemptUser['password'] = $request->input('password');
           $user->save();

           $portefeuille = new Portefeuille();

           $portefeuille->user_id = $user->id;

           $portefeuille->save();

           $token = false;
            //dump($attemptUser);   
            while(! $token ){
                $token = JWTAuth::attempt($attemptUser);
            }   
        
     
        return $this->createNewToken($token);

       
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout() {
        auth()->logout();

        return response()->json(['message' => 'User successfully signed out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh() {
        return $this->createNewToken(auth('api')->refresh());
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userProfile() {
        return response()->json(auth('api')->user());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function createNewToken($token){
        auth()->user()->code_affiliation;
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            //'expires_in' => auth()->factory()->getTTL() * 60,
            'expires_in' => auth('api')->factory()->getTTL(),
            'user' => auth()->user(),

            
        ]);
    }

    public function make_random_custom_string($n)
    {

        $alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        $s = "";
        for ($i = 0; $i != $n; ++$i)
            $s .= $alphabet[mt_rand(0, strlen($alphabet) - 1)];
        return $s;
    }



}
