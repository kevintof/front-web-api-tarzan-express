<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use App\Pays;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:web')->except('logout');
    }

    public function showLoginForm()
    {
        $pays = Pays::orderBy('id', 'asc')->get();

        return view('auth.user-login', compact('pays'));
    }

    public function login(Request $request)
    {
        // Validate form data
        $this->validate($request, [
            'phone_number' => 'required',
            'password' => 'required|min:8'
        ]);

        // Attempt to log the user in
        if(Auth::guard('web')->attempt(['phone_number' => $request->phone_number, 'password' => $request->password], $request->remember))
        {
            return redirect()->intended(route('user.home'));
        }

        // if unsuccessful
        return redirect()->back()->withInput($request->only('phone_number','remember'))
                ->withErrors('erreurs');
    }

    public function logout(Request $request) {
        Auth::logout();
        return redirect('/login');
      }
}
