<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nom', 'prenoms', 'phone_number', 'email', 'password', 'pays_id', 'code_parrainage_id', 'agree', 'verification'
    ];

    protected $with = ['pays'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function commandes(){
        return $this->hasMany('App\Commandes');
    }

    public function pays(){
        return $this->belongsTo('App\Pays');
    }
    
    public function code_affiliation(){
        return $this->hasOne('App\CodeAffiliation');
    }
    
    public function code_parrainage(){
        return $this->belongsTo('App\CodeAffiliation');
    }

     /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier() {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims() {
        return [];
    }
    
    public function portefeuille()
    {
        return $this->hasOne('App\Portefeuille');
    }
}
