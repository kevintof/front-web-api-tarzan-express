<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cotation extends Model
{
    protected $fillable = [
        'montant',
        'paiement_id',
        'date_retour_prevu',
        'commentaire',
        'fichier_in',
        'fichier_out'
    ];

    public $timestamps = true;

    public function produits(){
        return $this->hasMany('App\Produits');
    }
}
