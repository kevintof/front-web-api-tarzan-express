<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Articles extends Model
{
    protected $fillable = [
        'titre',
        'photo_avant',
        'background',
        'contenu',
        'type_id'
    ];

    public $timestamps = true;
    
    protected $with = ['type'];
    protected $appends = ['photo_avant_lien','background_lien'];
    public function type()
    {
        return $this->belongsTo('App\Automation');
    }

    public function getPhotoAvantLienAttribute()
    {
        if($this->photo_avant)
         {
            $image = json_decode($this->photo_avant); 

            return $image->host.'/'.$image->name;

         }
         else{
             return null;
         }
        
    }

    public function getBackgroundLienAttribute()
    {
        if($this->background){
            $image = json_decode($this->background); 
            return $image->host.'/'.$image->name;

        } else{
            return null;
        }

        
    }



}
