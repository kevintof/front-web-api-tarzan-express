<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = [
        
        'commande_id',
        'contenu',
        'is_admin',
        'is_read_by_admin',
        'date',
        'is_read'
    ];
    public $timestamps = false;
    
    public function commande(){
        return $this->belongsTo('App\Commandes');
    }
    
    

    
}
