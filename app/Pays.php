<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pays extends Model
{
    protected $fillable = [
        'indicatif',
        'nom',
        'abr',
        'icone'
    ];
    public $timestamps = true;


    public function users(){
        return $this->hasMany('App\User');
    }
     
    public function villes(){
        return $this->hasMany('App\Ville');
    }
}
