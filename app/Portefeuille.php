<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Portefeuille extends Model
{
    protected $fillable = [
        'valeur_actuelle',
        'user_id',
        'compte_bonus',
        
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
