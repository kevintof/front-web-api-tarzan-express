<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agence extends Model
{
   protected $fillable = ['nom',
                          'ville_id',
                          'adresse_id'];

    protected $with = ['adresse'];

   public function ville()
   {
     return $this->belongsTo('App\Ville');
   }

   public function adresse()
   {
       return $this->belongsTo('App\Adresses');
   }
 }
