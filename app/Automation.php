<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Automation extends Model
{
    protected $fillable = [
        'group',
        'desc',
        'param1',
        'param2',
        'param3',
        'param4',
        'param5',
        'param6'
    ];

    public $timestamps = true;

    public function colis(){
        return $this->hasMany('App\Colis');
    }
    public function lots(){
        return $this->hasMany('App\Lots');
    }
}
