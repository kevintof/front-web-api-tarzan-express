<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CodeAffiliation extends Model
{
    
	protected $fillable = [
        'code',
        'date_validite',
        'user_id'
    ];
    public $timestamps = false;

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function user_parraines(){
        return $this->hasMany('App\User');
    }
}

