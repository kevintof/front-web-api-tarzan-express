 
    <!-- ///////////// Js Files ////////////////////  -->
    
    <!-- Jquery -->
    <script src="{{ asset('js/lib/jquery-3.4.1.min.js') }}"></script>

    <!-- Bootstrap-->
    <script src="{{ asset('js/lib/popper.min.js') }}"></script>
    <script src="{{ asset('js/lib/bootstrap.min.js') }}"></script>
    <!-- Ionicons -->
    <script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>
    <!-- Owl Carousel -->
    <script src="{{ asset('js/plugins/owl-carousel/owl.carousel.min.js') }}"></script>
    <!-- Base Js File -->
    <script src="{{ asset('js/base.js') }}"></script>
    <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
    <script src="https://js.pusher.com/beams/1.0/push-notifications-cdn.js"></script>
<script src="https://js.pusher.com/beams/service-worker.js"></script>
    @yield('js')
    <script>
      const beamsClient = new PusherPushNotifications.Client({
        instanceId: '25ee16ac-55dd-4d65-b870-8b4495092d71',
      });
       console.log('{{Auth::user()->id}}')
      beamsClient.start()
        .then(() => beamsClient.addDeviceInterest('user'+'{{Auth::user()->id}}'))
        .then(() => console.log('Successfully registered and subscribed!'))
        .catch(console.error);
    </script>
    

    <script type="text/javascript">
        $(document).ready(function(){

            fetch_user();
            function fetch_user(){
                $.ajax({
                  url: "{{ route('user.notification') }}" ,
                  method:"GET",
                  success:function(data){
                  /* $('#user_details').html(data); */
                    console.log("message:" + data);
                    if(data==0)
                    {
                      $( "span.notification" ).remove();    
                    }
                    else{
                      $( "span.notification" ).replaceWith( "<span class=\"badge badge-danger\">"+data+"</span>" ); 
                    }
                  } 
                })
            }
        });
         
        var pusher = new Pusher('710a42ce02a3f4836392');
        var channel = pusher.subscribe('notification_channel');


        channel.bind('notification', function(data) {

          if($('span.notification')){
            $('span.notification').remove();
          }

          if(data.user_id == {{Auth::id()}}){
            $('.headerButton').append('<span class="badge badge-danger notification ">'+data.nonlus+'</span>')                       
          }
        });
    </script>
    
</body>

</html>