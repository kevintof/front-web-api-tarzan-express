<div class="modal fade panelbox panelbox-left" id="sidebarPanel" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body p-0">
                <!-- profile box -->
                <div class="profileBox pt-2 pb-2">
                    <div class="image-wrapper">
                        <img src="{{ asset('img/app/profile.png') }}" alt="image" class="imaged  w36">
                    </div>
                    <div class="in">
                        <strong>{{Auth::user()->nom}} {{Auth::user()->prenoms}}</strong>
                        <div class="text-muted">+{{Auth::user()->pays->indicatif}} {{Auth::user()->phone_number}}</div>
                    </div>
                    <a href="#" class="btn btn-link btn-icon sidebar-close" data-dismiss="modal">
                        <ion-icon name="close-outline"></ion-icon>
                    </a>
                </div>
                <!-- * profile box -->
                <!-- balance -->
                <div class="sidebar-balance">
                    <div class="listview-title">Commandes en cours ...</div>
                    <div class="in">
                        <h3 class="amount"> {{ $count_commande_cours }}</h3>
                    </div>
                </div>
                <!-- * balance -->

                <!-- menu -->
                <div class="listview-title mt-1">Menu</div>
                <ul class="listview flush transparent no-line image-listview">
                    <li>
                        <a href="{{ route('user.home') }}" class="item">
                            <div class="icon-box bg-light">
                                <img src="{{ asset('img/app/home-1.png') }}" alt="img" class="image-block imaged w36">
                            </div>
                            <div class="in">
                                Home
                            </div>
                        </a>
                    </li>

                    <li>
                        <a href="{{ route('user.commande.messages') }}" class="item">
                            <div class="icon-box bg-light">
                                <img src="{{ asset('img/app/chat-1.png') }}" alt="img" class="image-block imaged w36">
                            </div>
                            <div class="in">
                                Messages
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('user.commande.index') }}" class="item">
                            <div class="icon-box bg-light">
                                <img src="{{ asset('img/app/commandes.png') }}" alt="img" class="image-block imaged w36">
                            </div>
                            <div class="in">
                                Vos commandes
                            </div>
                        </a>
                    </li>

                    <li>
                        <a href="{{ route('user.commande.create') }}" class="item">
                            <div class="icon-box bg-light">
                                <img src="{{ asset('img/app/order-add.png') }}" alt="img" class="image-block imaged w36">
                            </div>
                            <div class="in">
                                Nouvelle commande
                            </div>
                        </a>
                    </li>

                    <li>
                        <a href="{{ route('user.panier') }}" class="item">
                            <div class="icon-box bg-light">
                                <img src="{{ asset('img/app/cart.png') }}" alt="img" class="image-block imaged w36">
                            </div>
                            <div class="in">
                                Votre Panier
                            </div>
                        </a>
                    </li>

                    <li>
                        <a href="{{ route('user.aliexpress') }}" class="item">
                            <div class="icon-box bg-light">
                                <img src="{{ asset('img/app/aliexpress.png') }}" alt="img" class="image-block imaged w36">
                            </div>
                            <div class="in">
                                Allez sur AliExpress
                            </div>
                        </a>
                    </li>

                    
                </ul>
                <!-- * menu -->

                <!-- others -->
                <div class="listview-title mt-1">Parametres</div>
                <ul class="listview flush transparent no-line image-listview">
                    <li>
                        <a href="{{ route('user.profile.index') }}" class="item">
                            <div class="icon-box bg-light">
                                <img src="{{ asset('img/app/compte.png') }}" alt="img" class="image-block imaged w36">
                            </div>
                            <div class="in">
                                Profile
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('user.serviceClient.index') }}" class="item">
                            <div class="icon-box bg-light">
                                <img src="{{ asset('img/app/administrator.png') }}" alt="img" class="image-block imaged w36">
                            </div>
                            <div class="in">
                                Services clients
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('logout') }}" class="item" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <div class="icon-box bg-light">
                                <img src="{{ asset('img/app/logout.png') }}" alt="img" class="image-block imaged w36">
                            </div>
                            <div class="in">
                                Deconnexion
                            </div>
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                </ul>
                <!-- * others -->

                <!-- send money -->
                <div class="listview-title mt-1">Tarzan Express</div>
                <ul class="listview image-listview flush transparent no-line">
                    <li>
                        <a href="{{ route('user.Informations.presentation') }}" class="item">
                            <div class="icon-box bg-light">
                                <img src="{{ asset('img/app/info.png') }}" alt="img" class="image-block imaged w36">
                            </div>
                            <div class="in">
                                <div>A propos de nous</div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('user.Informations.aide') }}" class="item">
                            <div class="icon-box bg-light">
                                <img src="{{ asset('img/app/help.png') }}" alt="img" class="image-block imaged w36">
                            </div>
                            <div class="in">
                                <div>Comment ça marche</div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('user.Informations.termes_conditions') }}" class="item">
                            <div class="icon-box bg-light">
                                <img src="{{ asset('img/app/terms-and-conditions.png') }}" alt="img" class="image-block imaged w36">
                            </div>
                            <div class="in">
                                <div>Termes et conditions</div>
                            </div>
                        </a>
                    </li>
                </ul>
                <!-- * send money -->

            </div>
        </div>
    </div>
</div>