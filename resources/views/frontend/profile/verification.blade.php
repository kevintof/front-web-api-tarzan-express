@include('frontend.header')

@section('firebase')

    <!-- The core Firebase JS SDK is always required and must be listed first -->
    <script src="https://www.gstatic.com/firebasejs/7.20.0/firebase-app.js"></script>

    <!-- TODO: Add SDKs for Firebase products that you want to use
         https://firebase.google.com/docs/web/setup#available-libraries -->
    <script src="https://www.gstatic.com/firebasejs/7.20.0/firebase-analytics.js"></script>
    <script>
      // Your web app's Firebase configuration
      var firebaseConfig = {
        apiKey: "AIzaSyB5lqEmkTmSXTKvtGCRSjtcq468yf03uJ0",
        authDomain: "pymoi-b1576.firebaseapp.com",
        databaseURL: "https://pymoi-b1576.firebaseio.com",
        projectId: "pymoi-b1576",
        storageBucket: "pymoi-b1576.appspot.com",
        messagingSenderId: "486864420257",
        appId: "1:486864420257:web:ee3f18054a203a97bb9fe0",
        measurementId: "G-JR83JS2CNV"
      };
      // Initialize Firebase
      firebase.initializeApp(firebaseConfig);
      firebase.analytics();


    </script>

@endsection


    <!-- loader -->
    <div id="loader">
        <img src="{{ asset('img/logo-icon.png') }}" alt="icon" class="loading-icon">
    </div>
    <!-- * loader -->

    <!-- App Header -->
    <div class="appHeader no-border transparent position-absolute">
        <div class="left">
            <a href="javascript:;" class="headerButton goBack">
                <ion-icon name="chevron-back-outline"></ion-icon>
            </a>
        </div>
        <div class="pageTitle"></div>
        <div class="right">
        </div>
    </div>
    <!-- * App Header -->

    <!-- App Capsule -->
    <div id="appCapsule">
        
                

        <div class="section mt-2 text-center">
            <h1>VALIDATION DE NUMERO TELEPHONE</h1>
            <h4>Entrer les chiffres reçu par SMS</h4>
        </div>
        <div class="section mb-5 p-2">
            <form method="POST" action="{{ route('user.profile.verification', Auth::user()->id ) }}">
                @csrf
                @method('POST')

                @if ($errors->any())
                    <font color="red">
                        LE CODE NE CORRESPOND PAS
                    </font>
                @endif
                <div class="form-group basic">
                    <input type="text" class="form-control verification-input" id="smscode" placeholder="••••"
                        maxlength="6">
                </div>

                </br>

                <div class="text-center"> 
                    <h4> Recevez le code dans 
                        <button type="button" class="btn btn-light mr-1 mb-1">
                            <span id="time">05:00</span>
                        </button>

                    </h4>

                    <a> J'ai pas reçu le code </a>
                </div>

                
                
                <div class="form-button-group transparent">
                    <div class="col-6">
                        <a href="{{ route('user.profile.index', Auth::user()->id ) }}" class="btn btn-danger btn-block btn-lg"> ANNULER</a>
                    </div>

                    <div class="col-6">
                        <a type="submit" class="btn btn-success btn-block btn-lg" id="validate">VALIDER</a>
                    </div>
                </div>

            </form>
        </div>

    </div>
    <!-- * App Capsule -->

@include('frontend.footer')




