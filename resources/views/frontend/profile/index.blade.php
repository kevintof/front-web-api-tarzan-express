@extends('frontend.layouts.app')
@section('content')
	@section('titre-page')
	  TARZAN EXPRESS | PROFIL
	@endsection
	@section('title')
		PROFIL
	@endsection

	@section('notification')
		<a href="app-notifications.html" class="headerButton">
            <ion-icon name="cart"></ion-icon>
            <span class="badge badge-danger">{{ $cart_count }}</span>
        </a>

    	<a href="app-notifications.html" class="headerButton">
            <ion-icon class="icon" name="notifications"></ion-icon>
            <span class="badge badge-danger notification"></span>
        </a>
	@endsection
	<div id="appCapsule">

		<div class="section mt-3 text-center">
		    <div class="avatar-section">
		        <a href="#">
		            <img src="{{ asset('img/app/profile.png') }}" alt="avatar" class="imaged w100 rounded">
		            <span class="button">
		                <ion-icon name="camera-outline"></ion-icon>
		            </span>
		        </a>
		    </div>
		</div>

		<div class="listview-title mt-1">Notifications</div>
		<ul class="listview image-listview text inset">
		    <li>
		        <div class="item">
		            <div class="in">
		                <div>
		                    Notiications
		                    <div class="text-muted">
		                        Recevez un bip à la réception d'une notification ou message
		                    </div>
		                </div>
		                <div class="custom-control custom-switch">
		                    <input type="checkbox" class="custom-control-input" id="customSwitch4" checked disabled />
		                    <label class="custom-control-label" for="customSwitch4"></label>
		                </div>
		            </div>
		        </div>
		    </li>
		    <li>
		        <a href="#" class="item">
		            <div class="in">
		                <div>Son de notification</div>
		                <span class="text-primary">Beep</span>
		            </div>
		        </a>
		    </li>
		</ul>

		<div class="listview-title mt-1">Profile</div>
		<ul class="listview image-listview text inset">

			<li>
		        <a href="#" class="item" onclick="action('nom')" >
		            <div class="in">
		                <div><b id="name">{{ $user->nom }} {{ $user->prenoms }}</b></div>
		                <span class="text-primary">Editer</span>
		            </div>
		        </a>
		    </li>
		    <li>
		        <a href="#" class="item" id="phone_number" onclick="action('phone_number')">
		            <div class="in">
		                <div>Téléphone : <b id="phone">{{ $user->phone_number }}</b id=""></div>
		                <span class="text-primary">Editer</span>
		            </div>
		        </a>
		    </li>
		    <li>
		        <a href="#" class="item" id="email" onclick="action('email')">
		            <div class="in">
		                <div>Mail : <b id="email">{{ $user->email }}</b id=""></div>
		                <span class="text-primary">Editer</span>

		            </div>
		        </a>
		    </li>

		    <li>
		        <a href="#" class="item" id="code_parrainage">
		            <div class="in">
		                <div>Code parrainage : <b id="email">{{ $user->code_affiliation->code }}</b id=""></div>
		                <span class="text-primary">Copier</span>

		            </div>
		        </a>
		    </li>
		</ul>

		<div class="listview-title mt-1">Securité</div>
		<ul class="listview image-listview text mb-2 inset">
		    <li>
		        <a href="#" class="item" onclick="action('password')">
		            <div class="in">
		                <div>Changer Mot de passe</div>
		            </div>
		        </a>
		    </li>
		    <!--li>
		    	<a href="{{ route('user.profile.verification', Auth::user()->id) }}" class="item">
			        
		            <div class="in">
		                <div>
		                    Vérification du numéro
		                </div>
		                <div class="custom-control custom-switch">
		                    <input type="checkbox" class="custom-control-input" id="customSwitch3"/>
		                    <label class="custom-control-label" for="customSwitch3"></label>
		                </div>
		            </div>
			        
		        </a>
		    </li-->
		    <li>
		        <a href="{{ route('logout') }}" class="item"  onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
		            <div class="in">
		                <div>Déconneter tous les comptes</div>
		            </div>
		        </a>
		    </li>
		</ul>

    	@include('frontend.profile.edit-modal')

    	<div id="loadingToast" class="toast-box toast-center">
            <div class="in">
                <ion-icon name="checkmark-circle" class="text-success"></ion-icon>
                <br>
                <div class="text">
                    Opération effectuée avec succès
                </div>
            </div>
            <button type="button" class="btn btn-dark mr-1 mb-1 close-button">OK</button>  
        </div>
		
	</div>

@endsection

@section('js')

	<script type="text/javascript">

		function action(arg) {

		    $.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
			
			$.get({

		        url: '/profile/edit/'+arg,
		        success: function (response) {

		            $("#editContent").html('<div class="overlay" id="editMsg"></div>'+response.html);
		            $('#edit').modal('show');
		        },
		         error: function (response) {
		             console.log(response);
		        }
		    });
		}


		function submit(arg) {

		    var data;
		    var action = false;

		    if (arg == "nom"){
		        data = {nom:$('#nom').val(), prenoms:$('#prenoms').val()};
		        action = true;
		    }
		 
		    if (arg == "phone_number"){
		        data = {phone_number:$('#phonenumber').val()};
		        action = true;
		    }

		    if (arg == "email"){
		        data = {email:$('#email').val()};
		        action = true;
		    }

		    if(arg == "password"){

		        data = {oldpassword:$('#oldpassword').val(), password:$('#password').val()};

		        if ($('#password').val() == $('#confirm').val())
		            action = true;
		        else
		            action = false;
		    }

		    $.ajaxSetup({

		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });

		    if(action){

		        $("#editMsg").html('<center>Enregistrement en cours ... <div class="spinner-border text-success" role="status"></div></center>');

		        $.post({

		            url: '/profile/update/'+arg,
		            data: data,
		            success: function (response) {

		            	toastbox('loadingToast', 3000);

		                $('#edit').modal('hide');
		                if (arg == "nom")
		                    $('#name').html($('#nom').val() +' '+ $('#prenoms').val());

		                if (arg == "phone_number")
		                    $('#phone').html($('#phonenumber').val());

		                if (arg == "email")
		                    $('#email').html($('#email').val());
		                
		            },
		            error: function (response) {

		                console.log(response.responseJSON);
		                $("#editMsg").html('<center><font color="red">'+response.responseJSON.msg_error+'</font></center>');
		            }
		        });
		    }else
		    	$("#editMsg").html('<center><font color="red"> Les mots de passe ne correspondent </font></center>');
		    	        
		}
	</script>
@endsection

