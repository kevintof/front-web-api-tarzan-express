@extends('frontend.layouts.app')


@section('title')
	SHOPPING
@endsection


@section('content')

	<div id="appCapsule">
		<div class="section mt-4">
	    
		    <div class="transactions">
		        <!-- item -->
		        <a href="{{ route('user.commande.create', Auth::user()->id) }}" class="item">
		            <div class="detail">
		                <img src="{{ asset('img/shopping/chinaShopping-commande.png') }}" alt="img" class="image-block imaged w48">

		                <div>
		                    <strong>Passer une commande</strong>
		                    <p>Passer directement votre commande si vous disposez des liens de vos produits</p>
		                </div>
		            </div>
		            <div class="right">
		                <div class="price text-danger"> > </div>
		            </div>
		        </a>
		        <!-- * item -->
		        <!-- item -->
		        <a href="{{ route('user.alibaba') }}" class="item">
		            <div class="detail">
		                <img src="{{ asset('img/shopping/chinaShopping-alibaba.png') }}" alt="img" class="image-block imaged w48">
		                <div>
		                    <strong>Alibaba</strong>
		                    <p>Visitez Alibaba et faite votre course</p>
		                </div>
		            </div>
		            <div class="right">
		                <div class="price text-danger"> > </div>
		            </div>
		        </a>
		        <!-- * item -->
		        <!-- item -->
		        <a href="{{ route('user.aliexpress') }}" class="item">
		            <div class="detail">
		                <img src="{{ asset('img/shopping/chinaShopping-aliexpress.png') }}" alt="img" class="image-block imaged w48">
		                <div>
		                    <strong>Aliexpress</strong>
		                    <p>Site dérivé de Alibaba, n'hésitez pas à faire un tour et passer une commande</p>
		                </div>
		            </div>
		            <div class="right">
		                <div class="price text-danger"> > </div>
		            </div>
		        </a>
		        <!-- * item -->
		        <!-- item -->
		        <a href="{{ route('user.taobao') }}" class="item">
		            <div class="detail">
		                <img src="{{ asset('img/shopping/chinaShopping-taobao.jpg') }}" alt="img" class="image-block imaged w48">
		                <div>
		                    <strong>Taobao</strong>
		                    <p>Site de vente populaire en chine. Trouver de très bons articles à de meilleurs prix</p>
		                </div>
		            </div>
		            <div class="right">
		                <div class="price text-danger"> > </div>
		            </div>
		        </a>
		        <!-- * item -->
		    </div>
		</div>
	</div>

	
@endsection