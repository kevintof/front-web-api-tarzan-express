@extends('frontend/layouts.appShopProducts')
@section('titre-page')
      TARZAN EXPRESS | ALIEXPRESS    
      
@endsection
@section('site')
	ALIEXPRESS
@endsection


@section('content')

	<div>
       	<iframe id="shopframe" src="https://m.fr.aliexpress.com" width="100%" height="1300" frameBorder="0"></iframe>
    </div>

@endsection

@section('js')
    <script type="text/javascript">

        function checkRequieredValue() {
            // body...

            var returnValue = true;
            $("#lien").val('document.getElementById("shopframe").contentWindow.location.href');



            if ($("#nom").val() == '' || $("#description").val() == '' || $("#lien").val() == '') {

                if ($("#nom").val() == ''){
                    $("#itemTitle").html('<ion-icon name="cube"></ion-icon> Nouveau produit &nbsp;&nbsp; <ion-icon color="danger" name="warning"></ion-icon>');

                    $("#nomMsg").html('<font color="red">Insérer le nom du produit</font>');
                }else{

                    $("#itemTitle").html('<ion-icon name="cube"></ion-icon>'+ $("#nom").val()+' &nbsp;&nbsp; <ion-icon color="danger" name="warning"></ion-icon>');

                    $("#nomMsg").html('');
                }

                if ($("#description").val() == '')
                    $("#descriptionMsg").html('<font color="red">Insérer une description du produit</font>');
                else
                    $("#descriptionMsg").html('');

                returnValue = false;
            }else{

                $("#itemTitle").html('<ion-icon name="cube"></ion-icon>'+ $("#nom").val());
                $("#nomMsg").html('');
                $("#descriptionMsg").html('');
                $("#lienMsg").html('');
            }

            if (!returnValue)
                toastbox('errorMsg', 2000);
            
            return returnValue;
        }

        function cartFormSubmit() {

            if (checkRequieredValue())
                document.getElementById('cart_form').submit();
        }
    </script>
@endsection