@extends('frontend.layouts.appHome')
 @section('titre-page')
   TARZAN EXPRESS | ACCUEIL
 @endsection
 @section('title')
    ACCUEIL
 @endsection
@section('content')

    <!-- Intro chinaShopping -->
    <div class="section wallet-card-section pt-1">

        <div class="carousel-single owl-carousel owl-theme">
            @foreach($articles as $article)
                <a href="{{ route('user.actualites.show', $article->id) }}">
                    <div class="item">
                        <div class="card" style="background-image: url('{{ asset('img/articles/'.$article->photo_avant) }}');">
                            <div class="card-body">
                                <h3 class="card-title">{{ $article->titre }}</h3>
                                <img src="{{ asset('img/articles/'.$article->photo_avant) }}" alt="image" width="40" height="120">
                                @php ($pos = strpos($article->contenu, ' ', 60))
                                <font class="text-dark"> {{ substr($article->contenu,0,$pos ) }} ... </font>
                            </div>
                        </div>
                    </div>
                </a>
            @endforeach
        </div> 
    </div>

    <br>

    <div class="section">
        <div class="wallet-card" style="background: url('{{ asset('img/app/background5.jpg') }}');">
            <!-- Balance -->
            <div class="balance">

                <div class="center">
                    <span class="title">Faites vos courses</span>
                    <h4 class="total">Maintenant sur</h4> 
                </div>
                <a href="{{ route('user.aliexpress') }}">

                    <img src="{{ asset('img/shopping/chinaShopping-aliexpress.png') }}" width="60" height="60" alt="image" class="image">
                </a>
            </div>
            <!-- * Balance -->

            <font color="black">Prise en main en quelques secondes</font>
            <!-- Wallet Footer -->
            <div class="wallet-footer">
                <iframe style="overflow: hidden; height: 100%; width: 100%;" src="https://www.youtube.com/embed/tgbNymZ7vqY?autoplay=1&mute=1"  frameBorder="0">
                </iframe>

            </div>
            <!-- * Wallet Footer -->
        </div>
    </div>

    <!-- menu -->
    <div class="section">
        <div class="row mt-2">
            <div class="col-6">
                <a href="{{ route('user.Informations.aide') }}">
                    <div class="item">
                        <div class="bill-box">
                            <div class="img-wrapper">
                                <img src="{{ asset('img/app/help.png') }}" alt="img" class="image-block imaged w64">
                            </div>
                            <h4>COMMENT CA MARCHE</h4>
                        </div>
                    </div>  
                </a>
            </div>
            
            <div class="col-6">
                <a href="{{ route('user.commande.create', Auth::user()->id) }}">
                    <div class="item">
                        <div class="bill-box">
                            <div class="img-wrapper">
                                <img src="{{ asset('img/app/order-add.png') }}" alt="img" class="image-block imaged w64">
                            </div>
                            <h4>NOUVELLE COMMANDE</h4>
                        </div>
                    </div>  
                </a>
            </div>
    
        </div>

        <div class="row mt-2">

            <div class="col-6">
                <a href="{{ route('user.commande.index', Auth::user()->id) }}">
                    <div class="item">
                        <div class="bill-box">
                            <div class="img-wrapper">
                                <img src="{{ asset('img/app/commandes.png') }}" alt="img" class="image-block imaged w64">
                            </div>
                            <h4>COMMANDES</h4>
                        </div>
                    </div>  
                </a>
            </div>

            <div class="col-6">
                <a href="{{ route('user.profile.index', Auth::user()->id) }}">
                    <div class="item">
                        <div class="bill-box">
                            <div class="img-wrapper">
                                <img src="{{ asset('img/app/compte-1.png') }}" alt="img" class="image-block imaged w64">
                            </div>
                            <h4>MON COMPTE</h4>
                        </div>
                    </div>  
                </a>
            </div>

        </div>
    </div>
    <!-- * Stats -->
    
    <!-- Livraison -->
    <div class="section mt-4">
        <div class="section-heading">
            <h2 class="title">Mode de livraison</h2>
            <a href="app-transactions.html" class="link">Découvrir</a>
        </div>
        <div class="transactions">
            <!-- item -->
            <a href="" class="item">
                <div class="detail">
                    <img src="{{ asset('img/app/plane.png') }}" alt="img" class="image-block imaged w48">
                    <div>
                        <strong>Aérien</strong>
                        <p>Express</p>
                    </div>
                </div>
                <div class="right">
                    <div class="price text-danger"> 5 à 15 jours </div>

                </div>
            </a>
            <!-- * item -->
            <!-- item -->
            <a href="" class="item">
                <div class="detail">
                    <img src="{{ asset('img/app/cargo-ship.png') }}" alt="img" class="image-block imaged w48">
                    <div>
                        <strong>Maritime</strong>
                        <p>Simple</p>
                    </div>
                </div>
                <div class="right">
                    <div class="price text-danger"> 30 à 45 jours </div>
                </div>
            </a>
            <!-- * item -->
            <!-- item -->
            <a href="" class="item">
                <div class="detail">
                    <img src="{{ asset('img/app/moving-truck.png') }}" alt="img" class="image-block imaged w48">
                    <div>
                        <strong>Livraison à domicile</strong>
                        <p>Service efficace</p>
                    </div>
                </div>
                <div class="right">
                    <div class="price text-danger"> > </div>
                </div>
            </a>
            <!-- * item -->
        </div>
    </div>
    <!-- * Livraison -->
    
    <!-- Promos -->
    <div class="section full mt-4">
        <div class="section-heading padding">
            <h2 class="title">Dernières commandes</h2>
            @if(count($commandes) != 0)
                <a href="{{ route('user.commande.index', Auth::user()->id) }}" class="link">Tout voir ...</a>
            @endif
        </div>
        @if(count($commandes) != 0)
        <div class="carousel-single owl-carousel owl-theme shadowfix">

            @foreach($commandes as $commande)
                <div class="item">

                    <!-- card block -->
                    <div class="card-block {{ $commande->statut->param3 }}">
                        <div class="card-main">
                            <div class="card-button dropdown">
                                <button type="button" class="btn btn-link btn-icon" data-toggle="dropdown">
                                    <ion-icon name="ellipsis-horizontal"></ion-icon>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="{{route('user.commande.chat', ['id'=>$commande->id])}}">
                                        <ion-icon name="mail-outline"></ion-icon>Discuter
                                    </a>
                                    <a class="dropdown-item" href="{{ route('user.commande.show', [Auth::user()->id, $commande->id]) }}">
                                        <ion-icon name="arrow-up-circle-outline"></ion-icon>Détails
                                    </a>
                                </div>
                            </div>
                            <div class="balance">
                                <span class="label">Montant commande</span>
                                @if ($commande->montant_commande != null)
                                    <h1 class="title">{{ $commande->montant_commande }} F CFA</h1>
                                @else
                                    <h1 class="title">NON DISPONIBLE</h1>
                                @endif
                                
                            </div>
                            <div class="in">
                                <div class="card-number">
                                    <span class="label">Etat</span>
                                    {{ $commande->statut->param1 }}
                                </div>
                                <div class="bottom">
                                    <div class="card-expiry">
                                        <span class="label">Date commande</span>
                                        {{ $commande->details_commande->date_commande }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- * card block -->
                </div>
            @endforeach

        </div>
        @else                

            <div class="section mt-2">
                <div class="card">
                    <div class="card-body">
                        <center>
                            <img src="{{ asset('img/app/close.png') }}" alt="img" class="image-block imaged w48">
                            AUCUNE COMMANDE ENREGISTREE
                        </center>
                        </p>
                    </div>
                </div>
            </div>

        @endif
    </div>

    <br>

    <!-- * Promos -->

    @if ($message = Session::get('success'))
        <div id="notification" class="toast-box toast-center show">
            <div class="in">
                <ion-icon name="checkmark-circle" class="text-success"></ion-icon>
                <div class="text">
                    {{$message}}
                </div>


                <a href="{{ route('user.profile.index', Auth::user()->id) }}" class="btn btn-secondary mr-1 mb-1"> VOIR MON PROFILE </a>
            </div>
            <button type="button" class="btn btn-dark mr-1 mb-1 close-button">OK</button>    
        </div>
    @endif

@endsection