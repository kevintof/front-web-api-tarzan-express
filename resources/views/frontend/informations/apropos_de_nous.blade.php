@extends('frontend.layouts.appInformation')
@section('content')

    
    <!-- Wallet Card -->
    <div class="section wallet-card-section pt-1">
        <div class="wallet-card bg-warning">
            <center> <img src="{{ asset('img/Tarzanexp.png') }}" alt="img" class="square imaged w140"> </center>                   
            <center> 
                <h1 class="text-light"> TarzanExpress </h1> 
                Version 1.0.1
            </center>
        </div>
    </div>
    <!-- Wallet Card -->

    <div class="section mt-5">
            <p align="justify">
                <font size="4" color="black" style="text-align: center;">
                    TarzanExpress est la première application en Afrique conçu pour vous accompagner et résoudre toutes vos difficultés par rapports aux commandes en chine. 

                    <br>

                    Grace à cette application intituive et simple d'utilisation, elle vous assure et garantie une meilleure expérience.

                    <br>
                    <br>

                    Grace à elle vous n'avez plus de soucis à vous faire pour le suivi de vos colis, ou encore les coûts élevés du transport de vos marchandises jusqu'à vous. 

                    <br>

                </font>
            </p>
            <br>

            <center>A ces fins nous vous offront les fonctions suivantes </center>
            <font size="4" color="black">
                <ul>
                    <li> Commande de marchandise sur AliExpress, Alibaba etc ...</li>
                    <li> Suivi de colis en temps réel</li>
                    <li> Discussion instantanée avec les administrateurs</li>
                    <li> Livraison à domicile</li>
                    <li> Paiement par Mobile money</li>
                </ul>
            </font>
       </div>

    <br><br>



    

@endsection