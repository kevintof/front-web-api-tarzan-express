<!-- Comment ca marche -->
<div class="section mt-1 mb-2">
    <div class="section-title"><h2 class="title">Comment ça marche?</h2></div>
    <div class="card">
        <!-- timeline -->
        <div class="timeline ml-3">
            <div class="item">
                <div class="dot bg-primary"></div>
                <div class="content">
                    <h4 class="title">Etape 1</h4>
                    <div class="text">Rechercher et choisissez les articles sur votre application chinaShopping (via les sites Taobao, Aliexpress, Alibaba). Vous pouvez directement coller le lien dde l'article.</div>
                </div>
            </div>
            
            <div class="item">
                <div class="dot"></div>
                <div class="content">
                    <h4 class="title">Etape 2</h4>
                    <div class="text">Ajoutez les produits et demander la validation des cotation de prix</div>
                </div>
            </div>
            <div class="item">
                <div class="dot bg-info"></div>
                <div class="content">
                    <h4 class="title">Etape 3</h4>
                    <div class="text">On vous envoie la cotation, vous valider la commande et passer au paiement (1 a 2 jours)</div>
                </div>
            </div>

            <div class="item">
                <div class="dot bg-primary"></div>
                <div class="content">
                    <h4 class="title">Etape 4</h4>
                    <div class="text">
                    	<p>Suivez votre commande.</p>
                    	<p>On reçois, verifie les articles et prépare le colis pour l'expédition de la chine à l'adresse de livraison</p>
                </div>
            </div>

            <div class="item">
                <div class="dot bg-primary"></div>
                <div class="content">
                    <h4 class="title">Etape 5</h4>
                    <div class="text">
                    	<p>On vous rensigne sur le prix de livraison</p>
                    	<p>Effectuer le paiment de l'envoie du colis par T-Money, Flooz (Option paiement à la livraison disponible)</p>
                    </div>
                </div>
            </div>

            <div class="item">
                <div class="dot bg-danger"></div>
                <div class="content">
                    <h4 class="title">Etape 6</h4>
                    <div class="text">
                        <p>Suivez votre colis en temps réel, et recevez votre colis à l'adresse de livraison.</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- * timeline -->
    </div>
</div>
<!-- * Comment ca marche -->