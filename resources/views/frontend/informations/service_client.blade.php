@extends('frontend.layouts.app')
@section('titre-page')
    TARZAN-EXPRESS | SERVICE-CLIENT
@endsection
	@section('title')
	  Service clients
	@endsection

	@section('notification')

		<a href="{{ route('user.panier', Auth::user()->id) }}" class="headerButton">
            <ion-icon name="cart"></ion-icon>
            <span class="badge badge-danger">{{ $cart_count }}</span>
        </a>

        <a href="{{route('user.notifications')}}" class="headerButton">
            <ion-icon class="icon" name="notifications"></ion-icon>
            <span class="badge badge-danger notification"></span>
        </a>

	@endsection


	@section('content')
         

		<div id="appCapsule">
            <div class="message-divider" id="{{ Auth::user()->id }}">
            </div>
            @foreach($messages as $message)
                @if($message->is_admin == false )
                    <div class="message-item user">
                        <div class="content">
                            <div class="bubble">
                                {{$message->contenu}}
                            </div>
                            <div class="footer">{{$message->date}}</div>
                        </div>
                    </div>
                @else
                    <div class="message-item">
                        <img src="{{ asset('img/logo-icon.png') }}" alt="avatar" class="avatar">

                        <div class="content">
                            <div class="title">
                                @if($message->is_admin == true)
                                    Service Tarzan Express
                                @else
                                    {{$commande->client->nom}}
                                @endif
                            </div>
                            <div class="bubble">
                                {{ $message->contenu }}
                            </div>
                            <div class="footer">{{$message->date}}</div>
                        </div>
                    </div>
             
                @endif
        
            @endforeach
        </div>

        <div class="chatFooter">
            <form>
                <a href="javascript:;" class="btn btn-icon btn-text-secondary rounded">
                    <ion-icon name="mail-outline" role="img" class="md hydrated" aria-label="camera"></ion-icon>
                </a>
                <div class="form-group basic">
                    <div class="input-wrapper">
                        <input type="text" class="form-control" placeholder="Saisissez un message">
                        <i class="clear-input">
                            <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                        </i>
                    </div>
                </div>
                <button type="button" class="btn btn-icon btn-primary rounded">
                    <ion-icon name="arrow-forward-outline" role="img" class="md hydrated" aria-label="arrow forward outline"></ion-icon>
                </button>
            </form>
       </div>

       <div class="modal fade dialogbox" id="DialogBasic" data-backdrop="static" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Cette discussion est dédiée pour vous assister dans l'utilisation globale de l'application. Pour toute discussion liée à une commande veuillez accéder aux discussions dans la section de détails de commande  </h5>
                    </div>
                    <div class="modal-body">
                        Souhaitez vous continuer la discussion ?
                    </div>
                    <div class="modal-footer">
                        <div class="btn-inline">
                            <a href="#" class="btn btn-text-secondary" data-dismiss="modal">Continuer</a>
                            <a href="{{route('user.commande.messages')}}" class="btn btn-text-primary" >Messagerie des commandes</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>	   
	@endsection
    
    @section('js')

        <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
        <script>
            $(document).ready(function() {
               $('.appBottomMenu').remove();
            });

            setTimeout(function(){  $("#DialogBasic").modal(); }, 3000);
            var pusher = new Pusher('710a42ce02a3f4836392');
            var channel = pusher.subscribe('chat_client_channel');
            channel.bind('chat_client', function(data) {
           
            var chatmess = '<div class="message-item">';
            var chatmess1 = '<div class="message-item user">';

            if(data.user_id == $('.message-divider').attr("id") && data.is_admin ==false ){

                chatmess1 =chatmess1 + '<div class="content"> <div class="title">'+''+'</div><div class="bubble">'+data.contenu+'</div><div class="footer">'+data.date+'</div> </div></div>'
                $('#appCapsule').append(chatmess1);    
            }

            if(data.user_id == $('.message-divider').attr("id") && data.is_admin == true ){

                user = $('.message-divider').attr("id")
                chatmess =chatmess +'<img src="{{ asset('img/logo-icon.png') }}" alt="avatar" class="avatar"><div class="content"><div class="title">'
                 if(user != null)
                 {
                     chatmess = chatmess +'Service Tarzan Express'+'</div><div class="bubble">' + data.contenu+'</div><div class="footer">10:44 AM</div></div></div>'
                 }
                 else{
                     
                 
                    }
                $('#appCapsule').append(chatmess);
            }

            
            
            $('.input_send_holder').html('<input type = "submit" value = "Send" class = "btn btn-primary input_send" />');
            
            $("#appCapsule").scrollTop($("#appCapsule")[0].scrollHeight);
            $(".spinner-border").remove();
            $("html,body").animate({scrollTop: $('#appCapsule .message-item:last').offset().top - 30});
            $(".form-control").removeAttr('disabled');
            $('.form-control').attr('placeholder','Saisissez un message');
            });
            function ajaxSend(ajax_data) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
               });

                $.post({
                    
                    url: "{{route('user.serviceClient.send',['id'=> Auth::id()])}}",
                    dataType: "json",
                    data: ajax_data,
                    success: function(response, textStatus, jqXHR) {
                        
                        
                    },
                    error: function(msg) {}
                });
            }

            $( ".rounded" ).click(function() {
               
                ajaxData = {contenu: $('.chatFooter .form-control').val() }
                $('.chatFooter').prepend('<div class="spinner-border text-primary" role="status"></div>')
                $('.form-control').attr('disabled','disabled');
                $('.form-control').attr('placeholder','En cours d\'envoi');
               if(ajaxData.contenu != null || ajaxData.contenu!='')
                {
                 ajaxSend(ajaxData);
                }
               $('.chatFooter .form-control').val('');
            });

        </script> 

	@endsection