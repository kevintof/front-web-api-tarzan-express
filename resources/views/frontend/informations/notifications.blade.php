@extends('frontend.layouts.app')

    @section('titre-page')
       TARZAN-EXPRESS | NOTIFICATIONS
    @endsection
	@section('title')
		 NOTIFICATIONS
	@endsection

	@section('notification')

    	<a href="{{route('user.notifications')}}" class="headerButton">
            <ion-icon class="icon" name="notifications"></ion-icon>
            <span class="badge badge-danger notification"></span>
        </a>
	@endsection


	@section('content')

		<div id="appCapsule">
        <div class="section mt-4">
        <div class="section-heading">
           <!-- <h2 class="title">Statuts des commandes</h2> -->
           <!-- <a href="app-transactions.html" class="link">Découvrir</a>-->
        </div>
        <!--  -->
        <div class="section full">

            <ul class="listview image-listview flush">
               
                
            @if($notifications)
                @foreach($notifications as $notification)
                    @if($notification->type=="ETAT_COMMANDE")
                    
                        <!-- <a id="{{$notification->id}}" href="{{route('user.commande.show', ['u_id' => Illuminate\Support\Facades\Auth::user()->id,'id'=>$notification->commande_id])}}" class="item">
                            <div class="detail">
                                
                                <div>
                                  @if($notification->type =="ETAT_COMMANDE")
                                    <strong>Votre commande {{$notification->commande->reference}} est {{$notification->commande->statut->param1}}</strong>
                                    <p>{{$notification->date}}</p>
                                    @endif
                                </div>
                            </div>
                            <div class="right">
                                <div class="price text-danger"> &gt; </div>
                            </div>
                        </a> -->
                        
                        <li class=" @if($notification->seen == false) {{'active'}} @endif"  >
                    <a id="{{$notification->id}}" href="javascript:;" class="item notification" data-redirect="{{route('user.commande.show', ['id'=>$notification->commande_id])}}">
                        <div class="icon-box bg-danger">
                            <ion-icon name="key-outline" role="img" class="md hydrated" aria-label="key outline"></ion-icon>
                        </div>
                        <div class="in">
                            <div>
                            @if($notification->type =="ETAT_COMMANDE")
                                <div class="mb-05"><strong>Votre commande {{$notification->commande->reference}} est   {{$notification->commande_statut->param1}} </strong></div>
                                <div class="text-small mb-05"></div>
                                <div class="text-xsmall">{{$notification->date}}</div>
                             

                            @endif
                            </div>
                            @if($notification->seen == false)<span class="badge badge-primary badge-empty"></span> @endif
                        </div>
                    </a>
                </li>
                    @endif
                    @if($notification->type=="MESSAGERIE")
                    <li>
                    <a id="{{$notification->id}}" href="{{route('user.commande.chat', ['id'=>$notification->commande_id])}}" class="item">
                        <div class="icon-box bg-info">
                            <ion-icon name="mail-outline" role="img" class="md hydrated" aria-label="key outline"></ion-icon>
                        </div>
                        <div class="in">
                            <div>
                            
                                <div class="mb-05"><strong>Vous avez un nouveau message à propos de la commande {{$notification->commande->reference}}</strong></div>
                                <div class="text-small mb-05"></div>
                                <div class="text-xsmall">{{$notification->date}}</div>
                             

                            
                            </div>
                             {{-- <span class="badge badge-primary badge-empty"></span> --}} 
                        </div>
                    </a>
                </li>  
                    @endif

                @endforeach
            @endif
                
                
                
                
                
            </ul>

        </div>
    </div>

	        
	    </div>
	@endsection
    @section('js')
        <script src="{{ asset('js/lib/jquery-3.4.1.min.js') }}"></script>
        <script src="{{asset('app-js/notification/notification.js')}}">  
        </script>
        <script>
          $(document).ready(function(){

              $('.notification').click(function(){
                 update_notification_is_seen_or_clicked($(this).attr('id'))
                location.href = $(this).data('redirect');
               
              })
              
              function update_notification_is_seen_or_clicked(id){
                
                $.get({
                    url: "/notifications/is_clicked/"+id,
                    success: function(data){

                    },
                    error:function(msg){}

                })
              }
          })
        </script>

	@endsection
