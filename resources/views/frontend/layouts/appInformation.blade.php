@include('frontend.header')

    <!-- loader -->
    @include('frontend.appLogoLoader')
    <!-- * loader -->

    <!-- App Header -->
    <div class="appHeader bg-secondary text-ligh">
        <div class="left">
            <a href="#" class="headerButton goBack">
                <ion-icon name="chevron-back-outline"></ion-icon>
            </a>
        </div>
        <div class="pageTitle">
            A PROPOS DE NOUS
        </div>
    </div>
    <!-- * App Header -->

    <!-- App Content -->
    <div id="appCapsule" style="background: #ffffff">

        @yield('content')

        <!-- app footer Copyright-->
        @include('frontend.appFooterCopyright')
        <!-- * app footer -->
    </div>
    <!-- * App Content -->


    <!-- App Bottom Menu -->
    @include('frontend.appBottomMenu')
    <!-- * App Bottom Menu -->

@include('frontend.footer')