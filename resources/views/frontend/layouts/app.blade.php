@include('frontend.header')

    <!-- loader -->
    @include('frontend.appLogoLoader')
    <!-- loader -->

    <!-- App Header -->
    <div class="appHeader bg-secondary text-light">
        <div class="left">
            <a href="#" class="headerButton goBack">
                <ion-icon name="chevron-back-outline"></ion-icon>
            </a>
        </div>
        <div class="pageTitle">
            @yield('title')
        </div>
        <div class="right">
            @yield('notification')
        </div>
    </div>
    <!-- App Header -->

    <!-- App Capsule -->
    @yield('content')
    <!-- App Capsule -->


    <!-- App Bottom Menu -->
    @include('frontend.appBottomMenu')
    <!-- App Bottom Menu -->


@include('frontend.footer')