@include('frontend.header')

    <!-- loader -->
    @include('frontend.appLogoLoader')
    <!-- * loader -->

    <!-- App Header -->
    <div class="appHeader">
        <div class="left">
            <a href="#" class="headerButton goBack">
                <ion-icon name="chevron-back-outline"></ion-icon>
            </a>
        </div>
        <div class="pageTitle">
            @yield('site')
        </div>
        <div class="right">
            <a href="#" type="button" class="btn btn-outline-primary mr-1 mb-1" data-toggle="modal" data-target="#addNewProductfield">
                <img src="{{ asset('img/app/cart-addin.png') }}" alt="img" class="square imaged w24"> &nbsp; </a>
        </div>
    </div>
    <!-- * App Header -->

    <!-- MODAL PRODUCT ADD TO SHOP -->
    <div class="modal fade action-sheet inset" id="addNewProductfield" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Insérer les informations</h5>
                </div>
                <div class="modal-body">
                    <div class="action-sheet-content">
                        <form id="cart_form" method="POST" action="{{ route('user.panier', Auth::user()->id) }}"  enctype="multipart/form-data">
                            @csrf

                            <div class="form-group boxed">
                                <div class="input-wrapper">
                                    <div id="nomMsg"></div>
                                    <input type="text" class="form-control" id="nom" name="nom" placeholder="* Nom du produit" onchange="setNewItemProductTitle()">
                                    <i class="clear-input" onclick="resetNewItemProductTitle()">
                                        <ion-icon name="close-circle"></ion-icon>
                                    </i>
                                </div>
                            </div>

                            <div class="form-group boxed">
                                <div class="input-wrapper">
                                    <div id="descriptionMsg"></div>
                                    <textarea id="description" name="description" rows="3" class="form-control"
                                        placeholder="* Description (Toutes les précisions sur le produit)"></textarea>
                                    <i class="clear-input">
                                        <ion-icon name="close-circle"></ion-icon>
                                    </i>
                                </div>
                            </div>

                            <div class="form-group boxed">
                                <div class="input-wrapper">
                                    <div id="lienMsg"></div>
                                    <input type="hidden" class="form-control" id="lien" name="lien" placeholder="* Lien">
                                    <i class="clear-input">
                                        <ion-icon name="close-circle"></ion-icon>
                                    </i>
                                </div>
                            </div>

                            <div class="form-group boxed">
                                <div class="input-wrapper">
                                    <input type="number" class="form-control" id="quantite" name="quantite" value="1">
                                    <i class="clear-input">
                                        <ion-icon name="close-circle"></ion-icon>
                                    </i>
                                </div>
                            </div>

                            <div class="custom-file-upload">
                                <input type="file" id="images" name="images" accept=".png, .jpg, .jpeg">
                                <label for="images">
                                    <span>
                                        <strong>
                                            <ion-icon name="arrow-up-circle-outline"></ion-icon>
                                            <i>Insérer une image du produit</i>
                                        </strong>
                                    </span>
                                </label>
                            </div>

                            <br>

                            <button type="button" class="btn btn-success btn-block btn-lg" onclick="cartFormSubmit()">Ajouter au panier</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- * MODAL PRODUCT ADD TO SHOP -->


    <!-- App Content -->
    <div id="appCapsule">
        @yield('content')    
    </div>
    <!-- * App Content -->  


    <!-- App Bottom Menu -->
    @include('frontend.appBottomMenu')
    <!-- * App Bottom Menu -->

@include('frontend.footer')