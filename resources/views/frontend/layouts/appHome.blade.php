@include('frontend.header')

    <!-- loader -->
    @include('frontend.appLogoLoader')
    <!-- * loader -->

    <!-- App Header -->
    @include('frontend.appNotificationHeader')
    <!-- * App Header -->

    <!-- App Content -->
    <div id="appCapsule">

        @yield('content')

        <!-- app footer Copyright-->
        @include('frontend.appFooterCopyright')
        <!-- * app footer -->
    </div>
    <!-- * App Content -->


    <!-- App Bottom Menu -->
    @include('frontend.appBottomMenu')
    <!-- * App Bottom Menu -->


    <!-- App Sidebar -->
    @include('frontend.appSidebar')
    <!-- * App Sidebar -->

@include('frontend.footer')