@extends('frontend.layouts.app')
	@section('titre-page')
    	TARZAN EXPRESS | ACTUALITES
	@endsection
	@section('title')
		ACTUALITES
	@endsection

	@section('notification')

		<a href="{{ route('user.panier', Auth::user()->id) }}" class="headerButton">
            <ion-icon name="cart"></ion-icon>
            <span class="badge badge-danger">{{ $cart_count }}</span>
        </a>

    	<a href="{{route('user.notifications')}}" class="headerButton">
            <ion-icon class="icon" name="notifications"></ion-icon>
            <span class="badge badge-danger notification"></span>
        </a>
        
	@endsection


	@section('content')

		<div id="appCapsule">

			<div class="section mt-2">
	            <div class="card">
	                <div class="card-body">

	                	<ul class="nav nav-tabs lined" role="tablist">
		                    <li class="nav-item">
		                        <a class="nav-link active" data-toggle="tab" href="#promo" role="tab">
		                            <img src="{{ asset('img/app/promo.png') }}" alt="img" class="image-block imaged w24">
		                            &nbsp;&nbsp; PROMO
		                        </a>
		                    </li>
		                    <li class="nav-item">
		                        <a class="nav-link" data-toggle="tab" href="#info" role="tab">
		                            <img src="{{ asset('img/app/info-1.png') }}" alt="img" class="image-block imaged w24">
		                            &nbsp;&nbsp; INFO
		                        </a>
		                    </li>
		                    <li class="nav-item">
		                        <a class="nav-link" data-toggle="tab" href="#divers" role="tab">
		                            <img src="{{ asset('img/app/divers.png') }}" alt="img" class="image-block imaged w24">
		                            &nbsp;&nbsp; DIVERS
		                        </a>
		                    </li>
		                </ul>
		                <div class="tab-content mt-2">
		                    <div class="tab-pane fade show active" id="promo" role="tabpanel">
		                    	<div class="section tab-content mt-2 mb-2">
            						<div class="row">

            							@if(count($promos) > 0 )
					                        @foreach($promos as $promo)
								                <div class="col-6 mb-2">
								                    <a href="{{ route('user.actualites.show', $promo->id) }}">
								                        <div class="blog-card">
								                            <img src="{{ asset('img/articles/'.$promo->photo_avant) }}" alt="image" class="imaged w-100">
								                            <div class="text">
								                                <h2 class="title"> {{ $promo->titre }} <br> {{ $promo->contenu }} </h2>
								                            </div>
								                        </div>
								                    </a>
								                </div>
							                @endforeach
							            @else

							            	AUCUNE PROMOTION DISPONIBLE
							            @endif
							        </div>


					                <!--div>
						                <a href="javascript:;" class="btn btn-block btn-primary btn-lg">Découvrir plus ...</a>
						            </div-->
							    </div>

		                    </div>
		                    <div class="tab-pane fade" id="info" role="tabpanel">
		                        <div class="section tab-content mt-2 mb-2">
            						<div class="row">
										@if(count($infos) > 0 )

											@foreach($infos as $info)
								                <div class="col-6 mb-2">
								                    <a href="{{ route('user.actualites.show', $info->id) }}">
								                        <div class="blog-card">
								                            <img src="{{ asset('img/articles/'.$info->photo_avant) }}" alt="image" class="imaged w-100">
								                            <div class="text">
								                                <h2 class="title"> {{ $info->titre }} <br> {{ $info->contenu }} </h2>
								                            </div>
								                        </div>
								                    </a>
								                </div>
							                @endforeach
										@else

							            	AUCUNE INFO DISPONIBLE
							            @endif

							        </div>


					                <!--div>
						                <a href="javascript:;" class="btn btn-block btn-primary btn-lg">Découvrir plus ...</a>
						            </div-->
							    </div>
		                    </div>
		                    <div class="tab-pane fade" id="divers" role="tabpanel">
		                        <div class="section tab-content mt-2 mb-2">
            						<div class="row">
            							@if(count($infos) > 0 )

            								@foreach($divers as $divers)
								                <div class="col-6 mb-2">
								                    <a href="{{ route('user.actualites.show', $article->id) }}">
								                        <div class="blog-card">
								                            <img src="{{ asset('img/articles/'.$article->photo_avant) }}" alt="image" class="imaged w-100">
								                            <div class="text">
								                                <h2 class="title"> {{ $article->titre }} <br> {{ $article->contenu }} </h2>
								                            </div>
								                        </div>
								                    </a>
								                </div>
							                @endforeach

										@else

							            	AUCUN DIVERS DISPONIBLE
							            @endif

				                        
							        </div>


					                <!--div>
						                <a href="javascript:;" class="btn btn-block btn-primary btn-lg">Découvrir plus ...</a>
						            </div-->
							    </div>
		                    </div>
		                </div>
	                </div>
	            </div>
	        </div>
				        
	    </div>
	@endsection