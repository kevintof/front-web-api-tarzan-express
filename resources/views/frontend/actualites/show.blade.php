@extends('frontend.layouts.app')

	@section('title')
		TARZAN-EXPRESS | ACTUALITES
	@endsection

	@section('notification')

    	<a href="{{ route('user.panier', Auth::user()->id) }}" class="headerButton">
            <ion-icon name="cart"></ion-icon>
            <span class="badge badge-danger">{{ $cart_count }}</span>
        </a>

    	<a href="{{route('user.notifications')}}" class="headerButton">
            <ion-icon class="icon" name="notifications"></ion-icon>
            <span class="badge badge-danger notification"></span>
        </a>
        
	@endsection


	@section('content')

		<div id="appCapsule">

			<div class="section mt-2">
				<h2>
                	<center> {{$article->titre}} </center>
	            </h2>
			</div>

			<div class="section mt-2">
				<figure>
	                <img src="{{ asset('img/articles/'.$article->photo_avant) }}" alt="image" class="imaged img-fluid">
	            </figure>
	            <p>
	                <font class="text-dark"> {{$article->contenu}} </font>
	            </p>
	            
	        </div>
	        <div class="section mt-2">

	        	<div class="blog-header-info mt-2 mb-2">
	                <div>
	                    <img src="{{ asset('img/logo-icon.png') }}" alt="img" class="imaged w24 rounded mr-05">
	                    Par <a href="#"> TARZAN EXPRESS </a>
	                </div>

	                <div>
	                   Date <a href="#"> {{$article->created_at}} </a>
	                </div>
	            </div>
			</div>
	            

		</div>
	@endsection