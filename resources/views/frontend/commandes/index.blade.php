@extends('frontend.layouts.app')


	@section('title')
		 VOS COMMANDES
	@endsection
    @section('titre-page')
		TARZAN-EXPRESS | VOS COMMANDES
	@endsection
	@section('notification')

		<a href="{{ route('user.panier', Auth::user()->id) }}" class="headerButton">
            <ion-icon name="cart"></ion-icon>
            <span class="badge badge-danger"> {{ $cart_count }} </span>
        </a>

    	<a href="{{route('user.notifications')}}" class="headerButton">
            <ion-icon class="icon" name="notifications"></ion-icon>
            <span class="badge badge-danger notification"></span>
        </a>
	@endsection


	@section('content')

		<div id="appCapsule">
			@if(count($commandes) != 0)
	        	@foreach ($commandes as $commande)
			        <div class="section mt-2">
			        	
			        	<!-- card block -->	
			            <div class="card-block {{ $commande->statut->param3 }} mb-2">
			                <div class="card-main">
			                    <div class="card-button dropdown">
			                        <button type="button" class="btn btn-link btn-icon" data-toggle="dropdown">
			                            <ion-icon name="ellipsis-horizontal"></ion-icon>
			                        </button>
			                        <div class="dropdown-menu dropdown-menu-right">
			                            <a class="dropdown-item" href="{{route('user.commande.chat', ['id'=>$commande->id])}}">
			                                <ion-icon name="mail-outline"></ion-icon>Discuter
			                            </a>
			                            <a class="dropdown-item" href="{{ route('user.commande.show', ['id'=> $commande->id]) }}">
			                                <ion-icon name="arrow-up-circle-outline"></ion-icon>Détails
			                            </a>
			                        </div>
			                    </div>
			                    <div class="balance">
			                        <span class="label">Montant commande</span>
			                        @if ($commande->montant_commande != null)
			                        	<h1 class="title">{{ $commande->montant_commande }} F CFA</h1>
			                        @else
			                        	<h1 class="title">NON DISPONIBLE</h1>
									@endif
			                
			                    </div>
			                    <div class="in">
			                        <div class="card-number">
			                            <span class="label">Etat</span>
			                            {{ $commande->statut->param1 }}
			                        </div>
			                        <div class="bottom">
			                            <div class="card-expiry">
			                                <span class="label">Date commande</span>
			                                {{ $commande->details_commande->date_commande }}
			                            </div>
			                            <div class="card-ccv">
			                                <span class="label">Date livraison</span>
			                                @if ($commande->date_livraison != null)
					                        	{{ $commande->date_livraison }}
					                        @else
					                        	INCONNU
											@endif
			                            </div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			            <!-- * card block -->
					</div>
				@endforeach
	        @else
		        <!-- menu -->
			    <div class="section">

			    	<br>
			    	<br>

	        		<div class="row mt-2">
			            <div class="col-12">
			                <center>
	        					<img src="{{ asset('img/app/order-empty.png') }}" alt="img" class="imaged w140">
	        					<br>

	        					<h2>AUCUNE COMMANDE ENREGISTREE</h2>
	        				</center> 
			            </div>
			        </div>

			        <br>
			    	<br>

			        <div class="row mt-2">
			            <div class="col-6">
			                <a href="{{ route('user.Informations.aide') }}">
			                    <div class="item">
			                        <div class="bill-box">
			                            <div class="img-wrapper">
			                                <img src="{{ asset('img/app/help.png') }}" alt="img" class="image-block imaged w64">
			                            </div>
			                            <h4>COMMENT CA MARCHE</h4>
			                        </div>
			                    </div>  
			                </a>
			            </div>
			            
			            <div class="col-6">
			                <a href="{{ route('user.commande.create', Auth::user()->id) }}">
			                    <div class="item">
			                        <div class="bill-box">
			                            <div class="img-wrapper">
			                                <img src="{{ asset('img/app/order-add.png') }}" alt="img" class="image-block imaged w64">
			                            </div>
			                            <h4>NOUVELLE COMMANDE</h4>
			                        </div>
			                    </div>  
			                </a>
			            </div>
			        </div>			        
			    </div>
			@endif
	        
		    @if ($message = Session::get('success'))
		        <div id="notification" class="toast-box toast-center show">
		            <div class="in">
		                <ion-icon name="checkmark-circle" class="text-success"></ion-icon>
		                <div class="text">
		                    Votre commande a été prise en compte avec succès
		                </div>
                		
		                <a type="button" class="btn btn-secondary mr-1 mb-1"  href="{{ route('user.commande.show', [Auth::user()->id, $commande->id]) }}">Voir détails</a>
		            </div>

                    <button type="button" class="btn btn-dark mr-1 mb-1 close-button">OK</button>    

		        </div>
		    @endif

	        
	    </div>
	@endsection