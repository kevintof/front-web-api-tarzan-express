@extends('frontend.layouts.app')


	@section('title')
		TARZAN-EXPRESS | RECAP DE VOTRE COMMANDE
	@endsection


	@section('content')

		<div id="appCapsule">

			@php($coli = $commande->details_commande->colis[0])

			<!-- toast top -->
	        <div id="security-message" class="toast-box toast-top">
	            <div class="in">
	                <div class="text">
	                    Vous serez redirigé sur une page sécurisée pour le paiement de votre facture. Votre numéro de téléphone est encrypté et toutes les communications sont encryptées et signées via SSL/TLS. <br>
	                    Soyez donc tranquille vos données sont sécurisées et vous restent confidentielles.
	                </div>
	            </div>
	            <button type="button" class="btn btn-sm btn-text-light close-button">OK</button>
	        </div>
	        <!-- * toast top -->

			<div class="section mt-2">
	        	<div class="card">

	                <div class="card-body">
			            <h5 class="card-title"><ion-icon name="pin"></ion-icon> Livraison 
			            	<span class="text-secondary">{{ $coli->type_livraison->param1 }}</span>

			            	<!--button type="button" class="btn btn-icon btn-secondary mr-1" style="float:right;" onclick="updateAdresse({{ $coli->id }})">
		                    	<ion-icon name="create"></ion-icon>
		                    </button-->

			            </h5>
	                    <p class="card-text">
	                    	@if($coli->adresse_livraison->id == 1)
	                    		<span class="text-warning">Récupération du colis à l'agence </br></span>
	                    	@else
	                    		<span class="text-warning">Livraison à domicile </br></span>
	                    	@endif
	                       {{ $coli->adresse_livraison->ville }} {{ $coli->adresse_livraison->pays }} </br>
	                       {{ $coli->adresse_livraison->quartier }} </br>
	                       {{ $coli->adresse_livraison->adresse1 }} </br>
	                       {{ $coli->adresse_livraison->adresse2 }} </br>
	                    </p>
	        		</div>
        		</div>
	        </div>

			<div class="section mt-2">
	        	<div class="card">
                	<div class="section full">
						<ul class="listview image-listview flush">
			                @foreach ($coli->produits as $produit)
				                <li>
				                    <a href="{{ $produit->lien }}" class="item" target="_blank">
				                        <div>
				                        	@if($produit->images != '')
				                            	<img src="{{ asset('/commandes-colis/'.Auth::user()->id.'/'.$produit->images) }}" alt="img" class="image-block imaged w48">
				                            @else
				                            	<img src="{{ asset('img/sample/brand/1.jpg') }}" alt="img" class="image-block imaged w48">
				                            @endif
				                        </div>
				                        <div class="in">
				                            <div>
				                                <div class="mb-05"><strong>{{ $produit->nom }}</strong></div>
				                                <div class="text-small mb-05">&nbsp;&nbsp;&nbsp;&nbsp;x {{ $produit->quantite }}</div>
				                            </div>
				                            <div><b>{{ $produit->montant }} F CFA</b></div>
				                        </div>
				                    </a>
				                </li>
							@endforeach
						</ul>
			        </div>
        		</div>
	        </div>
			
	        <div class="section mt-2">
	        	<div class="card">
	                <div class="card-body">
			            <ul class="listview flush transparent simple-listview no-space mt-3">
			                <li>
			                    <strong>Référence</strong>
			                    <span id="reference">{{ $commande->reference }}</span>
			                </li>
			                <li>
			                    Nombre de produits
			                    <span><strong>{{ $nbre_produit }}</strong></span>
			                </li>
			                <li>
			                    Montant commande
			                    <span><strong>{{ $commande->montant_commande }} CFA</strong></span>
			                </li>
			                <li>
			                    Transport (Chine - Togo)
			                    <span><strong>{{ $coli->transport }} CFA</strong></span>
			                </li>
			                @if($coli->adresse_livraison_id != 1)
			                	<li>
			                    	Livraison à domicile
			                    	<span><strong>{{ $coli->frais_livraison }} CFA</strong></span>
			                	</li>
			                @endif
			                <li>
			                    Frais Service
			                    <span><strong>{{ $commande->montant_service }} CFA</strong></span>
			                </li>
			        	</ul>


			                <div class="alert alert-outline-danger mb-1" role="alert">
								Total
			                    <span style="float: right;">
			                    	<strong>{{ $commande->montant_livraison+$commande->montant_commande+$commande->montant_service }} CFA</strong>
			                    </span>
			                </div>
	        		</div>
        		</div>
	        </div>

	        <div class="section mt-2">
            	<a class="btn btn-danger mr-1 mb-1" id="cancelOrder">Annuler</a>
	            <a href="#" data-toggle="modal" data-target="#paiement"class="btn btn-success mr-1 mb-1" id="validateOrder" style="float: right;">Passer la commande</a>
	        </div>
	    <!-- Exchange Action Sheet -->
        <div class="modal fade action-sheet" id="paiement" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">PAIEMENT DE LA FACTURE</h5>
                    </div>
                    <div class="modal-body">
                        <div class="action-sheet-content">
                        	<label>
			                    <font color="black"><strong> Je veux payer </strong></font>
			                </label>
                            <form method="POST" action="{{ route('user.transaction.store') }}">
                            	@csrf

                            	@php( $key = $typePaiements[0]->param3 )
                            	@php($idenfifier = 0)

                            	@if($commande->details_commande->transaction != null)

                            		@php($idenfifier = $commande->details_commande->transaction->reference)

                            	@endif

                            	<input type="hidden" name="identifier" id="identifier" value="{{$idenfifier}}">
                            	<input type="hidden" name="cmd_id" id="cmd_id" value="{{$commande->id}}">

                                <div class="btn-group btn-group-toggle" data-toggle="buttons">
			                        <label class="btn btn-outline-primary active">
			                            <input type="radio" name="optionPaiement" id="option1" value="montant_total" onchange="montantApayer({{ $commande->id}})">La totatlité du montant
			                        </label>
			                        <label class="btn btn-outline-primary">
			                            <input type="radio" name="optionPaiement" id="option2" value="montant_commande" onchange="montantApayer({{ $commande->id}})">Le transport à la livraison
			                        </label>
			                    </div>

			                    <div class="section full mt-5" id="transportInfo">
						            
						        </div>

			                    <div class="mt-1"></div>

                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group basic">
                                            <div class="input-wrapper">
                                                <label class="label" for="network">Opération</label>
                                                <select class="form-control custom-select" id="network" name="network">
                                                    @foreach ($typePaiements as $typePaiement)
                                                    	<option value="{{$typePaiement->param1}}">{{$typePaiement->param1}}</option>
													@endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-6">
                                        <div class="form-group basic">
		                                    <label class="label">Numéro de telephone</label>
		                                    <div class="input-group mb-2">
		                                        <div class="input-group-prepend">
		                                            <span class="input-group-text" id="input1"></span>
		                                        </div>
		                                        <input type="phone_number" class="form-control form-control-lg" id="phone_number" name="phone_number" required>
		                                    </div>
		                                </div>
                                    </div>
                                </div>

                                <div class="form-group basic">
                                    <label class="label">Montant à prélever</label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="input1">F CFA</span>
                                        </div>
                                        <input type="number" class="form-control form-control-lg" id="montant" name="montant" value="{{ $commande->montant_livraison+$commande->montant_commande+$commande->montant_service }}" style="float: right;" readonly="readonly">
                                    </div>
							        							            
                                </div>

                                <div class="form-group basic">
                                    <button type="submit" class="btn btn-success btn-block btn-lg" id="validate">Effectuer le paiement</button>
                                </div>

                            </form>


                            <div id="loadingToast" class="toast-box toast-center">
				                <div class="in">
				                    <div class="spinner-border text-success" role="status"></div>
				                    <br>
				                    <div class="text">
				                        Patientez SVP ...
				                    </div>
				                </div>
				            </div>
				            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- * Exchange Action Sheet --> 

        </div>

	@endsection

	@section('js')
		<script src="{{ asset('app-js/commande/recap.js') }}"></script>
	@endsection