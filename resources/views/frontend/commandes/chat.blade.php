@extends('frontend.layouts.app')

    @section('titre-page')
     TARZAN EXPRESS | DISCUSSIONS    

    @endsection
	@section('title')
		{{$commande->reference}}
    @endsection
    

	@section('notification')

		<a href="{{ route('user.panier', Auth::user()->id) }}" class="headerButton">
            <ion-icon name="cart"></ion-icon>
            <span class="badge badge-danger">{{ $cart_count }}</span>
        </a>

    	<a href="{{route('user.notifications')}}" class="headerButton">
            <ion-icon class="icon" name="notifications"></ion-icon>
            <span class="badge badge-danger notification"></span>
        </a>
	@endsection


	@section('content')
         

		<div id="appCapsule">
            <div class="message-divider" id="{{$commande->id}}">
                
            </div>
            @foreach($messages as $message)
            @if($message->is_admin == false)
                <div class="message-item user">
                <div class="content">
                    <div class="bubble">
                        @if($message->image == null)
                           {{$message->contenu}}
                         @else
                          <a class="imageModal" href="#" data-imgurl="{{asset('messages-commandes/'.$commande->id.'/'.$message->image)}}" data-toggle="modal" data-target="#DialogImage" id="{{'img_'.$message->id}}" onClick="appendInModal({{$message->id}})">
                           <img src="{{asset('messages-commandes/'.$commande->id.'/'.$message->image)}}" class="imaged w160" >
                          </a>          
                        @endif
                    </div>
                    <div class="footer">{{$message->date}}</div>
                </div>
            </div>
            @else
            <div class="message-item">
            <div class="content">
                <div class="title">
                   @if($message->is_admin == true)
                  TarzanExpress ADMIN
                   @else
                      {{$commande->user->nom}}
                   @endif
                </div>
                <div class="bubble">
                @if($message->image == null)
                           {{$message->contenu}}
                         @else
                          <a class="imageModal" href="#" data-imgurl="{{asset('messages-commandes/'.$commande->id.'/'.$message->image)}}" data-toggle="modal" data-target="#DiaglogImage" id="{{'img_'.$message->id}}" onClick="appendInModal({{$message->id}})">
                           <img src="{{asset('messages-commandes/'.$commande->id.'/'.$message->image)}}" class="imaged w160" >
                          </a>
                 @endif
                </div>
                <div class="footer">{{$message->date}}</div>
            </div>
        </div>
             
             @endif
        
            
          @endforeach
     </div>
        <div class="chatFooter">
            <form>
                <a href="javascript:;" class="btn btn-icon btn-text-secondary select-image">
                    <ion-icon name="camera" role="img" class="md hydrated" aria-label="camera"></ion-icon>
                </a>
                <div class="form-group basic">
                    <div class="input-wrapper">
                        <input type="text" class="form-control" placeholder="Saisissez un message">
                        <i class="clear-input">
                            <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                        </i>
                    </div>
                </div>
                <button type="button" class="btn btn-icon btn-primary rounded">
                    <ion-icon name="arrow-forward-outline" role="img" class="md hydrated" aria-label="arrow forward outline"></ion-icon>
                </button>
            </form>
             <form id="uploadImage" method="post" action="{{route('user.message.picture.send',['id'=>$commande->id])}}">
                @csrf
               <input type="file" class="form-control" name="cmd_pictures" id="uploadFile" accept=".jpg,.png"/>
            <form> 
       </div>
       <div class="modal fade modalbox show" id="ModalBasic" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"></h5>
                        <a href="javascript:;" data-dismiss="modal">Fermer</a>
                    </div>
                    <div class="modal-body">
                        <p>
                            
                            cursus dictum lorem. Ut vitae arcu egestas, congue nulla at, gravida purus.
                        </p>
                        
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade dialogbox" id="DialogImage" data-backdrop="static" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    {{-- <img src="assets/img/sample/photo/1.jpg" alt="image" class="img-fluid"> --}}
                    <div class="modal-footer">
                        <div class="btn-inline">
                            <a href="#" class="btn btn-text-secondary" data-dismiss="modal">CANCEL</a>
                            <a href="#" class="btn btn-text-primary" data-dismiss="modal">DONE</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	@endsection
    @section('js')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>        
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="http://malsup.github.com/jquery.form.js"></script> 

        <script>
            $(document).ready(function() {
               $('.appBottomMenu').remove();
                setTimeout(function(){ $( '#appCapsule').scrollTop( 3500 ); }, 200);
                $( 'appCapsule' ).scrollTop( 3500 );    
                $("html,body").animate({scrollTop: $('#appCapsule .message-item:last').offset().top - 30});
            });

            $('#uploadFile').on('change', function(){
                $('#uploadImage').ajaxSubmit({
                target: "#group_chat_message",
                resetForm: true
                });
            }); 

            var pusher = new Pusher('710a42ce02a3f4836392');
            var channel = pusher.subscribe('chat_channel');

            channel.bind('chat', function(data) {
           
                var chatmess = '<div class="message-item">';
                var chatmess1 = '<div class="message-item user">';
                 console.log(data);
                if(data.commande_id == $('.message-divider').attr("id") || (data.is_admin = true && $('.message-divider').attr("id")==null)  ){

                    if(!data.image){

                        chatmess1 =chatmess1 + '<div class="content"> '+''+'<div class="bubble">'+data.contenu+'</div><div class="footer">'+data.date+'</div> </div></div>'
                        $('#appCapsule').append(chatmess1);
                    }else{

                        var url = data.image;
                        chatmess1 =chatmess1 + '<div class="content"> '+''+'<div class="bubble"><a class="imageModal" href="#" data-toggle="modal" data-target="#ModalBasic" id="img_'+data.message_id+'" data-imgurl="'+url+'" onclick="appendInModal('+data.message_id+')"><img src="'+url+'" class="imaged w160"></a></div><div class="footer">'+data.date+'</div> </div></div>';
                        $('#appCapsule').append(chatmess1);
                    } 
                }
              
                console.log(data.sender);
                console.log(data.commande_id);
                if(data.commande_id == $('.message-divider').attr('id') && data.sender ==null){
                    console.log("coucou")
                    user = $('.message-divider').attr("id")
                    chatmess =chatmess +'<div class="content"><div class="title">'
                    
                    if(user != null){
                         chatmess = chatmess +'TarzanExpress Admin'+'</div><div class="bubble">' + data.contenu+'</div><div class="footer">10:44 AM</div></div></div>'
                        // $('#appCapsule').append(chatmess);
                    }else{
                        
                        chatmess = chatmess+ {{$commande->user->nom}}+ '</div><div class="bubble"> Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div><div class="footer">10:44 AM</div></div></div>'
                        //$('#appCapsule').append(chatmess); 
                    }
                        
                    if(!data.image){

                        $('#appCapsule').append(chatmess);
                    }else{
                        
                        var url = data.image;
                        
                        chatmess = '<div class="message-item"><div class="content"><div class="title">TarzanExpress Admin</div><div class="bubble"><a class="imageModal" href="#" data-toggle="modal" data-target="#DialogImage" id="img_'+data.message_id+'" data-imgurl="'+url+'" onclick="appendInModal('+data.message_id+')"><img src="'+url+'" class="imaged w160"></a></div><div class="footer">'+data.date+'</div> </div></div>';
                        
                        $('#appCapsule').append(chatmess);           
                    }    
                }

            
            
                $('.input_send_holder').html('<input type = "submit" value = "Send" class = "btn btn-primary input_send" />');
            
                $("#appCapsule").scrollTop($("#appCapsule")[0].scrollHeight);
             
                $(".spinner-border").remove();
             
                $("html,body").animate({scrollTop: $('#appCapsule .message-item:last').offset().top - 30});
                $('.form-control').removeAttr('disabled');
                $('.form-control').attr('placeholder','Saisissez un message');
            });

            function ajaxSend(ajax_data) {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.post({
                    
                    url: "{{route('user.message.send',['id'=>$commande->id])}}",
                    dataType: "json",
                    data: ajax_data,
                    success: function(response, textStatus, jqXHR) {                  
                        
                    },
                    error: function(msg) {}
                });
            }
          
            function appendInModal(id){

                var img= $('#img_'+id).data('imgurl');

                //$('#ModalBasic .modal-body').html('<img src="'+img+'" width="100%"/>');
                $('#DialogImage .modal-content .img-fluid').remove();
                $('#DialogImage .modal-content').prepend('<img alt="image" src="'+img+'" class="img-fluid" />')
            }

            $( ".rounded" ).click(function() {
               
                ajaxData = {contenu: $('.chatFooter .form-control').val() }
                $('.chatFooter').prepend('<div class="spinner-border text-primary" role="status"></div>');
                $('.form-control').attr('disabled','disabled');
                $('.form-control').attr('placeholder','En cours d\'envoi');
                 
                if(ajaxData.contenu != null || ajaxData.contenu!=''){

                    ajaxSend(ajaxData);
                }
               
               $('.chatFooter .form-control').val('');
            });

            $(".select-image").click(function(){
                $("#uploadFile").trigger("click");
            });

        </script>

	@endsection
