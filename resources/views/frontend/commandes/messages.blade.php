@extends('frontend.layouts.app')


	@section('title')
		 Messages 
    @endsection
    @section('titre-page')
		TARZAN-EXPRESS | Messages commandes
	@endsection

	@section('notification')

		<a href="{{ route('user.panier', Auth::user()->id) }}" class="headerButton">
            <ion-icon name="cart"></ion-icon>
            <span class="badge badge-danger">{{ $cart_count }}</span>
        </a>

    	<a href="{{route('user.notifications')}}" class="headerButton">
            <ion-icon class="icon" name="notifications"></ion-icon>
            <span class="badge badge-danger notification"></span>
        </a>
	@endsection


	@section('content')

		<div id="appCapsule">
        <div class="section mt-4">
        <!--  -->
        <div class="section full">
            @if(count($commandes) == 0)

                    <div class="section">

                        <br>

                        <div class="row mt-2">
                            <div class="col-12">
                                <center>
                                    <img src="{{ asset('img/app/order-empty.png') }}" alt="img" class="imaged w100">
                                    <br>

                                    <h2>AUCUNE COMMANDE ENREGISTREE </h2>
                                    <p> Passez une commande, et vous aurez une section pour discuter et échanger sous votre commande </p>
                                </center> 
                            </div>
                        </div>
                    </div>

                    <div class="section">

                        <div class="row mt-2">
                            <div class="col-6">
                                <a href="{{ route('user.commande.create', Auth::user()->id) }}" class="btn btn-primary mr-1 mb-1" style="float: left;"> Passer une commande </a>
                            </div>
                            <div class="col-6">
                                <a href="{{ route('user.serviceClient.index', Auth::user()->id) }}" class="btn btn-secondary mr-1 mb-1" style="float: right;"> Nous écrire </a>
                            </div>
                        </div>
                    </div>
                @else

                    <ul class="listview image-listview flush">
                        
                            @php $i = 0; @endphp
                            @foreach($commandes as $commande)   
                                
                                <li>
                                    <a id="{{$commande->id}}" href="{{route('user.commande.chat', ['id'=>$commande->id])}}" class="item">
                                        @if($i%2 == 1)
                                            <div class="icon-box bg-success">
                                                <ion-icon name="mail-outline" role="img" class="md hydrated" aria-label="key outline"></ion-icon>
                                            </div>
                                        @else
                                            <div class="icon-box bg-danger">
                                                <ion-icon name="mail-outline" role="img" class="md hydrated" aria-label="key outline"></ion-icon>
                                            </div>
                                        @endif

                                        <div class="in">
                                            <div>
                                            
                                                <div class="mb-05"><strong>Commande N° {{$commande->reference}}</strong></div>
                                                <div class="text-small mb-05"></div>
                                                <div class="text-xsmall"></div>
                                            
                                            </div>
                                        </div>
                                    </a>
                                </li>

                                @php $i++; @endphp     
                            @endforeach  
                    </ul>

                @endif 

        </div>
    </div>

	        
	    </div>
        
	@endsection