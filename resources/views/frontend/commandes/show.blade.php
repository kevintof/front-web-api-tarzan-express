@extends('frontend.layouts.app')


	@section('title')
		TARZAN-EXPRESS | DETAILS COMMANDE
	@endsection

	@section('notification')

		<a href="{{ route('user.panier', Auth::user()->id) }}" class="headerButton">
            <ion-icon name="cart"></ion-icon>
            <span class="badge badge-danger"> {{ $cart_count }} </span>
        </a>

    	<a href="{{route('user.notifications')}}" class="headerButton">
            <ion-icon class="icon" name="notifications"></ion-icon>
            <span class="badge badge-danger notification"></span>
        </a>
	@endsection

	@section('content')

		<div id="appCapsule">

			<div class="section mt-2">
	            <div class="section-title"></div>
	            <div class="card">
	                <div class="card-body">
	                    <div class="chip chip-media">
	                        <i class="chip-icon bg-danger">
	                            <ion-icon name="calendar-outline"></ion-icon>
	                        </i>
	                        <span class="chip-label">{{ $commande->reference }}</span>
	                    </div>

	                    <a href="{{route('user.commande.chat', ['id'=>$commande->id])}}">
	                    	
	                    	<div class="chip chip-media float-right">
								<i class="chip-icon bg-warning">
									<ion-icon name="chatbubble" role="img" class="md hydrated" aria-label="chatbubble"></ion-icon>
								</i>
								<span class="chip-label">Discuter</span>
	                        </div>
	                    </a>	
	                </div>
	            </div>
	        </div>

	        <div class="section mt-2 mb-2">
	            <div class="section-title"></div>
	            <div class="card">
	                <div class="card-body">

	                    <ul class="nav nav-tabs style1" role="tablist">
	                        <li class="nav-item">
	                            <a class="nav-link active" data-toggle="tab" href="#details" role="tab">
	                                <ion-icon name="list-outline"></ion-icon>
	                            </a>
	                        </li>
	                        <li class="nav-item">
	                            <a class="nav-link" data-toggle="tab" href="#produits" role="tab">
	                                <ion-icon name="cube"></ion-icon>
	                            </a>
	                        </li>
	                        <li class="nav-item">
	                            <a class="nav-link" data-toggle="tab" href="#colis" role="tab">
	                                <ion-icon name="albums"></ion-icon>
	                            </a>
	                        </li>

	                        <li class="nav-item">
	                            <a class="nav-link" data-toggle="tab" href="#livraison" role="tab">
	                                <ion-icon name="car-sport"></ion-icon>
	                            </a>
	                        </li>
	                    </ul>

	                    @php ($coli = $commande->details_commande->colis[0])
	                    <div class="tab-content mt-1">
	                        <div class="tab-pane fade show active" id="details" role="tabpanel">
	                            
			                    <ul class="listview flush transparent simple-listview no-space mt-3">
					                <li>
					                    <strong>Etat</strong>
					                    <span class="{{ $commande->statut->param4 }}"> {{ $commande->statut->param1 }}</span>
					                </li>
					                <li>
					                    <strong>Date</strong>
					                    <span>{{ $commande->details_commande->date_commande}}</span>
					                </li>
					                <li>
					                    <strong>Informations</strong>
					                    <span>{{ $commande->information }}</span>
					                </li>
					                <li>
					                    <strong>Montant commande</strong>
					                    <span>{{ $commande->montant_commande }}</span>
					                </li>
					                <li>
					                    <strong>Transport (Chine - Togo)</strong>
					                    <span>{{ $commande->montant_livraison }}</span>
					                </li>
					                @if($coli->adresse_livraison_id != 1)
					                	<li>
					                    	<strong>Livraison à domicile</strong>
					                    	<span>{{ $coli->frais_livraison }}</span>
					                	</li>
					                @endif
					                <li>
					                    <strong>Frais Service</strong>
					                    <span>{{ $commande->montant_service }}</span>
					                </li>

					                <li>
					                    <strong>Montant Total</strong>
					                    <div class="alert alert-outline-danger mb-1" role="alert"><b>{{ $commande->montant_commande+$commande->montant_livraison+$commande->montant_service }} F CFA</b></div>
					                </li>

					                <li>

					                    @if($commande->statut->param2 == 3)
				                    		<strong>Paimenent</strong>
					                    	<a class="btn btn-success btn-sm mr-1" href="{{ route('user.commande.recap',[ $commande->id]) }}">Effectuer le paiement</a>
						                @else

					                    	<strong>Trans.</strong>
						                    NON DISPONIBLE
					                    @endif

					                </li>
					                @if($commande->details_commande->transaction != null)
						                <li>
						                	<div class="section mt-2">
									            <div class="card">
									                <div class="card-body">
									                    <h4 class="card-title"><center> Trans. Info</center></h4>
									                    <p class="card-text">
									                    	<ul class="listview flush transparent simple-listview no-space mt-6">								                
																<li>
												                    <strong>Etat</strong>
												                    <span class="{{ $commande->details_commande->transaction->etat_paiement->param4 }}">{{ $commande->details_commande->transaction->etat_paiement->param1 }}</span>
												                </li>
												                <li>
												                    <strong>Reference</strong>
												                    <span>{{ $commande->details_commande->transaction->reference }}</span>
												                </li>
												                <li>
												                    <strong>Montant</strong>
												                    <span>{{ $commande->details_commande->transaction->montant }}</span>
												                </li>
												                <li>
												                    <strong>Mode </strong>
												                    <span>{{ $commande->details_commande->transaction->mode_paiement->param1 }}</span>
												                </li>
												                <li>
												                    <strong>Commentaire</strong>
												                    <span>&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>
												                </li>
												                <li>
												                    <strong>Date</strong>
												                    <span>{{ $commande->details_commande->transaction->date }}</span>
												                </li>
												            </ul>
									                    </p>
									                    
									                </div>
									            </div>
									        </div>
						                </li>                   
				                    	
				                    	
				                    @endif
			            		</ul>
	                        </div>

	                        <div class="tab-pane fade" id="colis" role="tabpanel">
                        		<ul class="listview flush transparent simple-listview no-space mt-3">
	                            	
                            		<li>
					                    <strong>Réf</strong>
					                    <span class="text-success">{{ $coli->reference }}</span>
					                </li>
					                <li>
					                    <strong>Poids</strong>
					                    <span>{{ $coli->poids }}</span>
					                </li>
					                <li>
					                    <strong>Commentaire</strong>
					                    <span>{{ $coli->commentaire }}</span>
					                </li>
					                <li>
					                	<div class="section mt-2">
								            <div class="card">
								                <div class="card-body">
								                    <h4 class="card-title">Adresse de livraison</h4>
								                    <p class="card-text">
								                    	@if($coli->adresse_livraison->id == 1)
								                    		<span class="text-warning">Récupération du colis à l'agence </br></span>
								                    	@else
								                    		<span class="text-warning">Livraison à domicile </br></span>
								                    	@endif
								                       {{ $coli->adresse_livraison->ville }} {{ $coli->adresse_livraison->pays }} </br>
								                       {{ $coli->adresse_livraison->quartier }} </br>
								                       {{ $coli->adresse_livraison->adresse1 }} </br>
								                       {{ $coli->adresse_livraison->adresse2 }} </br>
								                    </p>
								                    
								                </div>
								            </div>
								        </div>
					                </li>
					                <li>
					                	@if($commande->details_commande->colis[0]->images == '')
                                            AUCUNE IMAGE DISPONIBLE
                                        @else
                                            @php($images = json_decode($commande->details_commande->colis[0]->images))
                                            @foreach($images as $image)
                                                @if($images->user != $image)
												<a href="app-notification-detail.html" class="item" data-toggle="modal" data-target="#ModalBasic" onclick="display_images('{{$images->user}}', '{{$image}}')">
                                                	<div class="mb-1">
								                        <img src="/commandes-colis/{{$images->user}}/{{$image}}" alt="image" class="imaged w100">
								                    </div>
								                </a>
								                @include('frontend.commandes.display_images')
                                                @endif
                                            @endforeach
						                @endif
									</li>
			            		</ul>
	                        </div>

	                        <div class="tab-pane fade" id="livraison" role="tabpanel">
	                        	
                        		<ul class="listview flush transparent simple-listview no-space mt-3">
	                            	<li>
	                            		@if($commande->details_commande->date_livraison != null)
	                            		
						                    <strong>Date de livraison </strong>
						                    <span class="text-success">{{$commande->details_commande->date_livraison}}</span>
						                
	                            		@else

						                    <strong>Livraison prévu</strong>
						                    <span>{{$commande->details_commande->date_prevu_livraison_max}}</span>
						                
	                            		@endif
					                </li>
					                <li>
					                    <strong>Livraison</strong>
					                    <span>{{$commande->details_commande->colis[0]->type_livraison->param1}}</span>
					                </li>

					                <li>
					                	@if(count($coli->livraison_details) > 0)
								            <div class="card">
								            	<div class="section-title">Suivez votre colis</div></br>
								                <!-- timeline -->
								                <div class="timeline timed ml-1 mr-2">
								                	@foreach($coli->livraison_details as $livraison_detail)

									                    <div class="item">
									                        <span class="time">{{$livraison_detail->date}}</span>
									                        <div class="dot bg-primary"></div>
									                        <div class="content">
									                            <h4 class="title">{{$livraison_detail->position}}</h4>
									                            <div class="text">{{$livraison_detail->commentaire}}</div>
									                        </div>
									                    </div>
									            	@endforeach

									            </div>
								            </div>
			                        	@else
			                        		<div class="section mt-2">
									            <div class="card">
									                <div class="card-body">
									                    <h5 class="card-title text-warning"><center>AUCUNE INFORMATION DE LIVRAISON DISPONIBLE</center></h5>
									                    <p class="card-text">
									                       <center>Les informations de livraison vont s'afficher ici dès que disponibles </center>
									                    </p>
									                </div>
									            </div>
									        </div>
									    @endif	

									</li>
			            		</ul>				            
	                        </div>

	                        <div class="tab-pane fade" id="produits" role="tabpanel">
	                            <div class="accordion" id="accordion01">

                            		@foreach ($coli->produits as $produit)
						                <div class="item">
						                    <div class="accordion-header">
						                        <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#accordion1a{{ $produit->id }}">
						                            {{ $produit->nom }}
						                        </button>
						                    </div>
						                    <div id="accordion1a{{ $produit->id }}" class="accordion-body collapse" data-parent="#accordion01">
						                        <div class="accordion-content">
						                            
						                            <ul class="listview flush transparent simple-listview no-space mt-3">
										                <li>
										                    <strong>Nom</strong>
										                    <span>{{ $produit->nom }}</span>
										                </li>
										                <li>
										                    <strong>Description</strong>
										                    <span>{{ $produit->description }}</span>
										                </li>
										                <li>
										                    <strong>Montant</strong>
										                    <span>
										                    	@if ($produit->montant != null)
										                        	{{ $produit->montant }}
										                        @else
										                        	<span class="text-danger">NON DISPONIBLE</span>
																@endif
										                    </span>
										                </li>
										                <li>
										                    <strong>Quantité</strong>
										                    <span>{{ $produit->quantite }}</span>
										                </li>

										                <li>
										                    <strong>Poids</strong>
										                    <span>
										                    	@if ($produit->poids != null)
										                        	{{ $produit->poids }}
										                        @else
										                        	<span class="text-danger">NON DISPONIBLE</span>
																@endif
										                    </span>
										                </li>
										                <li>
										                    <strong>Statut</strong>
										                    <span>{{ $produit->statut }}</span>
										                </li>
										                <li>
										                    <span><a href="{{ $produit->lien }}"  target="_blank">Voir le produit</a></span>
										                </li>
								            		</ul>
						                        </div>
						                    </div>
						                </div>
									@endforeach
					            </div>
					        </div>

	                    </div>

	                </div>
	            </div>
            </div>


            <br>

		    <!-- * Promos -->

		    @if ($message = Session::get('success'))
		        <div id="notification" class="toast-box toast-center show">
		            <div class="in">
		                <ion-icon name="checkmark-circle" class="text-success"></ion-icon>
		                <div class="text">
		                    {{$message}}
		                </div>
		                
		            </div>
		            <button type="button" class="btn btn-dark mr-1 mb-1 close-button">OK</button>    
		        </div>
		    @endif

	@endsection

	@section('js')
		<script type="text/javascript">

			function display_images(folder, link) {
				// body...
				$("#body-modal").html('<img src="/commandes-colis/'+folder+'/'+link+'" alt="image" class="imaged img-fluid">');
				
			}
		</script>
	@endsection