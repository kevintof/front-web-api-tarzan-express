@extends('frontend.layouts.app')

	@section('title')
	   NOUVELLES COMMANDES
	@endsection
	@section('titre-page')
		TARZAN-EXPRESS | NOUVELLES COMMANDES
	@endsection

	@section('notification')

		<a href="{{ route('user.panier', Auth::user()->id) }}" class="headerButton">
            <ion-icon name="cart"></ion-icon>
            <span class="badge badge-danger">{{ $cart_count }}</span>
        </a>
        
	@endsection

	@section('content')

		<div id="appCapsule">

			<div class="section mt-2 center" id="commandStep1Header">
                <!-- do not forget to delete mr-1 and mb-1 when copy / paste codes -->

                <button onclick="getPanierList({{Auth::user()->id}})" type="button" class="btn btn-outline-primary mr-1 mb-1" data-toggle="modal" data-target="#displayAllItems">
                	<img src="{{ asset('img/app/cart-add.png') }}" alt="img" class="square imaged w24"> &nbsp; &nbsp; Insérer depuis panier
                </button>

                <button onclick="addNewProductfield('Nouveau produit', '', 1, '', 0, 0)"  type="button" class="btn btn-outline-primary mr-1 mb-1">
                	<img src="{{ asset('img/app/cube-add.png') }}" alt="img" class="square imaged w24"> &nbsp; &nbsp; Nouveau produit
                </button>

                </br><font color="red">* Champ obligatoire</font>
	        </div>

	        <div class="section mt-2 center" id="commandStep2Header" style="display: none">
                <!-- do not forget to delete mr-1 and mb-1 when copy / paste codes -->
                <h2><center><font color="green">INFORMATION DE LIVRAISON</font></center></h2>
	        </div>

			<div class="section inset mt-2 mb-2" id="commandStep1Content">
				<form id="command_form" method="POST" action="{{ route( 'user.commande.store', Auth::user()->id) }}">
					@csrf

					<div id="step1">
						<div class="accordion" id="accordion01">
							@if(isset($items))
							    @php $i = 0; @endphp
							    <input type="hidden" id="nbreProduit" name="nbreProduit" value="{{ count($items) }}">

								@foreach($items as $item)
									@php $i++; @endphp
									
									<div class="item" id="item{{$i}}">
										<input type="hidden" id="isItem{{$i}}" name="isItem{{$i}}" value="{{$item->rowId}}">
					                    <div class="accordion-header">
					                        <button class="btn collapsed" type="button" id="itemTitle{{$i}}" data-toggle="collapse" data-target="#accordion1b{{$i}}">
					                            <ion-icon name="cube"></ion-icon>
					                            {{ $item->name }}
					                        </button>

					                    </div>
					                    <div id="accordion1b{{$i}}" class="accordion-body collapse" data-parent="#accordion01">

					                    	<button type="button" class="btn btn-icon btn-danger mr-1" style="float:right;" onclick="deleteProductItem({{$i}})">
					                        	<ion-icon name="trash"></ion-icon>
					                    	</button>

						                	</br>

					                        <div class="accordion-content">
						                        <div class="form-group boxed">
						                            <div class="input-wrapper">
						                            	<div id="nomMsg{{$i}}"></div>
						                                <input type="text" class="form-control" id="nom{{$i}}" name="nom{{$i}}" placeholder="* Nom du produit" value="{{ $item->name }}" onchange="setNewItemProductTitle({{$i}})">
						                                <i class="clear-input" onclick="resetNewItemProductTitle({{$i}})">
						                                    <ion-icon name="close-circle"></ion-icon>
						                                </i>
						                            </div>
						                        </div>

						                        <div class="form-group boxed">
						                            <div class="input-wrapper">
						                            	<div id="descriptionMsg{{$i}}"></div>
						                                	<textarea id="description{{$i}}" name="description{{$i}}" rows="3" class="form-control" placeholder="* Description (Toutes les précisions sur le produit)">
						                                    	{{$item->options->description}}
						                                	</textarea>
						                                <i class="clear-input">
						                                    <ion-icon name="close-circle"></ion-icon>
						                                </i>
						                            </div>
						                        </div>

						                        <div class="form-group boxed">
						                            <div class="input-wrapper">
						                            	<div id="lienMsg{{$i}}"></div>
						                                <input type="text" class="form-control" id="lien{{$i}}" name="lien{{$i}}" placeholder="* Lien" value="{{ $item->options->lien }}">
						                                <i class="clear-input">
						                                    <ion-icon name="close-circle"></ion-icon>
						                                </i>
						                            </div>
						                        </div>

						                        <div class="form-group boxed">
						                            <div class="input-wrapper">
						                                <input type="number" class="form-control" id="quantite{{$i}}" name="quantite{{$i}}" value="{{ $item->qty }}">
						                                <i class="clear-input">
						                                    <ion-icon name="close-circle"></ion-icon>
						                                </i>
						                            </div>
						                        </div>
						                        <div class="custom-file-upload">

						                            <input type="file" id="images{{$i}}" name="images{{$i}}" accept=".png, .jpg, .jpeg">

						                            @if($item->options->image == null || $item->options->image == '')
							                        	<label for="images{{$i}}">
							                                <span>
							                                    Insérer une image
							                                </span>
							                            </label>
							                        @else
							                        	<label for="images{{$i}}" class="file-uploaded" style="background-image: url('/commandes-colis/{{Auth::user()->id}}/{{$item->options->image}}');">
							                                <span>
							                                    Changer d'image
							                                </span>
							                            </label>
							                        @endif
						                        </div>
					                        </div>
					                    </div>
					                </div>  

								@endforeach


							@else
							
								<input type="hidden" id="nbreProduit" name="nbreProduit" value="0">
							@endif
							 
			            </div>

			        	</br>

			        	<button type="button" class="btn btn-success btn-block btn-lg" onclick="validateAllProduct()">Passer la commande</button>
					</div>

					<div id="step2" style="display: none;">
						<div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="label" for="select4">Mode de Livraison</label>
                                <select class="form-control custom-select" id="type_livraison_id" name="type_livraison_id">
									@foreach( $typeLivraisons as $typeLivraison)
								<option value="{{$typeLivraison->id}}">{{$typeLivraison->param1}}</option>
									@endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="label" for="a_livraison">Adresse de Livraison </label>
                                <select class="form-control custom-select" id="a_livraison" name="a_livraison" onchange="displayAdressForm()">
									<option value="" selected="" disabled hidden>veuillez cliquer pour choisir</option>
									<option value="1">Récupérer le colis à l'agence</option>
                                    <option value="2">Livraison à domicile</option>
                                </select>
                            </div>
                        </div>

                        <div class="section mt-3 mb-3" id="adressForm" style="display: none;">
				            <div class="section-title">Votre adresse de livraison</div>
				            <div class="card">
				                <div class="card-body">
				                    <input type="text" class="form-control" id="pays" name="pays" value="Togo" hidden="true">

			                        <div class="form-group boxed">
			                            <div class="input-wrapper">
			                                <label class="label" for="ville">Ville</label>
			                                <div id="villeMsg"></div>
			                                <input type="text" class="form-control" id="ville" name="ville" placeholder="Ville de livraison">
			                                <i class="clear-input">
			                                    <ion-icon name="close-circle"></ion-icon>
			                                </i>
			                            </div>
			                        </div>

			                        <div class="form-group boxed">
			                            <div class="input-wrapper">
			                                <label class="label" for="quartier">Quartier</label>
			                                <div id="quartierMsg"></div>
			                                <input type="text" class="form-control" id="quartier" name="quartier" placeholder="Quarttier">
			                                <i class="clear-input">
			                                    <ion-icon name="close-circle"></ion-icon>
			                                </i>
			                            </div>
			                        </div>

			                        <div class="form-group boxed">
			                            <div class="input-wrapper">
			                                <label class="label" for="adresse1">Adresse </label>
			                                <div id="adresseMsg"></div>
			                                <textarea type="text" rows="3" class="form-control" id="adresse1" name="adresse1" placeholder="Détails sur l'adresse"></textarea>
			                                <i class="clear-input">
			                                    <ion-icon name="close-circle"></ion-icon>
			                                </i>
			                            </div>
			                        </div>

			                        <div class="form-group boxed">
			                            <div class="input-wrapper">
			                                <label class="label" for="adresse2">Autre détails</label>
			                                <textarea type="text" rows="2" class="form-control" id="adresse2" name="adresse2" placeholder="Donnez une autre précision"></textarea>
			                                <i class="clear-input">
			                                    <ion-icon name="close-circle"></ion-icon>
			                                </i>
			                            </div>
			                        </div>

				                </div>
				            </div>
						</div>
						 <div class="section mt-3 mb-3" id="adressAgenceForm">
				            <div class="section-title">Votre agence</div>
				            <div class="card">
				                <div class="card-body">

				                    <div class="form-group boxed">
										<div class="input-wrapper">
											<label class="label" for="agence_pays_livraison"> Pays de livraison </label>
											<select class="form-control custom-select" id="agence_pays_livraison" name="agence_pays_livraison" onchange="refreshVilles()">
												<option value="" selected="" disabled hidden>veuillez cliquer pour choisir</option>
												@foreach($pays as $country)
											       <option value="{{$country->id}}">{{$country->nom}}</option> 
											    @endforeach   
												{{-- <option value="1">togo</option>
												<option value="2">benin</option> --}}
											</select>
										</div>
									</div>
									<div class="form-group boxed">
										<div class="input-wrapper">
											<label class="label" for="agence_ville_livraison"> Ville de livraison </label>
											<select class="form-control custom-select" id="agence_ville_livraison" name="agence_pays_livraison" onchange="refreshAgences()">
												<option value="" selected="" disabled hidden>veuillez cliquer pour choisir</option>
												
											</select>
										</div>
									</div>
                                    
			                        <div class="form-group boxed">
										<div class="input-wrapper">
											<label class="label" for="agence"> Agence de livraison </label>
											<select class="form-control custom-select" id="agence" name="agence" onchange="">
												<option value="" selected="" disabled hidden>veuillez cliquer pour choisir</option>
												
											</select>
										</div>
									</div>
                                    

			                        

			                 

				                </div>
				            </div>
				        </div> 
						

				        </br>

			        	<button type="button" class="btn btn-success btn-block btn-lg" id="submitForm" onclick="commandSubmit()">Valider</button>

			        	<div class="form-button-group transparent" id="loadingForm">
		                    <button class="btn btn-success btn-block btn-lg" type="button" disabled>
		                        <span class="spinner-border spinner-border-sm mr-05" role="status" aria-hidden="true"></span>
		                        Patientez...
		                    </button>
		                </div>

		                <script>
		                    document.getElementById('loadingForm').style.display = 'none';
		                </script>
					</div>					
		            
		            </br>

		            <button type="submit" class="btn btn-success btn-block btn-lg" style="display: none;">OK</button>
		            
				</form>

				<div id="errorMsg" class="toast-box toast-center">
		            <div class="in">
		                <ion-icon name="warning" class="text-danger"></ion-icon>
		                <div class="text">
		                    Veuillez saisir les champs obligatoire
		                </div>
		            </div>
		            <button type="button" class="btn btn-sm btn-text-light close-button" style="display:none;">OK</button>
		        </div>

		        <div id="errorMsg1" class="toast-box toast-center">
		            <div class="in">
		                <ion-icon name="warning" class="text-danger"></ion-icon>
		                <div class="text">
		                    Insérer au moins un produit
		                </div>
		            </div>
		            <button type="button" class="btn btn-sm btn-text-light close-button" style="display:none;">OK</button>
		        </div>
	        </div>

	        <div class="modal fade action-sheet inset" id="displayAllItems" tabindex="-1" role="dialog">
	            <div class="modal-dialog" role="document">
	                <div class="modal-content">
	                    <div class="modal-header">
	                        <h5 class="modal-title">VOTRE PANIER</h5>
	                    </div>
	                    <div class="modal-body">

	                        <div class="transactions">
		                    	
					        </div>

					        <div id="loadingToastItems" class="toast-box toast-center show">
				                <div class="in">
				                    <div class="spinner-border text-success" role="status"></div>
				                    <br>
				                    <div class="text">
				                        Chargement du panier ...
				                    </div>
				                </div>
				            </div>

	                    </div>
	                </div>
	            </div>
	        </div>

		</div>
	@endsection


	@section('js')
		<script type="text/javascript">
			
			var nbreProduit = parseInt($("#nbreProduit").val());

			function addNewProductfield(nom, desc, qte, lien, item, id){

				var idToInt = parseInt(id);
				
				nbreProduit +=1;
				$("#nbreProduit").val(nbreProduit);


				var html= 
		            '<div class="item" id="item'+nbreProduit+'">'+

						'<input type="hidden" id="isItem'+nbreProduit+'" name="isItem'+nbreProduit+'"  value="'+item+'">'+
		     		
		                '<div class="accordion-header">'+
		                    '<button class="btn collapsed" type="button" id="itemTitle'+nbreProduit+'" data-toggle="collapse" data-target="#accordion1b'+nbreProduit+'">'+
		                        '<ion-icon name="cube"></ion-icon>'+
		                        nom+
		                    '</button>'+
		                '</div>'+
		                '<div id="accordion1b'+nbreProduit+'" class="accordion-body collapse" data-parent="#accordion01">'+
		                    

					        '<button type="button" class="btn btn-icon btn-danger mr-1" style="float:right;" onclick="deleteProductItem('+nbreProduit+')">'+
		                        '<ion-icon name="trash"></ion-icon>'+
		                    '</button>'+

			                '</br>'+

		                    '<div class="accordion-content">';

		        if (idToInt <= 0) {

	        		html += '<div class="form-group boxed">'+
	                            '<div class="input-wrapper">'+
	                            	'<div id="nomMsg'+nbreProduit+'"></div>'+
	                                '<input type="text" class="form-control" id="nom'+nbreProduit+'" name="nom'+nbreProduit+'" placeholder="Nom du produit" onchange="setNewItemProductTitle('+nbreProduit+')" >'+
	                                '<i class="clear-input">'+
	                                    '<ion-icon name="close-circle"></ion-icon>'+
	                                '</i>'+
	                            '</div>'+
	                        '</div>';

				}else {

					html += '<div class="form-group boxed">'+
	                            '<div class="input-wrapper">'+
	                            	'<div id="nomMsg'+nbreProduit+'"></div>'+
	                                '<input type="text" class="form-control" id="nom'+nbreProduit+'" name="nom'+nbreProduit+'" placeholder="Nom du produit" onchange="setNewItemProductTitle('+nbreProduit+')" value="'+nom+'">'+
	                                '<i class="clear-input">'+
	                                    '<ion-icon name="close-circle"></ion-icon>'+
	                                '</i>'+
	                            '</div>'+
	                        '</div>';
				}
		                    	
		        html +=     '<div class="form-group boxed">'+
		                            '<div class="input-wrapper">'+
		                            	'<div id="descriptionMsg'+nbreProduit+'"></div>'+
		                                '<textarea id="description'+nbreProduit+'" name="description'+nbreProduit+'" rows="3" class="form-control" placeholder="Description (Toutes les précisions sur le produit)">'+desc+'</textarea>'+
		                                '<i class="clear-input" onclick="resetNewItemProductTitle('+nbreProduit+')">'+
		                                    '<ion-icon name="close-circle"></ion-icon>'+
		                                '</i>'+
		                            '</div>'+
		                        '</div>'+

		                        '<div class="form-group boxed">'+
		                            '<div class="input-wrapper">'+
		                            	'<div id="lienMsg'+nbreProduit+'"></div>'+
		                                '<input type="text" class="form-control" id="lien'+nbreProduit+'" name="lien'+nbreProduit+'" placeholder="Lien"  value="'+lien+'">'+
		                                '<i class="clear-input">'+
		                                    '<ion-icon name="close-circle"></ion-icon>'+
		                                '</i>'+
		                            '</div>'+
		                        '</div>'+
		 
		                        '<div class="form-group boxed">'+
		                            '<div class="input-wrapper">'+
		                                '<input type="number" class="form-control" id="quantite'+nbreProduit+'" name="quantite'+nbreProduit+'" value="'+qte+'">'+
		                                '<i class="clear-input">'+
		                                    '<ion-icon name="close-circle"></ion-icon>'+
		                                '</i>'+
		                            '</div>'+
		                        '</div>';

		        if (idToInt <= 0) {
	        		html +=        
                        '<div class="custom-file-upload">'+
                            '<input type="file" id="images'+nbreProduit+'" name="images'+nbreProduit+'" accept=".png, .jpg, .jpeg" onchange="previewFile(event,'+nbreProduit+');">'+
                            '<label for="images'+nbreProduit+'" id="labelimages'+nbreProduit+'">'+
                                '<span id="spanimages'+nbreProduit+'">'+
                                    '<strong>'+
                                        '<ion-icon name="arrow-up-circle-outline"></ion-icon>'+
                                        '<i>Insérer une image du produit</i>'+
                                    '</strong>'+
                                '</span>'+
                            '</label>'+
                        '</div>';

		        }else {
		        	var imgSrc = $("#itemImg"+id).attr('src');

		        	html +=        
                        '<div class="custom-file-upload">'+
                            '<input type="file" id="images'+nbreProduit+'" name="images'+nbreProduit+'" accept=".png, .jpg, .jpeg" onchange="previewFile(event,'+nbreProduit+');">'+
                            '<label for="images'+nbreProduit+'" class="file-uploaded" id="labelimages'+nbreProduit+'" style="background-image: url('+imgSrc+');">'+
                                '<span id="spanimages'+nbreProduit+'">'+
                                    '<strong> Changer d\'image'+
                                    '</strong>'+
                                '</span>'+
                            '</label>'+
                        '</div>';
		        }



		        html +=
		                    '</div>'+
		                '</div>'+
		            '</div> ';


				if (idToInt > 0){

					if ($('#'+idToInt).is(':checked'))
			           $( "#accordion01" ).append(html);

			      	else
			      		deleteProductItem(nbreProduit); 
					
				}else
		        	$( "#accordion01" ).append(html);				
			}
		 
			function deleteProductItem(productFieldId) {
				
				if (productFieldId < nbreProduit){

						var j = productFieldId;
					for (var i = productFieldId; i < nbreProduit; i++) {
						j += 1;
						$("#itemTitle"+i).html($( "#itemTitle"+j).html());
						$("#nom"+i).val($("#nom"+j).val());
						$("#description"+i).val($("#description"+j).val());
						$("#quantite"+i).val($("#quantite"+j).val());
						$("#lien"+i).val($("#lien"+j).val());
						$("#images"+i).val($("#images"+j).val());
					}
				}

				$("#item"+nbreProduit).remove();
				nbreProduit-=1;
				$("#nbreProduit").val(nbreProduit);
			}

			function resetNewItemProductTitle(id) {

				$("#itemTitle"+id).html('<ion-icon name="cube"></ion-icon> Nouveau produit');
			}

			function setNewItemProductTitle(id) {

				if ($("#nom"+id).val() == "")
					$("#itemTitle"+id).html('<ion-icon name="cube"></ion-icon> Nouveau produit');

				else
					$("#itemTitle"+id).html('<ion-icon name="cube"></ion-icon>'+$( "#nom"+id).val());
			}

			function checkRequieredValue() {
				// body...

				var returnValue = true;

				for (var i = 1; i <= nbreProduit; i++) {

					if ($("#nom"+i).val() == '' || $("#description"+i).val() == '' || $("#lien"+i).val() == '') {

						if ($("#nom"+i).val() == ''){
							$("#itemTitle"+i).html('<ion-icon name="cube"></ion-icon> Nouveau produit &nbsp;&nbsp; <ion-icon color="danger" name="warning"></ion-icon>');

							$("#nomMsg"+i).html('<font color="red">Insérer le nom du produit</font>');
						}else{

							$("#itemTitle"+i).html('<ion-icon name="cube"></ion-icon>'+ $("#nom"+i).val()+' &nbsp;&nbsp; <ion-icon color="danger" name="warning"></ion-icon>');

							$("#nomMsg"+i).html('');
						}


						if ($("#description"+i).val() == '')
							$("#descriptionMsg"+i).html('<font color="red">Insérer une description du produit</font>');
						else
							$("#descriptionMsg"+i).html('');



						if ($("#lien"+i).val() == '')
							$("#lienMsg"+i).html('<font color="red">Insérer un lien sur le produit</font>');
						else
							$("#lienMsg"+i).html('');

						returnValue = false;
					}else{

						$("#itemTitle"+i).html('<ion-icon name="cube"></ion-icon>'+ $("#nom"+i).val());
						$("#nomMsg"+i).html('');
						$("#descriptionMsg"+i).html('');
						$("#lienMsg"+i).html('');
					}
				}

				if (!returnValue)
					toastbox('errorMsg', 2000);

				return returnValue;
			}

			function checkLivraisonRequieredValue(argument) {

				var returnValue = true;

				if ($("#a_livraison").val() == 2){

					if ($("#ville").val() == '' || $("#quartier").val() == '' || $("#adresse1").val() == '') {
						if ($("#ville").val() == '')
							$("#villeMsg").html('<font color="red">Insérer la ville de livraison</font>');
						else
							$("#villeMsg").html('');

						if ($("#quartier").val() == '')
							$("#quartierMsg").html('<font color="red">Insérer le quartier</font>');
						else
							$("#quartierMsg").html('');

						if ($("#adresse1").val() == '')
							$("#adresseMsg").html('<font color="red">Insérer une précision de votre adresse</font>');
						else
							$("#adresseMsg").html('');

						returnValue = false;
					}else{

						$("#villeMsg").html('');
						$("#quartierMsg").html('');
						$("#adresseMsg").html('');
					}

				}

				if (!returnValue)
					toastbox('errorMsg', 2000);

				return returnValue;
			}

	 		function validateAllProduct(argument) {

				if (checkRequieredValue() & nbreProduit > 0 ){
					$("#step2").show();
					$("#commandStep2Header").show();
					$("#step1").hide();
					$("#commandStep1Header").hide();



					/* $.ajaxSetup({
				        headers: {
				            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				        }
				    }); */

				    /* $.get({
				        url: '/auto/type-LIVRAISON',
				        success: function (response) {

				        	for (var i = 0; i < response.length; i++) {
				        		$("#type_livraison_id").append('<option value="'+response[i].id+'">'+response[i].param1+'</option>');
				        	}
				         
				        },
				         error: function (response) {
				             
				        }
				    }); */
				}

				if (nbreProduit <= 0)
					toastbox('errorMsg1', 1000);	
			} 

			function commandSubmit(argument) {

				if (checkLivraisonRequieredValue(argument)){
					$("#submitForm").hide();
	            	document.getElementById('loadingForm').style.display = 'block';

					document.getElementById('command_form').submit();
				}
			}

			function displayAdressForm() {

				if ($("#a_livraison" ).val() == 2)
				{
					$("#adressForm").show();
					$("#adressAgenceForm").hide();
				}

				else
				{
					$("#adressForm").hide();
					$("#adressAgenceForm").show();
				}
					
			}

			function getPanierList(id) {
				$(".transactions" ).html('');
				$.get({
			        url: '/panier/'+id+'/get_all_items',
			        success: function (response) {

			        	$("#loadingToastItems").removeClass("show");

			        	if (response.count > 0)
				            for (var i = response.count; i >= 0; i--) {
				            	$(".transactions" ).append(response[i]);
				            }
				        else
				        	$(".transactions" ).append('<br><center> AUCUN PRODUIT DANS LE PANIER </center><br><br>');
			         
			        },
			         error: function (response) {
			            
			        }
			    });
			}

			function addProductPanierIntoCommand(productsList) {
				
			}

			function getVilles(id)
			{

				$.ajax({
					url: "/pays/"+id+"/villes",
					type: 'GET',
					dataType: 'json', // added data type
					success: function(data) {
						var html;
						for(var i=0; i<data.length;i++)
						{
						html+='<option value="'+data[i].id+'">'+data[i].nom+'</option>';
						
						}
						console.log(html);
						$('#agence_ville_livraison').empty();
						$('#agence_ville_livraison').html(html);
						console.log(data)
						
					}
				});
				
			}

			function refreshVilles()
			{
				var id = $('#agence_pays_livraison').val();
				
				getVilles(id);
			}

			function getAgences(id)
			{
				$.ajax({
					url: "/villes/"+id+"/agences",
					type: 'GET',
					dataType: 'json', // added data type
					success: function(data) {
						var html;
						for(var i=0; i<data.length;i++)
						{
						html+='<option value="'+data[i].id+'">'+data[i].nom+'--'+data[i].adresse.quartier+'</option>';
						
						}
						console.log(html);
						$('#agence').empty();
						$('#agence').html(html);
						console.log(data)
						
					}
				});
			}

			function refreshAgences()
			{
				var id = $('#agence_ville_livraison').val();
				
				getAgences(id);

			}

		</script>
	@endsection