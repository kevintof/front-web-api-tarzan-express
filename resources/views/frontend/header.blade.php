<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('titre-page')</title>
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
    <meta name="description" content="Votre coursier en chine">
    <meta name="keywords" content="togo, benin, e-commerce, dropshipping, chine, livraison, coursiers, courses" />
    <meta name="author" content="Kevin TOFFA">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('img/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" href="{{ asset('img/favicon.png') }}" sizes="32x32">
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}">
    
    @yield('stylesheets')
    
</head>

<body>

    @yield('firebase')