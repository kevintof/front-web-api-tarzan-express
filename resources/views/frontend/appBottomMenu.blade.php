@if(Auth::id())
<div class="appBottomMenu">
    <a href="{{ route('user.home') }}" class="item">
        <div class="col">
            <img src="{{ asset('img/app/home.png') }}" alt="img" class="square imaged w24">
            <strong>Accueil</strong>
        </div>
    </a>

    <a href="{{ route('user.actualites.index') }}" class="item">
        <div class="col">
            <img src="{{ asset('img/app/newspaper.png') }}" alt="img" class="square imaged w24">
            <strong>Actualités</strong>
        </div>
    </a>

    <a href="{{route('user.commande.create') }}" class="item">
        <div class="col">
            <img src="{{ asset('img/app/shopping-bag-add.png') }}" alt="img" class="square imaged w24">
            <strong>Nouvelle commande</strong>
        </div>
    </a>
    
    <a href="{{ route('user.commande.index') }}" class="item">
        <div class="col">
            <img src="{{ asset('img/app/shopping-bag.png') }}" alt="img" class="square imaged w24">
            <strong>Commandes</strong>
        </div>
    </a>   

    <a href="{{ route('user.commande.messages') }}" class="item">
        <div class="col">
            <img src="{{ asset('img/app/chat.png') }}" alt="img" class="square imaged w24">
            <strong>Messages</strong>
        </div>
    </a>
</div>
@endif