@include('frontend.header')

    <!-- loader -->
    <div id="loader">
        <img src="{{ asset('img/logo-icon.png') }}" alt="icon" class="loading-icon">
    </div>
    <!-- * loader -->

    <!-- App Header -->
    <div class="appHeader bg-secondary text-light">
        <div class="pageTitle"> AJOUT DE PRODUIT AU PANIER</div>
        <div class="right">
        </div>
    </div>
    <!-- * App Header -->

    <!-- App Capsule -->
    <div id="appCapsule">
        
        <div class="section mt-2 text-center">
            <img src="{{ asset('img/apple-touch-icon.png') }}" alt="icon" width="50" height="50">
        </div>
        <div class="section mb-5 p-2">
            <div class="action-sheet-content" id="div_cart_form">
                <form id="cart_form" method="POST" action="{{ route('user.produit.save') }}" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <div id="nomMsg"></div>
                            <input type="text" class="form-control" id="nom" name="nom" placeholder="* Nom du produit" value="{{$name}}">
                            <i class="clear-input">
                                <ion-icon name="close-circle"></ion-icon>
                            </i>
                        </div>
                    </div>

                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <div id="descriptionMsg"></div>
                            <textarea id="description" name="description" rows="3" class="form-control"
                                placeholder="* Description (Toutes les précisions sur le produit)"></textarea>
                            <i class="clear-input">
                                <ion-icon name="close-circle"></ion-icon>
                            </i>
                        </div>
                    </div>

                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <div id="lienMsg"></div>
                            <input type="text" class="form-control" id="lien" name="lien" placeholder="* Lien" value="{{$link}}" readonly>
                            <i class="clear-input">
                                <ion-icon name="close-circle"></ion-icon>
                            </i>
                        </div>
                    </div>

                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <label>Quantité</label>
                            <input type="number" class="form-control" id="quantite" name="quantite" value="1">
                            <i class="clear-input">
                                <ion-icon name="close-circle"></ion-icon>
                            </i>
                        </div>
                    </div>

                    <div class="custom-file-upload">
                        <input type="file" id="images" name="images" accept=".png, .jpg, .jpeg">
                        <label for="images">
                            <span>
                                <strong>
                                    <ion-icon name="arrow-up-circle-outline"></ion-icon>
                                    <i>Insérer une image du produit</i>
                                </strong>
                            </span>
                        </label>
                    </div>

                    <br>

                    <button type="button" class="btn btn-success btn-block btn-lg" id="validate">Ajouter au panier</button>
                </form>
            </div>
            <div id="errorMsg" class="toast-box toast-center">
                <div class="in">
                    <div class="spinner-border text-success" role="status"></div>
                    <div class="text">
                        Veuillez saisir les champs obligatoire
                    </div>
                </div>
                <button type="button" class="btn btn-sm btn-text-light close-button" style="display:none;">OK</button>
            </div>
        </div>

        @if ($message = Session::get('success'))
            <script type="text/javascript">
                document.getElementById("div_cart_form").style.display = "none";
            </script>
            <div id="loading" class="toast-box toast-center show">
                <div class="in">
                    <ion-icon name="checkmark-circle" class="text-success"></ion-icon>
                    <div class="text">
                        Produit ajouté au panier
                    </div>
                    <button type="button" class="btn btn-secondary mr-1 mb-1" onclick="showAndroidToast()">OK</button>
                </div>
            </div>
        @endif

    </div>
    <!-- * App Capsule -->
    @section('js')

        <script type="text/javascript">
            function showAndroidToast(toast) {
                if(typeof Android !== "undefined" && Android !== null) {
                    Android.quitApp();
                } else {
                    alert("Not viewing in webview");
                }
            }
        </script>
        <script type="text/javascript">
            $("#validate").click(function() {
                if ($("#nom").val() == '' || $("#description").val() == '' || $("#lien").val() == '') {

                    if ($("#nom").val() == ''){
                        $("#itemTitle").html('<ion-icon name="cube"></ion-icon> Nouveau produit &nbsp;&nbsp; <ion-icon color="danger" name="warning"></ion-icon>');

                        $("#nomMsg").html('<font color="red">Insérer le nom du produit</font>');
                    }else{

                        $("#itemTitle").html('<ion-icon name="cube"></ion-icon>'+ $("#nom").val()+' &nbsp;&nbsp; <ion-icon color="danger" name="warning"></ion-icon>');

                        $("#nomMsg").html('');
                    }

                    if ($("#description").val() == '')
                        $("#descriptionMsg").html('<font color="red">Insérer une description du produit</font>');
                    else
                        $("#descriptionMsg").html('');
                    
                    toastbox('errorMsg', 2000);

                }else{
                    toastbox('loading');
                    document.getElementById('cart_form').submit();
                }
            });

        </script>
    @endsection


@include('frontend.footer')



