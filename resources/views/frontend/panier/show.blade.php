@extends('frontend.layouts.app')
	@section('titre-page')
	   TARZAN EXPRESS | VOTRE PANIER
	@endsection
	@section('title')
		VOTRE PANIER
	@endsection

	@section('notification')

        <a class="headerButton" data-toggle="modal" data-target="#addNewProductfield">
            <img src="{{ asset('img/app/cart-addin.png') }}" alt="image" class="imaged w32">
        </a>

    	<a href="{{ route('user.notification') }}" class="headerButton">
            <ion-icon class="icon" name="notifications-outline"></ion-icon>
            <span class="badge badge-danger notification"></span>
        </a>

        
	@endsection

	@section('content')

		<div id="appCapsule">

			<div class="section mt-2 center" id="commandStep1Header">
                <!-- do not forget to delete mr-1 and mb-1 when copy / paste codes -->
               <a href="#" type="button" class="btn btn-outline-primary mr-1 mb-1" data-toggle="modal" data-target="#addNewProductfield">
                    <img src="{{ asset('img/app/cart-addin.png') }}" alt="img" class="square imaged w24"> &nbsp; &nbsp; Insérer produit au panier
                </a>

                <button type="button" class="btn btn-outline-danger mr-1 mb-1">
                	<img src="{{ asset('img/app/shopping-cart.png') }}" alt="img" class="square imaged w24"> &nbsp; &nbsp; <span id="cart_count">{{$items->count()}} </span>
                </button>

                <input type="hidden" id="id" value="{{ Auth::user()->id }}">

	        </div>

	        <div class="section mt-4">
		        <div class="transactions">
		        	@php $i=0 @endphp
		        	@foreach($items as $item)

		        	<!-- item -->
			            <a class="item" id="itemdiv{{$i}}">
			            	<div class="custom-control custom-checkbox">
	                            <input type="checkbox" class="custom-control-input" id="{{ $i }}" name="cartItem" value="{{$item->rowId}}">
	                            <label class="custom-control-label" for="{{ $i }}"></label>
	                            @php $i++ @endphp
	                        </div>

			                <div class="detail">
			                	@if($item->options->image != '')
	                            	<img src="{{ asset('commandes-colis/'.Auth::user()->id.'/'.$item->options->image) }}" alt="img" class="image-block imaged w64">
	                            @else
	                            	<img src="{{ asset('img/sample/brand/1.jpg') }}" alt="img" class="image-block imaged w64">
	                            @endif

			                    <div>
			                        <strong> {{ $item->name }} </strong> x{{ $item->qty }} 
			                        
			                        <p> {{ $item->options->description }} &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</p>
			                    </div>
			                </div>
			                
			                <div class="right">

		                    	<button type="button" class="btn btn-icon btn-secondary mr-1" onclick="deleteItem('{{$item->rowId}}', {{ $i }})">
		                    		<img src="{{ asset('img/app/delete.png') }}" alt="img" class="square imaged w24">
		                    	</button> 

		                    	</br>
		                    	</br>

		                    	<button type="button" onclick="window.location.href='{{ $item->options->lien }}';" class="btn btn-icon btn-secondary mr-1">
		                    		<img src="{{ asset('img/app/link.png') }}" alt="img" class="square imaged w24">
		                    	</button> 

			                </div>
			            </a>
			            <!-- * item -->
			        @endforeach

			        <br>
			        <br>
		        </div>
		    </div>
			

			<div class="section inset mt-2 mb-2" id="commandStep1Content">
				
					
				<div class="accordion" id="accordion01">
					<input type="hidden" id="nbreProduit" name="nbreProduit" value="0">  
	            </div>
												            
				
	            <div class="modal fade action-sheet inset" id="addNewProductfield" tabindex="-1" role="dialog">
		            <div class="modal-dialog" role="document">
		                <div class="modal-content">
		                    <div class="modal-header">
		                        <h5 class="modal-title">Insérer les informations</h5>
		                    </div>
		                    <div class="modal-body">
		                        <div class="action-sheet-content">
			                        <form id="cart_form" method="POST" action="{{ route('user.panier', Auth::user()->id) }}"  enctype="multipart/form-data">
										@csrf

										<div class="form-group boxed">
				                            <div class="input-wrapper">
				                            	<div id="nomMsg"></div>
				                                <input type="text" class="form-control" id="nom" name="nom" placeholder="* Nom du produit">
				                                <i class="clear-input" onclick="resetNewItemProductTitle()">
				                                    <ion-icon name="close-circle"></ion-icon>
				                                </i>
				                            </div>
				                        </div>

				                        <div class="form-group boxed">
				                            <div class="input-wrapper">
				                            	<div id="descriptionMsg"></div>
				                                <textarea id="description" name="description" rows="3" class="form-control"
				                                    placeholder="* Description (Toutes les précisions sur le produit)"></textarea>
				                                <i class="clear-input">
				                                    <ion-icon name="close-circle"></ion-icon>
				                                </i>
				                            </div>
				                        </div>

				                        <div class="form-group boxed">
				                            <div class="input-wrapper">
				                            	<div id="lienMsg"></div>
				                                <input type="text" class="form-control" id="lien" name="lien" placeholder="* Lien">
				                                <i class="clear-input">
				                                    <ion-icon name="close-circle"></ion-icon>
				                                </i>
				                            </div>
				                        </div>

				                        <div class="form-group boxed">
				                            <div class="input-wrapper">
				                                <input type="number" class="form-control" id="quantite" name="quantite" value="1">
				                                <i class="clear-input">
				                                    <ion-icon name="close-circle"></ion-icon>
				                                </i>
				                            </div>
				                        </div>

				                        <div class="custom-file-upload">
				                            <input type="file" id="images" name="images" accept=".png, .jpg, .jpeg">
				                            <label for="images">
				                                <span>
				                                    <strong>
				                                        <ion-icon name="arrow-up-circle-outline"></ion-icon>
				                                        <i>Insérer une image du produit</i>
				                                    </strong>
				                                </span>
				                            </label>
				                        </div>

				                        <br>

				                        <button type="button" class="btn btn-success btn-block btn-lg" onclick="cartFormSubmit()">Ajouter au panier</button>
									</form>
								</div>

		                    </div>
		                </div>
		            </div>
		        </div>

				<div id="errorMsg" class="toast-box toast-center">
		            <div class="in">
		                <ion-icon name="warning" class="text-danger"></ion-icon>
		                <div class="text">
		                    Veuillez saisir les champs obligatoire
		                </div>
		            </div>
		            <button type="button" class="btn btn-sm btn-text-light close-button" style="display:none;">OK</button>
		        </div>

		        <div id="loadingToast" class="toast-box toast-center">
	                <div class="in">
	                    <div class="spinner-border text-success" role="status"></div>
	                    <br>
	                    <div class="text">
	                        Patientez SVP ...
	                    </div>
	                </div>
	            </div>
	        </div>

	        <div id="command" class="toast-box toast-bottom bg-secondary show">
	            <div class="in">
	                <div class="text">
	                    <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="all" name="allCartItem">
                            <label class="custom-control-label text-light" for="all"> Selectionner tout </label>
                        </div>
	                </div>
	            </div>
	            <button type="button" class="btn btn-danger mr-1 mb-1" id="commandAction">Commander (0)</button>
	        </div>

		</div>


@endsection


	@section('js')
		<script type="text/javascript">

			$( document ).ready(function() {
				if($('#all').is(':checked') == true)
					n = $("input:checked").length - 1;
				else
					n = $("input:checked").length;

			    $("#commandAction").text("Commander ("+(n)+")");
			});

			function checkRequieredValue() {
				// body...

				var returnValue = true;

				if ($("#nom").val() == '' || $("#description").val() == '' || $("#lien").val() == '') {

					if ($("#nom").val() == ''){
						$("#itemTitle").html('<ion-icon name="cube"></ion-icon> Nouveau produit &nbsp;&nbsp; <ion-icon color="danger" name="warning"></ion-icon>');

						$("#nomMsg").html('<font color="red">Insérer le nom du produit</font>');
					}else{

						$("#itemTitle").html('<ion-icon name="cube"></ion-icon>'+ $("#nom").val()+' &nbsp;&nbsp; <ion-icon color="danger" name="warning"></ion-icon>');

						$("#nomMsg").html('');
					}


					if ($("#description").val() == '')
						$("#descriptionMsg").html('<font color="red">Insérer une description du produit</font>');
					else
						$("#descriptionMsg").html('');



					if ($("#lien").val() == '')
						$("#lienMsg").html('<font color="red">Insérer un lien sur le produit</font>');
					else
						$("#lienMsg").html('');

					returnValue = false;
				}else{

					$("#itemTitle").html('<ion-icon name="cube"></ion-icon>'+ $("#nom").val());
					$("#nomMsg").html('');
					$("#descriptionMsg").html('');
					$("#lienMsg").html('');
				}

				if (!returnValue)
					toastbox('errorMsg', 2000);
				
				return returnValue;
			}

			function cartFormSubmit() {

				if (checkRequieredValue())
					document.getElementById('cart_form').submit();
			}

			$(".custom-control-input" ).click(function() {

				if($(this).is(':checked') == false)
					$("#all").prop("checked", false);
			  	$("#commandAction").text("Commander ("+($("input:checked").length)+")");
			});
			

    		$("#all" ).click(function() {
			  	$("input[type=checkbox]").prop('checked', $(this).prop('checked'));

			  	var n = 0;
			  	if ($("input:checked").length > 0)
			  		n = $("input:checked").length - 1;
			  	
			  	$("#commandAction").text("Commander ("+(n)+")");
			});

			$("#commandAction").click(function() {

				if ($("input:checked").length > 0 ) {
					var param = "";
					var i = 0;

					$.each($("input[name='cartItem']:checked"), function(){
						i++;
		                param += "&item"+i+"="+$(this).val();
		            });

					var lien = '/commande/'+$("#id").val()+'/create/panier?param='+i;
					lien = lien+param;
					window.location.href = lien;
				}		
			});


			function deleteItem(item, id) {
					
				toastbox("loadingToast");	

				$.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
               	});

                $.get({
                    
                    url: '/panier/destroy/'+item,
                    success: function(response, textStatus, jqXHR) {
                    	var i = id - 1;
                       	$("#itemdiv"+i).remove();
                       	$("#loadingToast").removeClass("show");
                       	console.log(parseInt($("#cart_count").text()));  
                       	$("#cart_count").text(parseInt($("#cart_count").text()) - 1) ;             
                    },
                    error: function(msg) {}
                });
			}


		</script>
	@endsection