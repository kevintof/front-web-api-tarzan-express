<!doctype html>
<html lang="fr">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>INSCRIPTION</title>
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
    <meta name="description" content="Finapp HTML Mobile Template">
    <meta name="keywords" content="bootstrap, mobile template, cordova, phonegap, mobile, html, responsive" />
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('img/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" href="{{ asset('img/favicon.png') }}" sizes="32x32">
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}">
</head>

<body>

    <!-- loader -->
    <div id="loader">
        <img src="{{ asset('img/logo-icon.png') }}" alt="icon" class="loading-icon">
    </div>
    <!-- * loader -->

    <!-- App Header -->
    <div class="appHeader text-dark">
        <div class="left">
        </div>
        <div class="pageTitle"> BIENVENUE SUR <span class="text-warning"> TarzanExpress </span> </div>
        <div class="right">
        </div>
    </div>
    <!-- * App Header -->


    <!-- App Capsule -->
    <div id="appCapsule">

        <div class="section mt-2 text-center">
            
            <h1><img src="{{ asset('img/apple-touch-icon.png') }}" alt="image" class="imaged w36"> INSCRIPTION</h1>

            <div class="alert alert-outline-warning mb-1" role="alert">
                <div class="chip chip-media">
                    
                    <img src="{{ asset('img/228.png') }}" alt="image" class="w16">
                    
                    <span class="chip-label"> Togo</span>
                </div>

                <div class="chip chip-media">
                    
                    <img src="{{ asset('img/229.png') }}" alt="image" class="w16">
                    
                    <span class="chip-label"> Bénin</span>
                </div>

                </br>

                <h4>Connectez vous sur TarzanExpress pour vivre une meilleure expérience d'achat en ligne et passer des commandes en Chine <br><font color="red">* Champs obligatoires</font></h4>
            </div>

            @if (Route::has('user.register'))
                <div>
                    <a href="{{ route('user.login') }}" class="btn btn-warning" id="registerForm">Déjà un compte? Connectez vous maintenant</a>
                </div>
            @endif

        </div>

            <div class="section mb-5 p-2">
                <form method="POST" action="{{ route('user.register') }}" id="registrerForm">
                    @csrf
                    <h3>INFORMATIONS PERSONNELLES</h3>
                    <div class="card">
                        <div class="card-body">
                            
                            <div class="form-group basic">
                                <div class="input-wrapper">
                                    <label class="label" for="nom">Nom *</label>
                                    <input type="text" class="form-control @error('nom') is-invalid @enderror" value="{{ old('nom') }}" name="nom" placeholder="Insérer votre nom" required autocomplete="nom" autofocus>
                                    <i class="clear-input">
                                        <ion-icon name="close-circle"></ion-icon>
                                    </i>
                                </div>

                                @if($errors->any())
                                    <span class="text-danger">
                                        <strong> {{ $errors->first('nom') }} </strong>
                                    </span>
                                @endif 

                            </div>

                            <div class="form-group basic">
                                <div class="input-wrapper">
                                    <label class="label" for="prenoms">Prénoms *</label>
                                    <input type="text" class="form-control @error('prenoms') is-invalid @enderror" value="{{ old('prenoms') }}" name="prenoms" placeholder="Inérer vos prénoms" required autocomplete="prenoms" autofocus>
                                    <i class="clear-input">
                                        <ion-icon name="close-circle"></ion-icon>
                                    </i>
                                </div>

                                @if($errors->any())
                                    <span class="text-danger">
                                        <strong> {{ $errors->first('prenoms') }} </strong>
                                    </span>
                                @endif  
                            </div>

                            <div class="form-group basic">
                                <div class="input-wrapper">
                                    <label class="label" for="email">E-mail</label>
                                    <input  id="email" type="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" name="email" placeholder="Inérer votre e-mail e-mail" required autocomplete="email">
                                    <i class="clear-input">
                                        <ion-icon name="close-circle"></ion-icon>
                                    </i>
                                </div>

                                @if($errors->any())
                                    <span class="text-danger">
                                        <strong> {{ $errors->first('email') }} </strong>
                                    </span>
                                @endif  

                            </div>
                        </div>
                    </div>

                    <br>

                    <h3>INFORMATIONS DU COMPTE</h3>
                    <div class="card">
                        <div class="card-body">

                            <div class="form-group boxed">
                                <div class="input-wrapper">
                                    <label class="label" for="phone_number">Numero de téléphone</label>
                                    <div class="row">
                                        <div class="col-5">
                                            <div class="form-group boxed">
                                                <div class="input-wrapper">
                                                    <label class="label" for="pays_id">Indicatif *</label>
                                                    <select class="form-control custom-select" id="pays_id" name="pays_id">
                                                        @foreach($pays as $p)
                                                            <option data-thumbnail="{{ asset('img/pays/228.png')}}" value="{{$p->id}}" name="pays_id">
                                                               {{$p->abr}} {{$p->indicatif}} 
                                                            </option>
                                                        @endforeach
                                                        
                                                    </select>
                                                </div>
                                                  
                                            </div>
                                            
                                        </div>
                    
                                        <div class="col-7">
                                            <div class="form-group boxed">
                                                <div class="input-wrapper">

                                                    <label class="label" for="phone_number">Numéro *</label>     
                                                    <input type="tel" id="phone_number" class="form-control @error('phone_number') is-invalid @enderror"  name="phone_number" value="{{ old('phone_number') }}" required autocomplete="phone_number" autofocus placeholder="Insérer le numéro">
                                                    <i class="clear-input"><ion-icon name="close-circle"></ion-icon></i>
                                                </div>
                                            </div>
                                        </div>
                                        @if($errors->any())
                                            <span class="text-danger">
                                                <strong> {{ $errors->first('phone_number') }} </strong>
                                            </span>
                                        @endif
                                    </div>                                   

                                </div>
                            </div>   
                        </div>
                    </div>

                    <br>

                    <div class="card">
                        <div class="card-body">

                            <div class="form-group boxed">
                                
                                <div class="custom-control custom-checkbox mt-2 mb-1">
                                    <input type="checkbox" class="custom-control-input" name="checkbox" id="checkbox" value="scheckbox"> 
                                    <label class="custom-control-label" for="checkbox">
                                        J'ai un code de parrainage
                                    </label>
                                    <input id="code_parrainage" name="code_parrainage" type="text" placeholder="Isérer le code parrainage" />
                                </div>

                            </div>       

                        </div>
                    </div>

                    <br>

                    <div class="card">
                        <div class="card-body">                  
                            <div class="form-group basic">
                                <div class="input-wrapper">
                                    <label class="label" for="password">Mot de passe *</label>
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Insérez votre mot de passe" required autocomplete="new-password">
                                    <i class="clear-input">
                                        <ion-icon name="close-circle"></ion-icon>
                                    </i>
                                </div>

                                @if($errors->any())
                                    <span class="text-danger">
                                        <strong> {{ $errors->first('password') }} </strong>
                                    </span>
                                @endif                                                          
                               
                            </div>

                            <div class="form-group basic">
                                <div class="input-wrapper">
                                    <label class="label" for="password">Confirmation Mot de passe *</label>
                                    <input type="password" class="form-control" id="password-confirm" name="password_confirmation" placeholder="Répéter votre mot de passe"  required autocomplete="new-password">
                                    <i class="clear-input">
                                        <ion-icon name="close-circle"></ion-icon>
                                    </i>
                                </div>
                            </div>

                            <div class="custom-control custom-checkbox mt-2 mb-1">
                                <input type="checkbox" class="custom-control-input @error('agree') is-invalid @enderror" id="agree" name="agree"> 
                                <label class="custom-control-label" for="agree">
                                    J'accepte <a href="#" data-toggle="modal" data-target="#termsModal">Termes et conditons *</a>
                                </label>

                            </div>
                            @if($errors->any())
                                <span class="text-danger">
                                    <strong> {{ $errors->first('agree') }} </strong>
                                </span>
                            @endif 

                        </div>
                    </div>

                    <div class="form-links mt-2">

                        @if (Route::has('login'))
                            <div>
                                <a href="{{ route('user.login') }}" class="btn btn-success" >Se connecter Maintenant</a>
                            </div>
                        @endif
                    </div>

                    <div class="section mt-2" id='formValidation'>
                        <button type="submit" class="btn btn-success btn-lg btn-block" style="float:right">VALIDER</button> 
                    </div>

                    <div class="form-button-group transparent" id="loadingForm">
                        <button class="btn btn-success btn-block btn-lg" type="button" disabled>
                            <span class="spinner-border spinner-border-sm mr-05" role="status" aria-hidden="true"></span>
                            Patientez...
                        </button>
                    </div>

                    <script>
                        document.getElementById('loadingForm').style.display = 'none';
                    </script>

                </form>
            </div>

            <div id="loadingToast" class="toast-box toast-center">
                <div class="in">
                    <div class="spinner-border text-success" role="status"></div>
                    <br>
                    <div class="text">
                        Patientez SVP ...
                    </div>
                </div>
            </div>

    </div>
    <!-- * App Capsule -->


    <!-- Terms Modal -->
    @include('frontend.profile.termsModal')
    <!-- * Terms Modal -->


    <!-- ///////////// Js Files ////////////////////  -->
    <!-- Jquery -->
    <script src="{{ asset('js/lib/jquery-3.4.1.min.js') }}"></script>
    <!-- Bootstrap-->
    <script src="{{ asset('js/lib/popper.min.js') }}"></script>
    <script src="{{ asset('js/lib/bootstrap.min.js') }}"></script>
    <!-- Ionicons -->
    <script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>
    <!-- Owl Carousel -->
    <script src="{{ asset('js/plugins/owl-carousel/owl.carousel.min.js') }}"></script>
    <!-- Base Js File -->
    <script src="{{ asset('js/base.js') }}"></script>

    <script type="text/javascript">
        $(function(){
            $("#registrerForm" ).submit(function() {
                $("#formValidation").hide();
                document.getElementById('loadingForm').style.display = 'block';
                toastbox('loadingToast');
            });

            $("#loginForm" ).click(function() {
                $("#formValidation").hide();
                document.getElementById('loadingForm').style.display = 'block';
            });

        });

        $(function () {
        $('input[name="code_parrainage"]').hide();


        $('input[name="checkbox"]').on('click', function () {
            if ($(this).prop('checked')) {
                $('input[name="code_parrainage"]').fadeIn();
            } else {
                $('input[name="code_parrainage"]').hide();
            }
        });
    });
    </script>


</body>

</html>