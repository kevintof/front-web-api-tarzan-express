<!doctype html>
<html lang="fr">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>TARZAN EXPRESS | AUTHENTIFICATION</title>
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
    <meta name="description" content="Votre coursier en chine">
    <meta name="keywords" content="tarzan-express, TARZAN-EXPRESS,togo, benin, e-commerce, dropshipping, chine, livraison, coursiers, courses" />
    <meta name="author" content="Kevin TOFFA">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('img/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" href="{{ asset('img/favicon.png') }}" sizes="32x32">
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}">
</head>

<body>

    <!-- loader -->
    <div id="loader">
        <img src="{{ asset('img/logo-icon.png') }}" alt="icon" class="loading-icon">
    </div>
    <!-- * loader -->

    <!-- App Header -->
    <div class="appHeader text-dark">
        <div class="left">
        </div>
        <div class="pageTitle"> BIENVENUE SUR <span class="text-warning"> TarzanExpress </span> </div>
        <div class="right">
        </div>
    </div>
    <!-- * App Header -->

    <!-- App Capsule -->
    <div id="appCapsule">

        <div class="section mt-2 text-center">
            <h1><img src="{{ asset('img/apple-touch-icon.png') }}" alt="image" class="imaged w36"> AUTHENTIFICATION</h1>
            <div class="alert alert-outline-warning mb-1" role="alert">
                <div class="chip chip-media">
                    
                    <img src="{{ asset('img/228.png') }}" alt="image" class="w16">
                    <span class="chip-label"> Togo</span>
                </div>

                <div class="chip chip-media">
                    
                    <img src="{{ asset('img/229.png') }}" alt="image" class="w16">
                    <span class="chip-label"> Bénin </span>
                </div>
                
                </br>
                </br>

                <h4>Connectez vous sur TarzanExpress pour vivre une meilleure expérience d'achat en ligne et passer des commandes en Chine</h4>

            </div>
            @if (Route::has('user.register'))
                <div>
                    <a href="{{ route('user.register') }}" class="btn btn-warning" id="registerForm">Pas de compte? Inscrivez vous maintenant</a>
                </div>
            @endif
        </div>
        <div class="section mb-5 p-2">
            
            <form method="POST" action="{{ route('user.login') }}" id="loginForm">
                @csrf

                @if ($errors->any())
                    <font color="red"><center>Les informations ne correspondent pas ... / Veuillez vous inscrire</center> </font>
                @endif

                <div class="card">
                    <div class="card-body pb-1">
                        
                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="label" for="phone_number">Numero de téléphone</label>
                                <div class="row">
                                    <div class="col-5">
                                        
                                        <div class="form-group boxed">
                                            <div class="input-wrapper">
                                                <label class="label" for="pays_id">Pays</label>
                                                <select class="form-control custom-select" id="pays_id" name="pays_id">

                                                    @foreach($pays as $p)
                                                        <option value="{{$p->id}}" name="pays_id">
                                                           {{$p->abr}} {{$p->indicatif}} 
                                                        </option>
                                                    @endforeach
                                                    
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-7">

                                        <div class="form-group boxed">
                                            <div class="input-wrapper">

                                                <label class="label" for="phone_number">Numéro</label>     
                                                <input type="tel" id="phone_number" class="form-control @error('phone_number') is-invalid @enderror"  name="phone_number" value="{{ old('phone_number') }}" required autocomplete="phone_number" autofocus placeholder="Insérer le numéro">
                                                <i class="clear-input"><ion-icon name="close-circle"></ion-icon></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>                               

                                @error('phone_number')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
        
                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="label" for="password">Mot de passe</label>
                                <input type="password" class="form-control  @error('password') is-invalid @enderror" id="password"  name="password" required autocomplete="current-password" placeholder="Insérer votre mot de passe">
                                <i class="clear-input"><ion-icon name="close-circle"></ion-icon></i>

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-5">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Retenir?') }}
                                    </label>
                                </div>
                            </div>
                            <div class="col-7">
                                @if (Route::has('password.request'))
                                    <!--div><a href="{{ route('password.request') }}" class="text-muted">Mot de passe oublié?</a></div-->
                                @endif
                            </div>    
                        </div>
                    </div>
                </div> 

                <div class="form-button-group transparent" id="submitForm">
                    <button type="submit" class="btn btn-success btn-block btn-lg" id="submit">SE CONNECTER</button>
                </div>

                <div class="form-button-group transparent" id="loadingForm">
                    <button class="btn btn-success btn-block btn-lg" type="button" disabled>
                        <span class="spinner-border spinner-border-sm mr-05" role="status" aria-hidden="true"></span>
                        Patientez...
                    </button>
                </div>

                <script>
                    document.getElementById('loadingForm').style.display = 'none';
                </script>
                    

            </form>
        </div>


        @if ($message = Session::get('success'))
            <div id="notification" class="toast-box toast-center show">
                <div class="in">
                    <ion-icon name="checkmark-circle" class="text-success"></ion-icon>
                    <div class="text">
                        {{$message}}
                    </div>

                </div>
                <button type="button" class="btn btn-sm btn-text-light close-button"> D'ACCORD </button>
            </div>
        @endif

    </div>
    <!-- * App Capsule -->


    <!-- ///////////// Js Files ////////////////////  -->
    <!-- Jquery -->
    <script src="{{ asset('js/lib/jquery-3.4.1.min.js') }}"></script>
    <!-- Bootstrap-->
    <script src="{{ asset('js/lib/popper.min.js') }}"></script>
    <script src="{{ asset('js/lib/bootstrap.min.js') }}"></script>
    <!-- Ionicons -->
    <script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>
    <!-- Owl Carousel -->
    <script src="{{ asset('js/plugins/owl-carousel/owl.carousel.min.js') }}"></script>
    <!-- Base Js File -->
    <script src="{{ asset('js/base.js') }}"></script>

    <script type="text/javascript">
        $(function(){
            $("#loginForm" ).submit(function() {
                $("#submitForm").hide();
                document.getElementById('loadingForm').style.display = 'block';
            });

            $("#registerForm" ).click(function() {
                $("#submitForm").hide();
                document.getElementById('loadingForm').style.display = 'block';
            });

        });
    </script>

</body>

</html>