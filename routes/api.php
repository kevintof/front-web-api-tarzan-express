<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/* use App\Http\Controllers\AuthController; */
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
}); */

Route::post('login', 'Auth\ApiAuthController@login')->name('api.login');
Route::post('register', 'Auth\ApiAuthController@register')->name('api.register');
Route::get('pays','User\App\PaysController@getPays')->name('api.pays');
Route::get('transactions/checkAll','User\App\TransactionsController@checkAllTransactionStateOnPaygate');
Route::get('/files/guide','User\App\FilesController@getGuidePdf');
Route::post('/users/check_phone_number','Auth\ApiAuthController@checkPhoneNumber');
Route::post('/users/change_password','Auth\ApiAuthController@changePassword');
Route::post('/notifications/trigger_missed/{userId}','User\App\NotificationController@triggerMissedPushNotification');
Route::group([
    'middleware' => ['jwt.verify'],
    
], function ($router) {
    
    /*Route::post('/logout', [AuthController::class, 'logout']);
    Route::post('/refresh', [AuthController::class, 'refresh']);
    Route::get('/user-profile', 'Auth\ApiAuthController@userProfile');*/   
    Route::get('commande','User\App\CommandesController@apiIndex')->name('api.commandes');
    Route::get('commande/show/{id}','User\App\CommandesController@show')->name('api.commande.show'); 
    Route::post('commande/store','User\App\CommandesController@store')->name('api.commande.store');
    Route::post('commande/checkout','User\App\TransactionsController@store');
    Route::post('commande/checkout/wallet','User\App\TransactionsController@payCommandWithWallet');
    Route::get('mode-paiements','User\App\TransactionsController@getModePaiement');
    Route::get('mode-livraisons','User\App\AutomationController@getModeLivraisons')->name('api.mode-livraisons');
    
    Route::post('messages/send/{id}','MessageController@sendMessage')->name('api.user.message.send');
    Route::post('messages/send/picture/{id}','MessageController@send_pictures_as_message')->name('api.user.message.pictures.send');
    Route::get('messages/commandes/{id}','User\App\MessageController@getMessagesfromCommande')->name('api.commande.chat');
    Route::put('messages/commandes/{id}/update_messages_states','MessageController@messagesFromCommandesIsRead');
    Route::get('messages/messages-non-lus','MessageController@messagesNonlus')->name('api.messages-nonlus.commande');
    Route::get('/notifications', 'User\App\NotificationController@index')->name('api.notifications');
    Route::get('/notifications/not-read', 'User\App\NotificationController@getNotReadNotifications');
    Route::get('/notifications_view', 'User\App\NotificationController@show')->name('api.notifications.view');
    
    Route::put('/users/update/{id}', 'User\App\UserController@edit');
    Route::put('users/change_password','User\App\UserController@changePassword');
    Route::put('/notifications/update/read_state','User\App\NotificationController@updateReadStateOfNotification');
    Route::put('/notifications/update/read_state/{id}','User\App\NotificationController@state');
    Route::put('/notifications/update/is_clicked/{id}','User\App\NotificationController@changeIsClickedState');
    //Route::post('/notifications/trigger_missed/{userId}','User\App\NotificationController@triggerMissedPushNotification');
    

    Route::put('/users/update/{id}', 'User\App\UserController@edit');
    Route::get('users/affilies/count','User\App\UserController@show_user_affilies');
    Route::get('/transactions/check/{commande_id}','User\App\TransactionsController@checkpaygate');
    Route::get('/transactions/historique','User\App\TransactionsController@historique');
    
    Route::post('/portefeuilles/charger','User\App\PortefeuilleController@chargerParPaygates');
    Route::get('/portefeuilles','User\App\PortefeuilleController@fetch');

    Route::get('/portefeuilles/check/last-transaction','User\App\PortefeuilleController@checkTransactionOnPaygates');
    Route::get('/pays/{id}/villes','User\App\PaysController@getVillesFromPays');
    Route::get('/villes/{id}/agences','User\App\PaysController@getAgencesFromVilles');
    
    ///home/kevin/Documents/front-chinashopping/frontchinashopping
    Route::get('/service_client/chat','User\App\ServiceClientController@show');
    Route::post('/service_client/chat/send/{id}','MessageClientController@sendMessage');
    
    Route::post('/articles/create','User\App\ActualitesController@store');

    Route::get('/articles','User\App\ActualitesController@index');

    
    
   
});

