<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// Admin routes



// Vendor routes
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('user.login');
Route::post('/login', 'Auth\LoginController@login')->name('user.login.submit');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/register', 'Auth\RegisterController@showRegisterForm')->name('user.register');
Route::post('/register', 'Auth\RegisterController@create')->name('user.register.submit');
Route::post('/messages/send/{id}','MessageController@sendMessage')->name('user.message.send');
Route::post('/messages/send/picture/{id}','MessageController@send_pictures_as_message')->name('user.message.picture.send');
Route::post('propos/service_client/send/{id}','MessageClientController@sendMessage')->name('user.serviceClient.send');

Route::group(['middleware' => ['auth.user']], function () {

	// Home AliExpress Taobao Alibaba
	Route::get('/','User\App\HomeController@index')->name('user.home');
	Route::get('taobao','User\App\HomeController@indexTaobao')->name('user.taobao');
	Route::get('aliexpress','User\App\HomeController@indexAliexpress')->name('user.aliexpress');
	Route::get('alibaba','User\App\HomeController@indexAlibaba')->name('user.alibaba');
    Route::get('shopping','User\App\HomeController@indexShopping')->name('user.shopping.index');



    //Products
    Route::get('produit/link','User\App\ProduitsController@get_link')->name('user.produit.link');

    

	// Commandes
	Route::get('commande/','User\App\CommandesController@index')->name('user.commande.index');
	Route::get('commande/show/{id}','User\App\CommandesController@show')->name('user.commande.show');
	Route::get('commande/create','User\App\CommandesController@create')->name('user.commande.create');
	Route::post('commande/store','User\App\CommandesController@store')->name('user.commande.store');
	Route::get('commande/edit/{id}','User\App\CommandesController@edit')->name('user.commande.edit');
	Route::put('commande/update/{id}','User\App\CommandesController@update')->name('user.commande.update');
    Route::get('commande/recap/{id}','User\App\CommandesController@recap')->name('user.commande.recap');
	Route::get('commande/create/panier','User\App\CommandesController@createByCart')->name('user.commande.createByCart');

	// user
	Route::get('profile/','User\App\ProfileController@index')->name('user.profile.index');
    Route::get('profile/edit/{arg}','User\App\ProfileController@edit')->name('user.profile.edit');
	Route::post('profile/update/{arg}','User\App\ProfileController@update')->name('user.profile.update');
    Route::get('profile/verification','User\App\ProfileController@verification')->name('user.profile.verification');
    Route::post('profile/verification','User\App\ProfileController@validation');


	// Panier
    Route::get('panier/','User\App\PanierController@show')->name('user.panier');
	Route::post('panier/','User\App\PanierController@store');
	Route::put('panier/update/{id}','User\App\PanierController@update')->name('user.panier.update');
    Route::get('panier/reset', 'User\App\PanierController@reset')->name('user.panier.reset');
    Route::get('panier/destroy/{id}', 'User\App\PanierController@destroy')->name('user.panier.destroy');
    Route::get('panier/get_all_items','User\App\PanierController@get_all_items')->name('user.panier.items');
    Route::get('panier/save/produit','User\App\PanierController@get_saveApp')->name('user.produit.save');
    Route::post('panier/save/produit','User\App\PanierController@post_saveApp');

    // TYPE DE LIVRAISON
    Route::get('auto/type-{arg}','User\App\AutomationController@getType')->name('user.get.automation');
	Route::get('auto/getKey','User\App\AutomationController@getKey')->name('user.app.key');

	
    // Colis

    // Messages
    Route::get('/messages/client','User\App\MessageController@getCommandes')->name('user.commande.messages');
    Route::get('messages/{id}','User\App\MessagesController@index')->name('user.messages.index');
    Route::get('messages/client/commande/{id}','User\App\MessageController@getMessagesfromCommande')->name('user.commande.chat');
    

    //Notifications
    Route::get('/notifications', 'User\App\NotificationController@index')->name('user.notification');
    Route::get('/notifications_view', 'User\App\NotificationController@show')->name('user.notifications');
    Route::get('/notifications/update/{id}', 'User\App\NotificationController@state')->name('user.notifications.state');
    Route::get('/notifications/is_clicked/{id}','User\App\NotificationController@changeIsClickedState')->name('user.notifications.is_clicked');
    Route::get('/notificationsyahoo', 'User\App\NotificationController@basic')->name('user.notifications.basic');
   

	// Actualités
    Route::get('/actualites', 'User\App\ActualitesController@index')->name('user.actualites.index');
    Route::get('/actualites/{id}', 'User\App\ActualitesController@show')->name('user.actualites.show');


	// Transactions
    Route::get('/transaction/index','User\App\TransactionsController@index')->name('user.transaction.index');
    Route::get('/transaction/check-state','User\App\TransactionsController@check_state')->name('user.transaction.checkStat');
    Route::post('/transaction/store','User\App\TransactionsController@store')->name('user.transaction.store');
    Route::post('/transaction/paygate/store','User\App\TransactionsController@store_paygate')->name('user.transaction.storePaygate');


    //Services client
    Route::get('propos/service_client','User\App\ServiceClientController@show')->name('user.serviceClient.index');


	// Automation
	Route::get('propos/livraison/{mode}','User\App\AutomationController@index')->name('user.propos.livraison');

    //Informations
    Route::get('propos/aide','User\App\AppInformationsController@aide')->name('user.Informations.aide');
    Route::get('propos/presentation','User\App\AppInformationsController@presentation')->name('user.Informations.presentation');
	Route::get('propos/termes_conditions','User\App\AppInformationsController@termes_conditions')->name('user.Informations.termes_conditions');

    Route::get('/not-found','User\App\ErrorHandlingController@notFound')->name('notFound');
    
    Route::get('/unauthorized','User\App\ErrorHandlingController@unauthorized')->name('unauthorized');

    //Checkout Paiement
    Route::get('checkout/getAmount-{arg}/{id}','User\App\CommandesController@getAmount')->name('user.checkout.getAmount');


    Route::get('pays/{id}/villes','User\App\PaysController@getVillesFromPays')->name('user.pays.villes');

    Route::get('villes/{id}/agences','User\App\PaysController@getAgencesFromVilles')->name('user.villes.agences');
});


Route::group(['prefix' => 'admin', 'middleware' => ['auth.admins']], function () {
    
    // Route::get('/', 'User\Admin\AdminController@index')->name('admin.dashboard');
});



