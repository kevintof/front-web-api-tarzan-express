$(document).ready(function(){
    function make_chat_dialog_box(commande_id, to_user_name)
    {
       var modal_content = '<div id="user_dialog_'+commande_id+'" class="user_dialog" title="commande N° '+to_user_name+'">';
       modal_content += '<div style="height:350px; border:1px solid #ccc; overflow-y: scroll; margin-bottom:24px; padding:16px;" class="chat_history" data-touserid="'+commande_id+'" id="chat_history_'+commande_id+'">';
       modal_content += fetch_user_chat_history(commande_id);
       modal_content += '</div>';
       modal_content += '<div class="form-group input-group mb-3">';
       modal_content += '<div class="input-group-prepend"><span class="input-group-text upload-image" id="basic-addon1"><i class="fas fa-camera"></i></span></div><textarea name="chat_message_'+commande_id+'" id="chat_message_'+commande_id+'" class="form-control chat_message"></textarea>';
       modal_content += '</div><div class="form-group" align="right">';
       modal_content+= '<form style="display:none;" method="post" id="uploadImage" action="/messages/send/picture/'+commande_id+'"><input type="file" name="cmd_pictures" id="uploadFile" accept=".jpg,.png"/></form>'
       modal_content+= '<button type="button" name="send_chat" id="'+commande_id+'" class="btn btn-info send_chat">Envoyer</button></div></div>';
       
       $('#user_model_details').html(modal_content);
    }

    $(document).on('click', '.send_chat', function(){
        var commande_id = $(this).attr('id');
        var chat_message = $('#chat_message_'+commande_id).val();
        $('textarea').val('');
        $('textarea').attr('placeholder','Votre message est en cours d\'envoi ...');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
           });
        $.post({
         url:"/messages/send/"+commande_id,
         dataType: "json",
         data:{ contenu:chat_message},
         success:function(response, textStatus, jqXHR)
         {
            
            $('textarea').val('');
             $('textarea').attr('placeholder','Veuillez saisir votre message');
             /* $('textarea').attr('placeholder','Votre est en cours d\'envoi ...'); */
            
            
         },
         error: function(msg){
             
         }
        })
       });

       $(document).on('click', '.ui-button-icon', function(){
        $('.user_dialog').dialog('destroy').remove();
       });

    function sendMessage(){

    }

    function fetch_user_chat_history(commande_id)
    {
        $.ajax({
        url:"/admin/commandes/"+commande_id+"/messages",
        method:"GET",
        //data:{commande_id:commande_id},
        success:function(data){
            $('#chat_history_'+commande_id).html(data);
        }
        })
    }

    function update_not_read_messages(id){
      $.get({
          url:'/admin/notifications/commandes/'+id+'/update-messages-status',
          success: function(data){

          }   
      })  
    }
         $(document).on('click','.upload-image',function(){
            $("#uploadFile").trigger("click");
        }) 
        /* $(".upload-image").click(function(){
            $("#uploadFile").trigger("click");
        }); */
        $(document).on('change','#uploadFile', function(){
            console.log('change');
            $('#uploadImage').ajaxSubmit({
            target: "#group_chat_message",
            resetForm: true,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            });
        }); 
        $(document).on('click', '.start_chat', function(){
           console.log('cccc') 
        var commande_id = $(this).data('commandeid');
        var reference = $(this).data('reference');
        $('#'+commande_id+' span.badge').remove();
        make_chat_dialog_box(commande_id, reference);
        $("#user_dialog_"+commande_id).dialog({
         autoOpen:false,
         width:400
        });
        
        update_not_read_messages(commande_id)
        $('#user_dialog_'+commande_id).dialog('open');

        setTimeout(function(){ $( '#chat_history_'+commande_id ).scrollTop( 3500 ); }, 1500);
        $( '#chat_history_'+commande_id ).scrollTop( 3500 ) 
       });

       var pusher = new Pusher('710a42ce02a3f4836392');
       var channel = pusher.subscribe('chat_channel');
       channel.bind('chat', function(data) {
      
       var chatmess = '<li class="round" style="border-bottom:1px dotted #ccc;margin-left:48px;margin-right:0"><div style="margin-right:0; padding:12px"><b class="text-success"></b>'+data.contenu+'</div></li><div align="right"><small><em>'+data.date+'</em></small></div>'
       var chatmess1 = '<li class="second" style="border-bottom:1px dotted #ccc;margin-right:48px"><div style="padding:12px"><b class="text-danger">'+data.sender_name+'</b> <br>'+data.contenu+'</div></li><div align="left"><small><em>'+data.date+'</em></small></div>';
        
       if(data.is_admin == true && $('.chat_history').attr('data-touserid') == data.commande_id)
       {
          if(!data.image)
          {
            $('#chat_history_'+data.commande_id).append(chatmess);
          }
          else{
              var url = "/messages-commandes/"+data.commande_id+"/"+data.image;
              var messimg =  '<li class="round" style="border-bottom:1px dotted #ccc;margin-left:48px;margin-right:0"><div style="margin-right:0; padding:12px"><b class="text-success"></b><img src="'+url+'" class="img-thumbnail" width="200" height="160" /></div></li><div align="right"><small><em>'+data.date+'</em></small></div>'
              $('#chat_history_'+data.commande_id).append(messimg);
          }
          

          console.log('admin case');
          $( '#chat_history_'+data.commande_id ).scrollTop( 3500 )
       }

       
       
       if(data.is_admin == false && $('.chat_history').attr('data-touserid') == data.commande_id )
       {
           if(!data.image)
           {
            $('#chat_history_'+data.commande_id).append(chatmess1);
           }
           else{
               var url = "/messages-commandes/"+data.commande_id+"/"+data.image
              var imgmess = '<li class="second" style="border-bottom:1px dotted #ccc;margin-right:48px"><div style="padding:12px"><b class="text-danger">'+data.sender_name+'</b> <br><img src="'+url+'" class="img-thumbnail" width="200" height="160"/></div></li><div align="left"><small><em>'+data.date+'</em></small></div>'; 
              $('#chat_history_'+data.commande_id).append(imgmess);
            }
        
        $( '#chat_history_'+data.commande_id ).scrollTop( 3500 ); 
                
       }
       
              
       /* $(".list-unstyled").scrollTop($(".list-unstyled .li:last")[0].scrollHeight); */
       /*$ ("html,body").animate({scrollTop: $('.list-unstyled .li:last').offset().top - 30}); */
       $('textarea').attr('placeholder','Veuillez saisir votre message');
        
    });
       
})
