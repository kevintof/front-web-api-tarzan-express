function action(arg) {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
	
	$.get({

        url: '/profile/edit/'+arg,
        success: function (response) {

            $("#editContent").html('<div class="overlay" id="editMsg"></div>'+response.html);
            $('#edit').modal('show');
        },
         error: function (response) {
             console.log(response);
        }
    });
}


function submit(arg) {

    var data;
    var action = false;

    if (arg == "nom"){
        data = {nom:$('#nom').val(), prenoms:$('#prenoms').val()};
        action = true;
    }
 
    if (arg == "phone_number"){
        data = {phone_number:$('#phonenumber').val()};
        action = true;
    }

    if (arg == "email"){
        data = {email:$('#email').val()};
        action = true;
    }

    if(arg == "password"){
        data = {oldpassword:$('#oldpassword').val(), password:$('#password').val()};

        if ($('#password').val() == $('#confirm').val())
            action = true;
        else{
            action = false;


        }

    }

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    if (action){
        $("#editMsg").html('<center>Enregistrement en cours ... <div class="spinner-border text-success" role="status"></div></center>');
        $.post({

            url: '/profile/update/'+arg,
            data: data,
            success: function (response) {

                $('#edit').modal('hide');
                if (arg == "nom")
                    $('#name').html($('#nom').val() +' '+ $('#prenoms').val());

                if (arg == "phone_number")
                    $('#phone').html($('#phonenumber').val());

                if (arg == "email")
                    $('#email').html($('#email').val());
                
            },
            error: function (response) {

                console.log(response.responseJSON);
                $("#editMsg").html('<center><font color="red">'+response.responseJSON.msg_error+'</font></center>');
            }
        });
    }
        
}