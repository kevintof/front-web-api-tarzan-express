-- convert Laravel migrations to raw SQL scripts --

-- migration:2014_10_12_100000_create_password_resets_table --
create table `password_resets` (
  `email` varchar(255) not null, 
  `token` varchar(255) not null, 
  `created_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `password_resets` 
add 
  index `password_resets_email_index`(`email`);

-- migration:2018_12_23_120000_create_shoppingcart_table --
create table `shoppingcart` (
  `identifier` varchar(255) not null, 
  `instance` varchar(255) not null, 
  `content` longtext not null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `shoppingcart` 
add 
  primary key `shoppingcart_identifier_instance_primary`(`identifier`, `instance`);

-- migration:2020_06_22_124623_adresses --
SET 
  FOREIGN_KEY_CHECKS = 0;;
create table `adresses` (
  `id` int unsigned not null auto_increment primary key, 
  `pays` varchar(100) not null, 
  `ville` varchar(100) not null, 
  `quartier` varchar(200) not null, 
  `adresse1` varchar(200) not null, 
  `adresse2` varchar(200) null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';

-- migration:2020_06_22_125043_commandes --
SET 
  FOREIGN_KEY_CHECKS = 0;;
create table `commandes` (
  `id` int unsigned not null auto_increment primary key, 
  `reference` varchar(100) not null, 
  `user_id` int unsigned not null, 
  `details_commande_id` int unsigned not null, 
  `statut_id` int unsigned not null, 
  `montant_livraison` int null, 
  `montant_commande` int null, 
  `montant_service` int null, 
  `information` varchar(255) null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `commandes` 
add 
  constraint `commandes_user_id_foreign` foreign key (`user_id`) references `users` (`id`) on delete restrict on update restrict;
alter table 
  `commandes` 
add 
  constraint `commandes_details_commande_id_foreign` foreign key (`details_commande_id`) references `details_commandes` (`id`) on delete restrict on update restrict;
alter table 
  `commandes` 
add 
  constraint `commandes_statut_id_foreign` foreign key (`statut_id`) references `automations` (`id`) on delete restrict on update restrict;

-- migration:2020_06_22_125130_cotation --
SET 
  FOREIGN_KEY_CHECKS = 0;;
create table `cotation` (
  `id` int unsigned not null auto_increment primary key, 
  `montant` int not null, 
  `paiement_id` int unsigned not null, 
  `date_retour_prevu` text null, 
  `commentaire` text null, 
  `fichier_in` varchar(255) null, 
  `fichier_out` varchar(255) null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `cotation` 
add 
  constraint `cotation_paiement_id_foreign` foreign key (`paiement_id`) references `transactions` (`id`) on delete restrict on update restrict;

-- migration:2020_06_22_125206_details_commande --
SET 
  FOREIGN_KEY_CHECKS = 0;;
create table `details_commandes` (
  `id` int unsigned not null auto_increment primary key, 
  `date_commande` varchar(255) not null, 
  `date_prevu_livraison_min` varchar(255) null, 
  `date_prevu_livraison_max` varchar(255) null, 
  `date_livraison` varchar(255) null, 
  `transaction_id` int unsigned null, 
  `commentaire` text null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `details_commandes` 
add 
  constraint `details_commandes_transaction_id_foreign` foreign key (`transaction_id`) references `transactions` (`id`) on delete restrict on update restrict;

-- migration:2020_06_22_125302_produits --
SET 
  FOREIGN_KEY_CHECKS = 0;;
create table `produits` (
  `id` int unsigned not null auto_increment primary key, 
  `nom` varchar(255) not null, 
  `description` text null, 
  `lien` text not null, 
  `images` varchar(255) null, 
  `poids` int null, 
  `montant` int null, 
  `quantite` int not null, 
  `statut` varchar(255) null, 
  `colis_id` int unsigned not null, 
  `unite_id` int unsigned null, 
  `details_unite` varchar(255) null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `produits` 
add 
  constraint `produits_colis_id_foreign` foreign key (`colis_id`) references `colis` (`id`) on delete restrict on update restrict;
alter table 
  `produits` 
add 
  constraint `produits_unite_id_foreign` foreign key (`unite_id`) references `automations` (`id`) on delete restrict on update restrict;

-- migration:2020_06_22_125321_user --
create table `users` (
  `id` int unsigned not null auto_increment primary key, 
  `nom` varchar(255) not null, 
  `prenoms` varchar(255) not null, 
  `phone_number` varchar(255) not null, 
  `email` varchar(255) not null, 
  `agree` varchar(255) not null, 
  `pays_id` int unsigned not null, 
  `code_parrainage_id` int unsigned null, 
  `email_verified_at` timestamp null, 
  `password` varchar(255) not null, 
  `remember_token` varchar(100) null, 
  `verification` int not null default '0', 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `users` 
add 
  constraint `users_pays_id_foreign` foreign key (`pays_id`) references `pays` (`id`) on delete restrict on update restrict;
alter table 
  `users` 
add 
  constraint `users_code_parrainage_id_foreign` foreign key (`code_parrainage_id`) references `code_affiliations` (`id`) on delete restrict on update restrict;
alter table 
  `users` 
add 
  unique `users_phone_number_unique`(`phone_number`);

-- migration:2020_06_30_164850_automation --
SET 
  FOREIGN_KEY_CHECKS = 0;;
create table `automations` (
  `id` int unsigned not null auto_increment primary key, 
  `group` varchar(255) not null, 
  `desc` varchar(255) not null, 
  `param1` varchar(255) null, 
  `param2` varchar(255) null, 
  `param3` varchar(255) null, 
  `param4` varchar(255) null, 
  `param5` varchar(255) null, 
  `param6` varchar(255) null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';

-- migration:2020_06_30_170428_colis --
SET 
  FOREIGN_KEY_CHECKS = 0;;
create table `colis` (
  `id` int unsigned not null auto_increment primary key, 
  `reference` varchar(100) not null, 
  `poids` double(8, 2) null, 
  `transport` int null, 
  `frais_livraison` int null, 
  `transaction_id` int unsigned null, 
  `type_livraison_id` int unsigned null, 
  `images` text null, 
  `commentaires` varchar(255) null, 
  `adresse_livraison_id` int unsigned not null default '1', 
  `details_commande_id` int unsigned not null, 
  `lots_id` int unsigned null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `colis` 
add 
  constraint `colis_transaction_id_foreign` foreign key (`transaction_id`) references `transactions` (`id`) on delete restrict on update restrict;
alter table 
  `colis` 
add 
  constraint `colis_type_livraison_id_foreign` foreign key (`type_livraison_id`) references `automations` (`id`) on delete restrict on update restrict;
alter table 
  `colis` 
add 
  constraint `colis_adresse_livraison_id_foreign` foreign key (`adresse_livraison_id`) references `adresses` (`id`) on delete restrict on update restrict;
alter table 
  `colis` 
add 
  constraint `colis_details_commande_id_foreign` foreign key (`details_commande_id`) references `details_commandes` (`id`) on delete restrict on update restrict;
alter table 
  `colis` 
add 
  constraint `colis_lots_id_foreign` foreign key (`lots_id`) references `lots` (`id`) on delete cascade on update restrict;

-- migration:2020_06_30_175505_transactions --
SET 
  FOREIGN_KEY_CHECKS = 0;;
create table `transactions` (
  `id` int unsigned not null auto_increment primary key, 
  `reference` varchar(255) not null, 
  `payement_reference` varchar(255) null, 
  `montant` varchar(255) not null, 
  `mode_paiement_id` int unsigned not null, 
  `etat_paiement_id` int unsigned not null, 
  `client_id` int unsigned not null, 
  `phone_number` varchar(255) not null, 
  `attempt` int not null, 
  `commentaire` text null, 
  `date` varchar(255) not null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `transactions` 
add 
  constraint `transactions_mode_paiement_id_foreign` foreign key (`mode_paiement_id`) references `automations` (`id`) on delete restrict on update restrict;
alter table 
  `transactions` 
add 
  constraint `transactions_etat_paiement_id_foreign` foreign key (`etat_paiement_id`) references `automations` (`id`) on delete restrict on update restrict;
alter table 
  `transactions` 
add 
  constraint `transactions_client_id_foreign` foreign key (`client_id`) references `users` (`id`) on delete restrict on update restrict;

-- migration:2020_06_30_183406_livraison_details --
SET 
  FOREIGN_KEY_CHECKS = 0;;
create table `livraison_details` (
  `id` int unsigned not null auto_increment primary key, 
  `position` varchar(255) not null, 
  `commentaire` varchar(255) not null, 
  `date` varchar(255) not null, 
  `colis_id` int unsigned not null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `livraison_details` 
add 
  constraint `livraison_details_colis_id_foreign` foreign key (`colis_id`) references `colis` (`id`) on delete restrict on update restrict;

-- migration:2020_06_30_233010_articles --
SET 
  FOREIGN_KEY_CHECKS = 0;;
create table `articles` (
  `id` int unsigned not null auto_increment primary key, 
  `titre` varchar(255) not null, 
  `photo_avant` varchar(255) not null, 
  `background` varchar(255) not null, 
  `contenu` text not null, 
  `type_id` int unsigned not null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `articles` 
add 
  constraint `articles_type_id_foreign` foreign key (`type_id`) references `automations` (`id`) on delete restrict on update restrict;

-- migration:2020_08_02_173732_admin --
create table `admins` (
  `id` int unsigned not null auto_increment primary key, 
  `nom` varchar(255) not null, 
  `prenoms` varchar(255) not null, 
  `email` varchar(255) not null, 
  `pays_id` int unsigned not null, 
  `email_verified_at` timestamp null, 
  `password` varchar(255) not null, 
  `remember_token` varchar(100) null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `admins` 
add 
  constraint `admins_pays_id_foreign` foreign key (`pays_id`) references `pays` (`id`) on delete restrict on update restrict;
alter table 
  `admins` 
add 
  unique `admins_email_unique`(`email`);

-- migration:2020_09_09_214542_pays --
SET 
  FOREIGN_KEY_CHECKS = 0;;
create table `pays` (
  `id` int unsigned not null auto_increment primary key, 
  `nom` varchar(255) not null, 
  `abr` varchar(255) not null, 
  `indicatif` varchar(255) not null, 
  `icone` varchar(255) not null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `pays` 
add 
  unique `pays_indicatif_unique`(`indicatif`);

-- migration:2020_09_26_144020_create_notifications_table --
SET 
  FOREIGN_KEY_CHECKS = 0;;
create table `notifications` (
  `id` int unsigned not null auto_increment primary key, 
  `etatLecture` tinyint(1) not null, 
  `date` datetime not null, 
  `type` varchar(255) not null, 
  `commande_id` int unsigned not null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `notifications` 
add 
  constraint `notifications_commande_id_foreign` foreign key (`commande_id`) references `commandes` (`id`) on delete restrict on update restrict;

-- migration:2020_09_30_124030_create_messages_table --
create table `messages` (
  `id` bigint unsigned not null auto_increment primary key, 
  `contenu` varchar(255) null, 
  `date` datetime not null, 
  `is_admin` tinyint(1) not null, 
  `commande_id` int unsigned not null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `messages` 
add 
  constraint `messages_commande_id_foreign` foreign key (`commande_id`) references `commandes` (`id`) on delete restrict on update restrict;

-- migration:2020_10_04_113538_create_message_clients_table --
create table `message_clients` (
  `id` bigint unsigned not null auto_increment primary key, 
  `contenu` varchar(255) not null, 
  `date` datetime not null, 
  `is_admin` tinyint(1) not null, 
  `client_id` int unsigned not null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `message_clients` 
add 
  constraint `message_clients_client_id_foreign` foreign key (`client_id`) references `users` (`id`) on delete restrict on update restrict;

-- migration:2020_10_05_101305_create_notification_admins_table --
create table `notification_admins` (
  `id` int unsigned not null auto_increment primary key, 
  `etatLecture` tinyint(1) not null, 
  `date` datetime not null, 
  `type` varchar(255) not null, 
  `commande_id` int not null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';

-- migration:2020_10_13_000050_ressources --
SET 
  FOREIGN_KEY_CHECKS = 0;;
create table `ressources` (
  `id` int unsigned not null auto_increment primary key, 
  `images` int not null, `type_id` int unsigned not null, 
  `created_at` timestamp null, `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `ressources` 
add 
  constraint `ressources_type_id_foreign` foreign key (`type_id`) references `automations` (`id`) on delete restrict on update restrict;

-- migration:2020_10_14_143536_add_readstate_tomessages_table --
alter table 
  `messages` 
add 
  `is_read_by_admin` tinyint(1) null;

-- migration:2020_10_20_132429_lots --
SET 
  FOREIGN_KEY_CHECKS = 0;;
create table `lots` (
  `id` int unsigned not null auto_increment primary key, 
  `reference` varchar(100) not null, 
  `date_depart` text null, 
  `date_arrive` text null, 
  `statut_id` int unsigned not null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `lots` 
add 
  constraint `lots_statut_id_foreign` foreign key (`statut_id`) references `automations` (`id`) on delete restrict on update restrict;

-- migration:2020_10_21_015236_addstate --
alter table 
  `message_clients` 
add 
  `is_read_by_admin` tinyint(1) null;

-- migration:2020_10_23_014156_add_image_to_messages_table --
alter table 
  `messages` 
add 
  `type` varchar(255) null;

-- migration:2020_10_23_014635_add_image_name_to_messages_table --
alter table 
  `messages` 
add 
  `image` varchar(255) null;

-- migration:2020_11_04_081415_create_code_affiliations_table --
create table `code_affiliations` (
  `id` int unsigned not null auto_increment primary key, 
  `code` varchar(255) not null, 
  `date_validite` varchar(255) not null, 
  `user_id` int unsigned not null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `code_affiliations` 
add 
  constraint `code_affiliations_user_id_foreign` foreign key (`user_id`) references `users` (`id`) on delete restrict on update restrict;

-- migration:2020_11_04_104941_add_seen_cmdstatut_to_notifications_table --
alter table 
  `notifications` 
add 
  `commande_statut_id` int unsigned null, 
add 
  `seen` tinyint(1) null;
alter table 
  `notifications` 
add 
  constraint `notifications_commande_statut_id_foreign` foreign key (`commande_statut_id`) references `automations` (`id`) on delete restrict on update restrict;

-- migration:2020_11_12_145901_add_mobile_payement_reference_transactions_table --
alter table 
  `transactions` 
add 
  `mobile_payement_reference` varchar(255) null;

-- migration:2021_01_03_214643_create_villes_tables --
create table `villes` (
  `id` int unsigned not null auto_increment primary key, 
  `nom` varchar(255) null, 
  `pays_id` int unsigned null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `villes` 
add 
  constraint `villes_pays_id_foreign` foreign key (`pays_id`) references `pays` (`id`) on delete restrict on update restrict;

-- migration:2021_01_03_214935_create_agences_tables --
create table `agences` (
  `id` int unsigned not null auto_increment primary key, 
  `nom` varchar(255) null, 
  `ville_id` int unsigned null, 
  `adresse_id` int unsigned null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `agences` 
add 
  constraint `agences_ville_id_foreign` foreign key (`ville_id`) references `villes` (`id`) on delete restrict on update restrict;
alter table 
  `agences` 
add 
  constraint `agences_adresse_id_foreign` foreign key (`adresse_id`) references `adresses` (`id`) on delete restrict on update restrict;

-- migration:2021_01_12_135959_add_agence_to_colis --
alter table 
  `colis` 
add 
  `agence_id` int unsigned null;
alter table 
  `colis` 
add 
  constraint `colis_agence_id_foreign` foreign key (`agence_id`) references `agences` (`id`) on delete restrict on update restrict;

-- migration:2021_01_26_101627_create_portefeuille_table --
SET 
  FOREIGN_KEY_CHECKS = 0;;
create table `portefeuilles` (
  `id` int unsigned not null auto_increment primary key, 
  `valeur_actuelle` bigint unsigned not null default '0', 
  `compte_bonus` bigint unsigned not null default '0', 
  `user_id` int unsigned not null, `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `portefeuilles` 
add 
  constraint `portefeuilles_user_id_foreign` foreign key (`user_id`) references `users` (`id`) on delete restrict on update restrict;

-- migration:2021_01_26_105903_add_credit_debit_portefeuille_transaction --
ALTER TABLE 
  transactions CHANGE montant montant VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT '0' NOT NULL COLLATE `utf8mb4_unicode_ci`;
alter table 
  `transactions` 
add 
  `montantCreditPorteFeuille` bigint not null default '0', 
add 
  `portefeuille_id` int unsigned null;
alter table 
  `transactions` 
add 
  constraint `transactions_portefeuille_id_foreign` foreign key (`portefeuille_id`) references `portefeuilles` (`id`) on delete restrict on update restrict;

-- migration:2021_01_31_231807_make_phone_number_nullable --
ALTER TABLE 
  transactions CHANGE phone_number phone_number VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`;

-- migration:2021_02_25_102717_add_is_read_to_table --
alter table 
  `messages` 
add 
  `is_read` tinyint(1) not null default '1';

-- migration:2021_02_28_223219_make_nullable_images --
ALTER TABLE 
  articles CHANGE photo_avant photo_avant VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, 
  CHANGE background background VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`;

-- migration:2021_03_15_043527_add_article_user_to_table_notifications --
alter table 
  `notifications` 
add 
  `article_id` int unsigned null, 
add 
  `user_id` int unsigned null;
alter table 
  `notifications` 
add 
  constraint `notifications_article_id_foreign` foreign key (`article_id`) references `articles` (`id`) on delete restrict on update restrict;
alter table 
  `notifications` 
add 
  constraint `notifications_user_id_foreign` foreign key (`user_id`) references `users` (`id`) on delete restrict on update restrict;

-- migration:2021_03_22_144238_make_nullable_email --
ALTER TABLE 
  users CHANGE email email VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`;

-- migration:2021_03_29_194315_add_admin_prenoms_nullable --
ALTER TABLE 
  admins CHANGE prenoms prenoms VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`;

-- migration:2021_03_29_194944_add_admin_user --
alter table 
  `messages` 
add 
  `admin_user` varchar(255) null;
