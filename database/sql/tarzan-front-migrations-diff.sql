-- migration:2021_01_03_214643_create_villes_tables --
create table `villes` (
  `id` int unsigned not null auto_increment primary key, 
  `nom` varchar(255) null, 
  `pays_id` int unsigned null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `villes` 
add 
  constraint `villes_pays_id_foreign` foreign key (`pays_id`) references `pays` (`id`) on delete restrict on update restrict;

-- migration:2021_01_03_214935_create_agences_tables --
create table `agences` (
  `id` int unsigned not null auto_increment primary key, 
  `nom` varchar(255) null, 
  `ville_id` int unsigned null, 
  `adresse_id` int unsigned null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `agences` 
add 
  constraint `agences_ville_id_foreign` foreign key (`ville_id`) references `villes` (`id`) on delete restrict on update restrict;
alter table 
  `agences` 
add 
  constraint `agences_adresse_id_foreign` foreign key (`adresse_id`) references `adresses` (`id`) on delete restrict on update restrict;

-- migration:2021_01_12_135959_add_agence_to_colis --
alter table 
  `colis` 
add 
  `agence_id` int unsigned null;
alter table 
  `colis` 
add 
  constraint `colis_agence_id_foreign` foreign key (`agence_id`) references `agences` (`id`) on delete restrict on update restrict;

-- migration:2021_01_26_101627_create_portefeuille_table --
SET 
  FOREIGN_KEY_CHECKS = 0;;
create table `portefeuilles` (
  `id` int unsigned not null auto_increment primary key, 
  `valeur_actuelle` bigint unsigned not null default '0', 
  `compte_bonus` bigint unsigned not null default '0', 
  `user_id` int unsigned not null, `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8mb4 collate 'utf8mb4_unicode_ci';
alter table 
  `portefeuilles` 
add 
  constraint `portefeuilles_user_id_foreign` foreign key (`user_id`) references `users` (`id`) on delete restrict on update restrict;

-- migration:2021_01_26_105903_add_credit_debit_portefeuille_transaction --
ALTER TABLE 
  transactions CHANGE montant montant VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT '0' NOT NULL COLLATE `utf8mb4_unicode_ci`;
alter table 
  `transactions` 
add 
  `montantCreditPorteFeuille` bigint not null default '0', 
add 
  `portefeuille_id` int unsigned null;
alter table 
  `transactions` 
add 
  constraint `transactions_portefeuille_id_foreign` foreign key (`portefeuille_id`) references `portefeuilles` (`id`) on delete restrict on update restrict;

-- migration:2021_01_31_231807_make_phone_number_nullable --
ALTER TABLE 
  transactions CHANGE phone_number phone_number VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`;
