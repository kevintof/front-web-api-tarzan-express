<?php

use Illuminate\Database\Seeder;

class VilleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('villes')->insert([
            'nom' => 'lome',
            'pays_id' => 1,
            
        ]);

        DB::table('villes')->insert([
            'nom' => 'kpalime',
            'pays_id' => 1,
            
        ]);
        DB::table('villes')->insert([
            'nom' => 'parakou',
            'pays_id' => 2,
        ]);
        DB::table('villes')->insert([
            'nom' => 'porto novo',
            'pays_id' => 2,
        ]);
    }
}
