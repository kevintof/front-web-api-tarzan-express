<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaymentMethodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('automations')->insert([
            'group' => 'TYPE',
            'desc' => 'PAIEMENT',
            'param1' => 'PORTEFEUILLE',
            'param2'=> '0'
        ]);

    }
}
