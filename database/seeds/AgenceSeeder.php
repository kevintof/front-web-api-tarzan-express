<?php

use Illuminate\Database\Seeder;
use App\Agence;
use App\Adresse;
class AgenceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $agence1 = new Agence();
        $agence2 = new Agence(); 
        $agence3 = new Agence();
        $agence4 =  new Agence();
        $adresse1 = new Adresse();
        $adresse2 = new Adresse();
        $adresse3 = new Adresse();
        $adresse4 = new Adresse();
      
        $adresse1->adresse1 = ' non loin de carrefour margot';
        $adresse1->quartier = 'Agoe vakpo';
        $adresse1->pays = 'TOGO';
        $adresse1->ville = 'lome';
        $adresse1->adresse2 = 'Entreprise de lunion';
        $adresse1->save();


        $adresse2->adresse1 = 'en face de muget';
        $adresse2->quartier = 'tokoin';
        $adresse2->pays = 'TOGO';
        $adresse2->ville = 'lome';
        $adresse2->adresse2 = '50m EAMAU';
        $adresse2->save();

        $adresse3->adresse1 = 'en face du lycee de kodji';
        $adresse3->quartier = 'kpodji';
        $adresse3->pays = 'TOGO';
        $adresse3->ville = 'kpalime';
        $adresse3->adresse2 = 'derriere le goudron';
        $adresse3->save();

        $adresse4->adresse1 = 'dahomey to';
        $adresse4->quartier = 'hountondji';
        $adresse4->pays = 'TOGO';
        $adresse4->ville = 'porto novo';
        $adresse4->adresse2 = '50m CANAL SIEGE';
        $adresse4->save();

        $adresse3->adresse3 = 'dahomey to';
        $adresse2->quartier = 'hountondji';
        $adresse2->pays = 'TOGO';
        $adresse2->ville = 'porto novo';
        $adresse2->adresse2 = '50m CANAL SIEGE';
        $adresse2->save();

        
    }
}
