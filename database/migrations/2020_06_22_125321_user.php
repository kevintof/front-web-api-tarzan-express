<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class User extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('nom');
            $table->string('prenoms');
            $table->string('phone_number')->unique();
            $table->string('email');
            $table->string('agree');
            
            $table->integer('pays_id')->unsigned();
            $table->foreign('pays_id')
                ->references('id')
                ->on('pays')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->integer('code_parrainage_id')->unsigned()->nullable();
            $table->foreign('code_parrainage_id')
                ->references('id')
                ->on('code_affiliations')
                ->onDelete('restrict')
                ->onUpdate('restrict');
                
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->integer('verification')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
