<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Produits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('produits', function(Blueprint $table)
        {
            $table->increments('id')->unsigned();
            $table->string('nom');
            $table->text('description')->nullable();
            $table->text('lien');
            $table->string('images')->nullable();
            $table->integer('poids')->nullable();
            $table->integer('montant')->nullable();
            $table->integer('quantite');
            $table->string('statut')->nullable();


            $table->integer('colis_id')->unsigned();
            $table->foreign('colis_id')
                ->references('id')
                ->on('colis')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->integer('unite_id')
                ->unsigned()
                ->nullable();
            $table->foreign('unite_id')
                ->references('id')
                ->on('automations')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->string('details_unite')
                ->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('produits', function(Blueprint $table) {
            $table->dropForeign('produits_colis_id_foreign');
            $table->dropForeign('produits_unite_id_foreign');
        });
        Schema::drop('produits');
    }
}
