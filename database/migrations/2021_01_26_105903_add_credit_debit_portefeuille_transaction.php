<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCreditDebitPortefeuilleTransaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->bigInteger('montantCreditPorteFeuille')->default(0);
            $table->string('montant')->default('0')->change();
            $table->integer('portefeuille_id')->unsigned()->nullable();
            $table->foreign('portefeuille_id')
                ->references('id')
                ->on('portefeuilles')
                ->onDelete('restrict')
                ->onUpdate('restrict');
          
            //$table->





        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function (Blueprint $table) {
            //
        });
    }
}
