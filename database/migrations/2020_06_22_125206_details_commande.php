<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DetailsCommande extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('details_commandes', function(Blueprint $table)
        {
            $table->increments('id')->unsigned();

            $table->string('date_commande');
            $table->string('date_prevu_livraison_min')->nullable();
            $table->string('date_prevu_livraison_max')->nullable();
            $table->string('date_livraison')->nullable();

            $table->integer('transaction_id')
                ->nullable()
                ->unsigned();
            $table->foreign('transaction_id')
                    ->references('id')
                    ->on('transactions')
                    ->onDelete('restrict')
                    ->onUpdate('restrict');

            $table->text('commentaire')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('details_commande', function(Blueprint $table) {

            $table->dropForeign('details_commande_transaction_id_foreign');
        });
        Schema::drop('details_commande');
    }
}
