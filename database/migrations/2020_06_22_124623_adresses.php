<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Adresses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('adresses', function(Blueprint $table)
        {
            $table->increments('id')->unsigned();
            $table->string('pays', 100);
            $table->string('ville', 100);
            $table->string('quartier', 200);
            $table->string('adresse1', 200);
            $table->string('adresse2', 200)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('adresses', function(Blueprint $table) {
            $table->dropForeign('adresses_colis_id_foreign');
        });
        Schema::drop('adresses');
    }
}
