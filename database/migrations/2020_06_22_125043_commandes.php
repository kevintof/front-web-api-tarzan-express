<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Commandes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('commandes', function(Blueprint $table)
        {
            $table->increments('id')->unsigned();
            $table->string('reference', 100);

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->integer('details_commande_id')->unsigned();
            $table->foreign('details_commande_id')
                ->references('id')
                ->on('details_commandes')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->integer('statut_id')->unsigned();
            $table->foreign('statut_id')
                ->references('id')
                ->on('automations')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->integer('montant_livraison')->nullable();
            $table->integer('montant_commande')->nullable();
            $table->integer('montant_service')->nullable();
            
            $table->string('information')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('commandes', function(Blueprint $table) {
            $table->dropForeign('commandes_statut_id_foreign');
            $table->dropForeign('commandes_compte_user_id_foreign');
            $table->dropForeign('commandes_details_commandes_id_foreign');
        });

        Schema::drop('commandes');
    }
}
