<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Lots extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('lots', function(Blueprint $table)
        {
            $table->increments('id')->unsigned();
            $table->string('reference', 100);
            $table->text('date_depart')->nullable();
            $table->text('date_arrive')->nullable();

            $table->integer('statut_id')->unsigned();
            $table->foreign('statut_id')
                ->references('id')
                ->on('automations')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->dropForeign('lots_statut_id_foreign');
        Schema::dropIfExists('lots');
    }

}
