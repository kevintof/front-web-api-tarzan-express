<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Transactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('transactions', function(Blueprint $table)
        {
            $table->increments('id')->unsigned();

            $table->string('reference');
            $table->string('payement_reference')->nullable();
            $table->string('montant');

            $table->integer('mode_paiement_id')->unsigned();
            $table->foreign('mode_paiement_id')
                    ->references('id')
                    ->on('automations')
                    ->onDelete('restrict')
                    ->onUpdate('restrict');

            $table->integer('etat_paiement_id')->unsigned();
            $table->foreign('etat_paiement_id')
                    ->references('id')
                    ->on('automations')
                    ->onDelete('restrict')
                    ->onUpdate('restrict');

            $table->integer('client_id')->unsigned();
            $table->foreign('client_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('restrict')
                    ->onUpdate('restrict');


            $table->string('phone_number');
            $table->integer('attempt');

            $table->text('commentaire')->nullable();
            $table->string('date');

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function(Blueprint $table) {
            $table->dropForeign('transactions_mode_paiement_id_foreign');
            $table->dropForeign('transactions_etat_paiement_id_foreign');
            $table->dropForeign('transactions_client_id_foreign');
        });
        Schema::drop('transactions');
    }
}
