<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LivraisonDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('livraison_details', function(Blueprint $table)
        {
            $table->increments('id')->unsigned();

            $table->string('position');
            $table->string('commentaire');
            $table->string('date');

            $table->integer('colis_id')->unsigned();
            $table->foreign('colis_id')
                    ->references('id')
                    ->on('colis')
                    ->onDelete('restrict')
                    ->onUpdate('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('livraison_details', function(Blueprint $table) {

            $table->dropForeign('livraison_details_colis_id_foreign');
        });
        Schema::drop('livraison_details');
    }
}
